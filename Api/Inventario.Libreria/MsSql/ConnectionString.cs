﻿using System;
using System.Data.SqlClient;
using System.Linq;

namespace Inventario.Libreria.MsSql
{
    public class ConnectionString
    {
        private const string DefaultCommandTimeoutParameterName = "DefaultCommandTimeout";

        public ConnectionString(string connectionName, string connectionString)
        {
            DefaultCommandTimeout = null;
            ConnectionName = connectionName;

            var connectionStringParams = connectionString
                                          .Split(';')
                                          .Where(s => !String.IsNullOrEmpty(s))
                                          .Select(t => t.Split(new char[] { '=' }, 2))
                                          .ToDictionary(t => t[0].Trim(), t => t[1].Trim(), StringComparer.InvariantCultureIgnoreCase);

            if (connectionStringParams.ContainsKey(DefaultCommandTimeoutParameterName))
            {
                this.DefaultCommandTimeout = Int32.Parse(connectionStringParams[DefaultCommandTimeoutParameterName]);
                connectionString = connectionStringParams
                                        .Where(v => v.Key != DefaultCommandTimeoutParameterName)
                                        .Select(v => String.Format("{0}={1}", v.Key, v.Value))
                                        .Aggregate((s, t) => String.Format("{0};{1}", s, t));
            }

            this.connectionString = new SqlConnectionStringBuilder(connectionString).ToString();
        }

        public ConnectionString(ConnectionString dbConnectionString)
            {
            if (dbConnectionString == null)
                throw new ArgumentNullException("dbConnectionString");

            this.ConnectionName = dbConnectionString.ConnectionName;
            this.connectionString = dbConnectionString.connectionString;
            this.DefaultCommandTimeout = dbConnectionString.DefaultCommandTimeout;
        }

        public string ConnectionName
        {
            get;
            private set;
        }

        public string connectionString
        {
            get;
            private set;
        }

        public int? DefaultCommandTimeout
        {
            get;
            private set;
        }

        public override string ToString()
        {
            return string.Format("ConnectionName: {0} ConnectionString: {1}", ConnectionName, connectionString);
        }

    }
}
