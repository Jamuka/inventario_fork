﻿namespace Inventario.Libreria.Enum
{
    internal enum ExecuteType
    {
        Reader,
        ReaderAsync,
        NonQuery,
        NonQueryAsync,
        Scalar,
        Table,
        Tables,
    }
}
