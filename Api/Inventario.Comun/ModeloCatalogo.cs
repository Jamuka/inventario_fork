﻿using System;
namespace Inventario.Comun
{
    public class ModeloCatalogo : ModeloBase
    {
        public string Nombre
        {
            get;
            set;
        }

        public bool Activo
        {
            get;
            set;
        }

        public DateTime FechaActualizacion
        {
            get;
            set;
        }
    }
}
