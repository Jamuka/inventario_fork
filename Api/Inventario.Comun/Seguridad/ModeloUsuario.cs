﻿using System.Collections.Generic;
using Inventario.Comun.Inventario;

namespace Inventario.Comun.Seguridad
{
    public class ModeloUsuario : ModeloCatalogo
    {
        public ModeloUsuario()
        {
            this.Roles = new List<ModeloRol>();
            this.Permisos = new List<ModeloPermiso>();
        }

        public int IdUsuario
        {
            get;
            set;
        }

        public int? IdSucursal
        {
            get;
            set;
        }

        public string Identificacion
        {
            get;
            set;
        }

        public string Correo
        {
            get;
            set;
        }

        public string Rut
        {
            get;
            set;
        }

        public string Contrasena
        {
            get;
            set;
        }

        public List<ModeloRol> Roles
        {
            get;
            set;
        }

        public List<ModeloPermiso> Permisos
        {
            get;
            set;
        }

        public List<ModeloEquipo> Equipos
        {
            get;
            set;
        }
    }
}
