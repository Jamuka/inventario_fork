﻿using Inventario.Comun.Ubicacion;

namespace Inventario.Comun.Seguridad
{
    public class ModeloRespuestaEntrar
    {
        public string Llave
        {
            get;
            set;
        }

        public ModeloUsuario Usuario
        {
            get;
            set;
        }

        public ModeloSucursal Sucursal
        {
            get;
            set;
        }

        public ModeloEmpresa Empresa
        {
            get;
            set;
        }
    }
}
