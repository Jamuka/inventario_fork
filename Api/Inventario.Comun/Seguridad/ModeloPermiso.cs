﻿using System.Collections.Generic;

namespace Inventario.Comun.Seguridad
{
    public class ModeloPermiso : ModeloCatalogo
    {
        public int IdPermiso
        {
            get;
            set;
        }

        public int? IdPermisoPadre
        {
            get;
            set;
        }

        public string Texto
        {
            get;
            set;
        }

        public string Icono
        {
            get;
            set;
        }

        public int Orden
        {
            get;
            set;
        }

        public bool? Concedido
        {
            get;
            set;
        }

        public List<ModeloPermiso> Hijos
        {
            get;
            set;
        }
    }
}
