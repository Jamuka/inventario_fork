﻿using System.Collections.Generic;

namespace Inventario.Comun.Inventario
{
    public class ModeloTipo : ModeloCatalogo
    {
        public int IdTipo
        {
            get;
            set;
        }

        public List<ModeloCaracteristica> Caracteristicas
        {
            get;
            set;
        }
    }
}
