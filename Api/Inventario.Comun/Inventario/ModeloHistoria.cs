﻿namespace Inventario.Comun.Inventario
{
    public class ModeloHistoria : ModeloBase
    {
        public int IdHistoria
        {
            get;
            set;
        }

        public int IdUsuario
        {
            get;
            set;
        }

        public int IdEquipo
        {
            get;
            set;
        }

        public int IdSucursal
        {
            get;
            set;
        }

        public string Comentarios
        {
            get;
            set;
        }
    }
}
