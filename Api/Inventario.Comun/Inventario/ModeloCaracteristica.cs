﻿namespace Inventario.Comun.Inventario
{
    public class ModeloCaracteristica : ModeloCatalogo
    {
        public int IdCaracteristica
        {
            get;
            set;
        }

        public bool? Asignado
        {
            get;
            set;
        }

        public string Valor
        {
            get;
            set;
        }
    }
}
