﻿namespace Inventario.Comun.Ubicacion
{
    public class ModeloEmpresa : ModeloCatalogo
    {
        public int IdEmpresa
        {
            get;
            set;
        }

        public string UrlLogo
        {
            get;
            set;
        }

        public object Imagen
        {
            get;
            set;
        }
    }
}