﻿CREATE TABLE [Seguridad].[Entradas] (
    [IdEntrada]      INT           IDENTITY (1, 1) NOT NULL,
    [IdUsuario]      INT           NOT NULL,
    [IdSesion]       NVARCHAR (36) NOT NULL,
    [DireccionIp]    NVARCHAR (50) NULL,
    [FechaEntrada]   DATETIME      NOT NULL,
    [FechaSalida]    DATETIME      NULL,
    [FechaInsercion] DATETIME      NOT NULL,
    CONSTRAINT [PK_Entradas] PRIMARY KEY CLUSTERED ([IdEntrada] ASC),
    CONSTRAINT [FK_Entradas_Usuario] FOREIGN KEY ([IdUsuario]) REFERENCES [Seguridad].[Usuario] ([IdUsuario])
);

