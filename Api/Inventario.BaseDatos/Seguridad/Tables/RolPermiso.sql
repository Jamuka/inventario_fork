﻿CREATE TABLE [Seguridad].[RolPermiso] (
    [IdRolPermiso]   INT      IDENTITY (1, 1) NOT NULL,
    [IdRol]          INT      NOT NULL,
    [IdPermiso]      INT      NULL,
    [FechaInsercion] DATETIME NOT NULL,
    CONSTRAINT [PK_RolPermiso] PRIMARY KEY CLUSTERED ([IdRolPermiso] ASC),
    CONSTRAINT [FK_RolPermiso_Permiso] FOREIGN KEY ([IdPermiso]) REFERENCES [Seguridad].[Permiso] ([IdPermiso]),
    CONSTRAINT [FK_RolPermiso_Rol] FOREIGN KEY ([IdRol]) REFERENCES [Seguridad].[Rol] ([IdRol])
);

