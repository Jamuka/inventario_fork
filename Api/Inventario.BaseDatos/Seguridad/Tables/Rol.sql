﻿CREATE TABLE [Seguridad].[Rol] (
    [IdRol]              INT           IDENTITY (1, 1) NOT NULL,
    [Nombre]             NVARCHAR (50) NOT NULL,
    [Activo]             BIT           NOT NULL,
    [FechaInsercion]     DATETIME      NOT NULL,
    [FechaActualizacion] DATETIME      NOT NULL,
    CONSTRAINT [PK_Rol] PRIMARY KEY CLUSTERED ([IdRol] ASC)
);

