﻿CREATE TABLE [Seguridad].[Permiso] (
    [IdPermiso]          INT           IDENTITY (1, 1) NOT NULL,
    [IdPermisoPadre]     INT           NULL,
    [Nombre]             NVARCHAR (50) NOT NULL,
    [Texto]              NVARCHAR (20) NULL,
    [Icono]              NVARCHAR (20) NULL,
    [Orden]              SMALLINT      NOT NULL,
    [Activo]             BIT           NOT NULL,
    [FechaInsercion]     DATETIME      NOT NULL,
    [FechaActualizacion] DATETIME      NOT NULL,
    CONSTRAINT [PK_Permiso] PRIMARY KEY CLUSTERED ([IdPermiso] ASC),
    CONSTRAINT [FK_Permiso_Permiso] FOREIGN KEY ([IdPermisoPadre]) REFERENCES [Seguridad].[Permiso] ([IdPermiso])
);

