﻿CREATE TABLE [Seguridad].[Usuario] (
    [IdUsuario]          INT           IDENTITY (1, 1) NOT NULL,
    [IdSucursal]         INT           NULL,
    [Identificacion]     NVARCHAR (50) NOT NULL,
    [Correo]             NVARCHAR (50) NULL,
    [Rut]                NVARCHAR (20) NULL,
    [Nombre]             NVARCHAR (50) NOT NULL,
    [Contrasena]         NVARCHAR (20) NULL,
    [Activo]             BIT           NOT NULL,
    [FechaInsercion]     DATETIME      NOT NULL,
    [FechaActualizacion] DATETIME      NOT NULL,
    CONSTRAINT [PK_Usuario] PRIMARY KEY CLUSTERED ([IdUsuario] ASC),
    CONSTRAINT [FK_Usuario_Sucursal] FOREIGN KEY ([IdSucursal]) REFERENCES [Ubicacion].[Sucursal] ([IdSucursal])
);



