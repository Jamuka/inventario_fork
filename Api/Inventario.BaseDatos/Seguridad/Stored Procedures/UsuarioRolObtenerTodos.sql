﻿CREATE PROCEDURE [Seguridad].[UsuarioRolObtenerTodos]
	@idUsuario int
as
 begin
        select * from [Seguridad].[Usuario] where idUsuario = @idUsuario

		select r.*, 
			   case 
					when ur.IdUsuario is null then cast(0 as bit)
					else cast(1 as bit)
				end as Asignado
		  from [Seguridad].[Rol] r
	 left join [Seguridad].[UsuarioRol] ur on ur.idRol = r.idRol and ur.idUsuario = @idUsuario
		 where r.Activo = 1
end