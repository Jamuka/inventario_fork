﻿CREATE PROCEDURE [Seguridad].[UsuarioGuardar]
	@IdUsuario int, 
	@IdSucursal int = null,
	@Identificacion nvarchar(50),
	@Correo nvarchar(50),
	@Rut nvarchar(20),
	@Nombre nvarchar(50),	
	@Contrasena nvarchar(20)=null,
	@Activo bit = 1
as
 begin
	if(@IdUsuario > 0)
	 begin
		update [Seguridad].[Usuario]
		   set IdSucursal = @IdSucursal,			   
			   Identificacion = @Identificacion,
			   Correo = @Correo,
			   Rut = @Rut,
			   Nombre=@Nombre,
			   Contrasena = @Contrasena,
			   Activo = @Activo
		 where IdUsuario = @IdUsuario
	 end
	else
	 begin
		if(not exists(select * from [Seguridad].[Usuario] where Identificacion = @Identificacion))
		 begin
			insert into [Seguridad].[Usuario](IdSucursal, Identificacion, Correo, Rut, Nombre, Contrasena)
			values (@IdSucursal, @Identificacion, @Correo, @Rut, @Nombre, @Contrasena)
		 end
	 end

 end