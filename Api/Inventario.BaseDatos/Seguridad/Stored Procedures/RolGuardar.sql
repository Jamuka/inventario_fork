﻿CREATE PROCEDURE [Seguridad].[RolGuardar]
	@IdRol int, 
	@Nombre nvarchar(50),	
	@Activo bit = 1
as
 begin
	if(@IdRol > 0)
	 begin
		update [Seguridad].[Rol]
		   set Nombre=@Nombre,
			   Activo = @Activo
		 where IdRol = @IdRol
	 end
	else
	 begin
		if(not exists(select * from [Seguridad].[Rol] where Nombre = @Nombre))
		 begin
			insert into [Seguridad].[Rol](Nombre)
			values (@Nombre)
		 end
	 end

 end