﻿CREATE PROCEDURE [Seguridad].[UsuarioEliminar]
	@idUsuario int
as
 begin
	update [Inventario].[Equipo]
	   set IdUsuario = null
	where IdUsuario = @idUsuario
	
	delete from [Seguridad].[UsuarioRol]
	where IdUsuario = @idUsuario

	delete from [Seguridad].[Usuario]
	where IdUsuario = @idUsuario

 end