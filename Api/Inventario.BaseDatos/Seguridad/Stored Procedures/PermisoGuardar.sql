﻿CREATE PROCEDURE [Seguridad].[PermisoGuardar]
	@IdPermiso int, 
	@IdPermisoPadre int = null, 
	@Nombre nvarchar(50),	
	@Texto nvarchar(20) = null,
	@Icono nvarchar(20) = null,
	@Orden smallint,
	@Activo bit = 1
as
 begin
	if(@IdPermiso > 0)
	 begin
		update [Seguridad].[Permiso]
		   set IdPermisoPadre = @IdPermisoPadre,
			   Nombre=@Nombre,
			   Texto = @Texto,
			   Icono = @Icono,
			   Orden = @Orden,
			   Activo = @Activo
		 where IdPermiso = @IdPermiso
	 end
	else
	 begin
		if(not exists(select * from [Seguridad].[Permiso] where Nombre = @Nombre))
		 begin
			insert into [Seguridad].[Permiso](IdPermisoPadre, Nombre, Texto, Icono, Orden)
			values (@IdPermisoPadre, @Nombre, @Texto, @Icono, @Orden)
		 end
	 end

 end