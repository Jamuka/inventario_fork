﻿CREATE procedure [Seguridad].[RolPermisoGuardar]
	@Permisos [Seguridad].[TRolPermiso] readonly
as
begin
	declare @idRol int
	select top 1 @idRol = idRol from @Permisos

	MERGE [Seguridad].[RolPermiso] AS target
     USING (SELECT * FROM @Permisos) AS source
     ON (target.idRol = source.IdRol AND target.idPermiso = source.idPermiso)
     WHEN NOT MATCHED BY TARGET THEN
		INSERT (idRol, idPermiso)
		VALUES (source.idRol, source.idPermiso)
    WHEN NOT MATCHED BY SOURCE and target.idRol = @idRol THEN
		DELETE 
    OUTPUT $action, Inserted.idRol, Inserted.idPermiso;
end