﻿CREATE PROCEDURE [Seguridad].[UsuarioEntrar]
    @idUsuario int,
    @idSesion nvarchar(36),
    @DireccionIp nvarchar(50)
as
 begin
	insert into [Seguridad].[Entradas](IdUsuario, IdSesion, DireccionIp) 
                                values(@idUsuario, @idSesion, @DireccionIp)
 end