﻿CREATE procedure [Seguridad].[UsuarioRolGuardar]
	@Roles [Seguridad].[TUsuarioRol] readonly
as
begin
	declare @idUsuario int
	select top 1 @idUsuario = idUsuario from @Roles

	MERGE [Seguridad].[UsuarioRol] AS target
     USING (SELECT * FROM @Roles) AS source
     ON (target.idUsuario = source.IdUsuario AND target.idRol = source.idRol)
     WHEN NOT MATCHED BY TARGET THEN
		INSERT (idUsuario, idRol)
		VALUES (source.idUsuario, source.idRol)
    WHEN NOT MATCHED BY SOURCE and target.idUsuario = @idUsuario THEN
		DELETE 
    OUTPUT $action, Inserted.idUsuario, Inserted.idRol;
end