﻿CREATE PROCEDURE [Ubicacion].[EmpresaGuardar]
	@IdEmpresa int, 
	@Nombre nvarchar(50),	
	@UrlLogo nvarchar(200),
	@Activo bit = 1
as
 begin
	if(@IdEmpresa > 0)
	 begin
		update [Ubicacion].[Empresa]
		   set Nombre=@Nombre,
			   UrlLogo = @UrlLogo,
			   Activo = @Activo
		 where IdEmpresa = @IdEmpresa
	 end
	else
	 begin
		if(not exists(select * from [Ubicacion].[Empresa] where Nombre = @Nombre))
		 begin
			insert into [Ubicacion].[Empresa](Nombre, UrlLogo)
			values (@Nombre, @UrlLogo)
		 end
	 end

 end