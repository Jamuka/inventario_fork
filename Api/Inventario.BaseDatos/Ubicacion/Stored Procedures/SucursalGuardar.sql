﻿CREATE PROCEDURE [Ubicacion].[SucursalGuardar]
	@IdSucursal int, 
	@IdEmpresa int, 
	@Nombre nvarchar(50),	
	@Activo bit = 1
as
 begin
	if(@IdSucursal > 0)
	 begin
		update [Ubicacion].[Sucursal]
		   set IdEmpresa = @IdEmpresa,
			   Nombre=@Nombre,
			   Activo = @Activo
		 where IdSucursal = @IdSucursal
	 end
	else
	 begin
		if(not exists(select * from [Ubicacion].[Sucursal] where Nombre = @Nombre))
		 begin
			insert into [Ubicacion].[Sucursal](IdEmpresa, Nombre)
			values (@IdEmpresa, @Nombre)
		 end
	 end

 end