﻿CREATE PROCEDURE [Inventario].[HistoriaObtenerTodos]
	@idSucursal int = null
as
 begin
	select * from [Inventario].[Historia]
	where (@idSucursal is null or IdSucursal = @idSucursal)
	order by FechaInsercion desc
 end