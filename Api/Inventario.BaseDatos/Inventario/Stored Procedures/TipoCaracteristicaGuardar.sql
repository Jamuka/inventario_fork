﻿CREATE procedure [Inventario].[TipoCaracteristicaGuardar]
	@Caracteristicas [Inventario].[TTipoCaracteristica] readonly
as
begin
	declare @idTipo int
	select top 1 @idTipo = idTipo from @Caracteristicas

	MERGE [Inventario].[TipoCaracteristica] AS target
     USING (SELECT * FROM @Caracteristicas) AS source
     ON (target.idTipo = source.IdTipo AND target.idCaracteristica = source.idCaracteristica)
     WHEN NOT MATCHED BY TARGET THEN
		INSERT (idTipo, idCaracteristica)
		VALUES (source.idTipo, source.idCaracteristica)
    WHEN NOT MATCHED BY SOURCE and target.idTipo = @idTipo THEN
		DELETE 
    OUTPUT $action, Inserted.idTipo, Inserted.idCaracteristica;
end