﻿CREATE PROCEDURE [Inventario].[CaracteristicaEliminar]
	@idCaracteristica int
as
 begin
	delete [Inventario].[TipoCaracteristica]
	where IdCaracteristica = @idCaracteristica
	
	delete from [Inventario].[EquipoCaracteristica]
	where IdCaracteristica = @idCaracteristica

	delete from [Inventario].[Caracteristica]
	where IdCaracteristica = @idCaracteristica

 end