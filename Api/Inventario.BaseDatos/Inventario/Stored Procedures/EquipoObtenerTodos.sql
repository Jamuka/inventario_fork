﻿--exec [Inventario].[EquipoObtenerTodos] @idSucursal = 1
CREATE PROCEDURE [Inventario].[EquipoObtenerTodos]
	@idSucursal int = null,
	@idUsuario int = null
as
 begin
	select * from [Inventario].[Equipo]
	where (@idSucursal is null or IdSucursal = @idSucursal)
	  and (@idUsuario is null or IdUsuario = @idUsuario)
	--where Activo = 1

		select ec.IdEquipo, c.*, ec.Valor 
		  from [Inventario].[EquipoCaracteristica] ec
	inner join [Inventario].[Caracteristica] c on c.IdCaracteristica = ec.IdCaracteristica
	inner join [Inventario].[Equipo] e on e.IdEquipo = ec.IdEquipo
		 where (@idSucursal is null or IdSucursal = @idSucursal)
		   and (@idUsuario is null or IdUsuario = @idUsuario)
	  order by ec.IdEquipo
 end