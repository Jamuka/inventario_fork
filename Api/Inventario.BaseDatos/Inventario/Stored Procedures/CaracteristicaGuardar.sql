﻿CREATE PROCEDURE [Inventario].[CaracteristicaGuardar]
	@IdCaracteristica int, 
	@Nombre nvarchar(50),	
	@Activo bit = 1
as
 begin
	if(@IdCaracteristica > 0)
	 begin
		update [Inventario].[Caracteristica]
		   set Nombre=@Nombre,
			   Activo = @Activo
		 where IdCaracteristica = @IdCaracteristica
	 end
	else
	 begin
		if(not exists(select * from [Inventario].[Caracteristica] where Nombre = @Nombre))
		 begin
			insert into [Inventario].[Caracteristica](Nombre)
			values (@Nombre)
		 end
	 end

 end