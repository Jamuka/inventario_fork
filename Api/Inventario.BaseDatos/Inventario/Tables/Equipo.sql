﻿CREATE TABLE [Inventario].[Equipo] (
    [IdEquipo]           INT           IDENTITY (1, 1) NOT NULL,
    [IdTipo]             INT           NOT NULL,
    [IdEstado]           INT           NOT NULL,
    [IdSucursal]         INT           NULL,
    [IdUsuario]          INT           NULL,
    [Nombre]             NVARCHAR (50) NOT NULL,
    [ActivoFijo]         NVARCHAR (20) NULL,
    [Marca]              NVARCHAR (50) NULL,
    [Modelo]             NVARCHAR (20) NULL,
    [Serie]              NVARCHAR (30) NULL,
    [Numero]             SMALLINT      NULL,
    [FechaCompra]        DATETIME      NULL,
    [IdUsuarioCaptura]   INT           NULL,
    [Activo]             BIT           NOT NULL,
    [FechaInsercion]     DATETIME      NOT NULL,
    [FechaActualizacion] DATETIME      NOT NULL,
    CONSTRAINT [PK_Equipo] PRIMARY KEY CLUSTERED ([IdEquipo] ASC),
    CONSTRAINT [FK_Equipo_Estado] FOREIGN KEY ([IdEstado]) REFERENCES [Inventario].[Estado] ([IdEstado]),
    CONSTRAINT [FK_Equipo_Sucursal] FOREIGN KEY ([IdSucursal]) REFERENCES [Ubicacion].[Sucursal] ([IdSucursal]),
    CONSTRAINT [FK_Equipo_Tipo] FOREIGN KEY ([IdTipo]) REFERENCES [Inventario].[Tipo] ([IdTipo]),
    CONSTRAINT [FK_Equipo_Usuario] FOREIGN KEY ([IdUsuario]) REFERENCES [Seguridad].[Usuario] ([IdUsuario]),
    CONSTRAINT [FK_Equipo_UsuarioCaptura] FOREIGN KEY ([IdUsuarioCaptura]) REFERENCES [Seguridad].[Usuario] ([IdUsuario])
);



