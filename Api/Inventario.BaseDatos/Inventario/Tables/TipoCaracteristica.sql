﻿CREATE TABLE [Inventario].[TipoCaracteristica] (
    [IdTipoCaracteristica] INT      IDENTITY (1, 1) NOT NULL,
    [IdTipo]               INT      NOT NULL,
    [IdCaracteristica]     INT      NOT NULL,
    [FechaInsercion]       DATETIME NOT NULL,
    [FechaActualizacion]   DATETIME NOT NULL,
    CONSTRAINT [PK_TipoCaracteristica] PRIMARY KEY CLUSTERED ([IdTipoCaracteristica] ASC),
    CONSTRAINT [FK_TipoCaracteristica_Caracteristica] FOREIGN KEY ([IdCaracteristica]) REFERENCES [Inventario].[Caracteristica] ([IdCaracteristica]),
    CONSTRAINT [FK_TipoCaracteristica_Tipo] FOREIGN KEY ([IdTipo]) REFERENCES [Inventario].[Tipo] ([IdTipo])
);

