﻿using System.Collections.Generic;
using Inventario.Comun.Inventario;

namespace Inventario.Servicio.Contracts.Inventario
{
    public interface IServicioTipo
    {
        IEnumerable<ModeloTipo> ObtenerTodos();

        void Guardar(ModeloTipo modelo);

        ModeloTipo CaracteristicaObtenerTodos(int idTipo);

        void CaracteristicaGuardar(ModeloTipo modelo);
    }
}