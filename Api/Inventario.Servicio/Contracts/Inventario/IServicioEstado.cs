﻿using System.Collections.Generic;
using Inventario.Comun.Inventario;

namespace Inventario.Servicio.Contracts.Inventario
{
    public interface IServicioEstado
    {
        IEnumerable<ModeloEstado> ObtenerTodos();

        void Guardar(ModeloEstado modelo);
    }
}
