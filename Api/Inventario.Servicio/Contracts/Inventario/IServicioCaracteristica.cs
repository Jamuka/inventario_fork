﻿using System.Collections.Generic;
using Inventario.Comun.Inventario;

namespace Inventario.Servicio.Contracts.Inventario
{
    public interface IServicioCaracteristica
    {
        IEnumerable<ModeloCaracteristica> ObtenerTodos();

        void Guardar(ModeloCaracteristica modelo);

        void Eliminar(int idCaracteristica);
    }
}
