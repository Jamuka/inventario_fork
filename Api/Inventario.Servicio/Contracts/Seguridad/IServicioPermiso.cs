﻿using System.Collections.Generic;
using Inventario.Comun.Seguridad;

namespace Inventario.Servicio.Contracts.Seguridad
{
    public interface IServicioPermiso
    {
        IEnumerable<ModeloPermiso> ObtenerTodos();

        void Guardar(ModeloPermiso modelo);
    }
}