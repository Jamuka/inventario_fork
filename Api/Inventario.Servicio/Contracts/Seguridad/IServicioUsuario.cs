﻿using System.Collections.Generic;
using Inventario.Comun.Seguridad;

namespace Inventario.Servicio.Contracts.Seguridad
{
    public interface IServicioUsuario
    {
        IEnumerable<ModeloUsuario> ObtenerTodos(int? idSucursal);

        IEnumerable<ModeloUsuario> ObtenerTodosPorRol(string rol);

        void Guardar(ModeloUsuario modelo);

        void Eliminar(int idUsuario);

        ModeloRespuestaEntrar Entrar(ModeloSolicitudEntrar modelo);

        void Salir(int idUsuario, string idSesion);

        ModeloUsuario RolObtenerTodos(int idUsuario);

        void RolGuardar(ModeloUsuario modelo);
    }
}
