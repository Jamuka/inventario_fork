﻿using System.Collections.Generic;
using Inventario.Comun.Ubicacion;

namespace Inventario.Servicio.Contracts.Ubicacion
{
    public interface IServicioSucursal
    {
        IEnumerable<ModeloSucursal> ObtenerTodos();

        void Guardar(ModeloSucursal modelo);
    }
}