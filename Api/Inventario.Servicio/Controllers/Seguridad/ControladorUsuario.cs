﻿using System;
using System.Collections.Generic;
using System.Linq;
using Inventario.Comun.Seguridad;
using Inventario.Servicio.Contracts.Seguridad;
using Inventario.Servicio.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace Inventario.Servicio.Controllers.Seguridad
{
    [Route("api/seguridad/[controller]"), ApiController]
    public class UsuarioController : ControllerBase
    {
        private readonly IServicioUsuario usuarioServicio;

        public UsuarioController(IServicioUsuario usuarioServicio)
        {
            this.usuarioServicio = usuarioServicio;
        }

        [Authorize, HttpGet, Route("obtenertodos")]
        public ActionResult<IEnumerable<ModeloUsuario>> ObtenerTodos(bool porSucursal=true)
        {
            try
            {
                int? idSucursal = null;
                if (porSucursal)
                {
                    var idSucursalClaim = User.Claims.FirstOrDefault(c => c.Type == Constantes.IdSucursal);
                    if (idSucursalClaim != null)
                    {
                        if (!string.IsNullOrEmpty(idSucursalClaim.Value))
                        {
                            idSucursal = int.Parse(idSucursalClaim.Value);
                        }
                        else
                        {
                            idSucursal = null;
                        }
                    }
                }

                return Ok(this.usuarioServicio.ObtenerTodos(idSucursal));
            }
            catch (Exception ex)
            {
                return StatusCode(500, ex.Message);
            }
        }

        [HttpGet, Route("obtenertodosporrol")]
        public ActionResult<IEnumerable<ModeloUsuario>> ObtenerTodosPorRol(string rol)
        {
            try
            {
                return Ok(this.usuarioServicio.ObtenerTodosPorRol(rol));
            }
            catch (Exception ex)
            {
                return StatusCode(500, ex.Message);
            }
        }

        [Authorize, HttpPost, Route("guardar")]
        public ActionResult Guardar(ModeloUsuario modelo)
        {
            try
            {
                this.usuarioServicio.Guardar(modelo);
                return Ok();
            }
            catch (Exception ex)
            {
                return StatusCode(500, ex.Message);
            }
        }

        [Authorize, HttpDelete, Route("eliminar")]
        public ActionResult Eliminar(int idUsuario)
        {
            try
            {
                this.usuarioServicio.Eliminar(idUsuario);
                return Ok();
            }
            catch (Exception ex)
            {
                return StatusCode(500, ex.Message);
            }
        }

        [HttpPost, Route("entrar")]
        public ActionResult<ModeloRespuestaEntrar> Entrar(ModeloSolicitudEntrar modelo)
        {
            try
            {
                if (string.IsNullOrEmpty(modelo.DireccionIp))
                {
                    modelo.DireccionIp = this.HttpContext.Connection.RemoteIpAddress.ToString();
                }

                return Ok(this.usuarioServicio.Entrar(modelo));
            }
            catch (Exception ex)
            {
                return StatusCode(500, ex.Message);
            }
        }

        [Authorize, HttpGet, Route("salir")]
        public ActionResult Salir()
        {
            try
            {
                var idUsuario = Convert.ToInt32(this.User.Claims.First(c => c.Type == Constantes.IdUsuario).Value);
                var idSesion = this.User.Claims.First(c => c.Type == Constantes.IdSesion).Value.ToString();
                this.usuarioServicio.Salir(idUsuario, idSesion);
                return Ok();
            }
            catch (Exception ex)
            {
                return StatusCode(500, ex.Message);
            }
        }

        [HttpGet, Route("rolobtenertodos")]
        public ActionResult<ModeloUsuario> RolesObtenerTOdos(int idUsuario)
        {
            try
            {
                return Ok(this.usuarioServicio.RolObtenerTodos(idUsuario));
            }
            catch (Exception ex)
            {
                return StatusCode(500, ex.Message);
            }
        }

        [HttpPost, Route("rolguardar")]
        public ActionResult RolesGuardar(ModeloUsuario modelo)
        {
            try
            {
                this.usuarioServicio.RolGuardar(modelo);
                return Ok();
            }
            catch (Exception ex)
            {
                return StatusCode(500, ex.Message);
            }
        }
    }

}
