﻿using System;
using System.Collections.Generic;
using Inventario.Comun.Seguridad;
using Inventario.Servicio.Contracts.Seguridad;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace Inventario.Servicio.Controllers.Seguridad
{
    [Authorize, Route("api/seguridad/[controller]"), ApiController]
    public class RolController : ControllerBase
    {
        private readonly IServicioRol servicioRol;

        public RolController(IServicioRol servicioRol)
        {
            this.servicioRol = servicioRol;
        }

        [HttpGet, Route("obtenertodos")]
        public ActionResult<IEnumerable<ModeloRol>> ObtenerTodos()
        {
            try
            {
                return Ok(this.servicioRol.ObtenerTodos());
            }
            catch (Exception ex)
            {
                return StatusCode(500, ex.Message);
            }
        }

        [HttpPost, Route("guardar")]
        public ActionResult Set(ModeloRol modelo)
        {
            try
            {
                this.servicioRol.Guardar(modelo);
                return Ok();
            }
            catch (Exception ex)
            {
                return StatusCode(500, ex.Message);
            }
        }

        [HttpGet, Route("permisoobtenertodos")]
        public ActionResult<ModeloRol> Permisoobtenertodos(int idRol)
        {
            try
            {
                return Ok(this.servicioRol.PermisoObtenerTodos(idRol));
            }
            catch (Exception ex)
            {
                return StatusCode(500, ex.Message);
            }
        }

        [HttpPost, Route("permisoguardar")]
        public ActionResult PermisoGuardar(ModeloRol modelo)
        {
            try
            {
                this.servicioRol.PermisoGuardar(modelo);
                return Ok();
            }
            catch (Exception ex)
            {
                return StatusCode(500, ex.Message);
            }
        }
    }
}
