﻿using System;
using System.Linq;
using System.Threading.Tasks;
using Inventario.Servicio.Contracts;
using Inventario.Servicio.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace Inventario.Servicio.Controllers
{
    [Authorize, Route("api/[controller]"), ApiController]
    public class TableroController : ControllerBase
    {
        private readonly IServicioTablero servicioTablero;

        public TableroController(IServicioTablero servicioTablero)
        {
            this.servicioTablero = servicioTablero;
        }

        [HttpPost, DisableRequestSizeLimit, Route("subirArchivo")]
        public async Task<ActionResult> SubirArchivo()
        {
            try
            {
                var idUsuarioCapturaClaim = User.Claims.FirstOrDefault(c => c.Type == Constantes.IdUsuario);
                var idUsuarioCaptura = Convert.ToInt32(idUsuarioCapturaClaim.Value);
                var formCollection = await Request.ReadFormAsync();
                this.servicioTablero.ProcesarArchivo(idUsuarioCaptura, formCollection.Files.First());
                return Ok();
            }
            catch (Exception ex)
            {
                return StatusCode(500, ex.Message);
            }
        }
    }
}
