﻿using System;
using System.Collections.Generic;
using Inventario.Comun.Inventario;
using Inventario.Servicio.Contracts.Inventario;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace Inventario.Servicio.Controllers.Inventario
{
    [Authorize, Route("api/inventario/[controller]"), ApiController]
    public class CaracteristicaController : ControllerBase
    {
        private readonly IServicioCaracteristica servicioCaracteristica;

        public CaracteristicaController(IServicioCaracteristica servicioCaracteristica)
        {
            this.servicioCaracteristica = servicioCaracteristica;
        }

        [HttpGet, Route("obtenertodos")]
        public ActionResult<IEnumerable<ModeloCaracteristica>> ObtenerTodos()
        {
            try
            {
                return Ok(this.servicioCaracteristica.ObtenerTodos());
            }
            catch (Exception ex)
            {
                return StatusCode(500, ex.Message);
            }
        }

        [HttpPost, Route("guardar")]
        public ActionResult Guardar(ModeloCaracteristica modelo)
        {
            try
            {
                this.servicioCaracteristica.Guardar(modelo);
                return Ok();
            }
            catch (Exception ex)
            {
                return StatusCode(500, ex.Message);
            }
        }

        [Authorize, HttpDelete, Route("eliminar")]
        public ActionResult Eliminar(int idCaracteristica)
        {
            try
            {
                this.servicioCaracteristica.Eliminar(idCaracteristica);
                return Ok();
            }
            catch (Exception ex)
            {
                return StatusCode(500, ex.Message);
            }
        }
    }
}
