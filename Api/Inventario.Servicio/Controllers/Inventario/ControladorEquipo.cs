﻿using System;
using System.Collections.Generic;
using System.Linq;
using Inventario.Comun.Inventario;
using Inventario.Servicio.Contracts.Inventario;
using Inventario.Servicio.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace Inventario.Servicio.Controllers.Inventario
{
    [Authorize, Route("api/inventario/[controller]"), ApiController]
    public class EquipoController : ControllerBase
    {
        private readonly IServicioEquipo servicioEquipo;

        public EquipoController(IServicioEquipo servicioEquipo)
        {
            this.servicioEquipo = servicioEquipo;
        }

        [HttpGet, Route("obtener")]
        public ActionResult<ModeloEquipo> Obtener(int idEquipo)
        {
            try
            {
                return Ok(this.servicioEquipo.Obtener(idEquipo));
            }
            catch (Exception ex)
            {
                return StatusCode(500, ex.Message);
            }
        }


        [HttpGet, Route("obtenertodos")]
        public ActionResult<IEnumerable<ModeloEquipo>> ObtenerTodos(bool porSucursal=true, int? idUsuario=null)
        {
            try
            {
                int? idSucursal = null;
                if (porSucursal)
                {
                    var idSucursalClaim = User.Claims.FirstOrDefault(c => c.Type == Constantes.IdSucursal);
                    if (idSucursalClaim != null)
                    {
                        if (!string.IsNullOrEmpty(idSucursalClaim.Value))
                        {
                            idSucursal = int.Parse(idSucursalClaim.Value);
                        }
                        else
                        {
                            idSucursal = null;
                        }
                    }
                }

                return Ok(this.servicioEquipo.ObtenerTodos(idSucursal, idUsuario));
            }
            catch (Exception ex)
            {
                return StatusCode(500, ex.Message);
            }
        }

        [HttpPost, Route("guardar")]
        public ActionResult Guardar(ModeloEquipo modelo)
        {
            try
            {
                var idUsuarioCapturaClaim = User.Claims.FirstOrDefault(c => c.Type == Constantes.IdUsuario);
                var idUsuarioCaptura = Convert.ToInt32(idUsuarioCapturaClaim.Value);
                this.servicioEquipo.Guardar(modelo, idUsuarioCaptura);
                return Ok();
            }
            catch (Exception ex)
            {
                return StatusCode(500, ex.Message);
            }
        }


        [HttpGet, Route("registroobtenertodos")]
        public ActionResult<IEnumerable<ModeloEquipo>> RegistroObtenerTodos(int idEquipo)
        {
            try
            {
                 return Ok(this.servicioEquipo.RegistroObtenerTodos(idEquipo));
            }
            catch (Exception ex)
            {
                return StatusCode(500, ex.Message);
            }
        }
    }
}
