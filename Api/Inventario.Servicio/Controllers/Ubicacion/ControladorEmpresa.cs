﻿using System;
using System.Collections.Generic;
using Inventario.Comun.Ubicacion;
using Inventario.Servicio.Contracts.Ubicacion;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace Inventario.Servicio.Controllers.Ubicacion
{
    [Authorize, Route("api/ubicacion/[controller]"), ApiController]
    public class EmpresaController : ControllerBase
    {
        private readonly IServicioEmpresa servicioEmpresa;
        private readonly IHttpContextAccessor httpContextAccessor;

        public EmpresaController(IServicioEmpresa servicioEmpresa, IHttpContextAccessor httpContextAccessor)
        {
            this.servicioEmpresa = servicioEmpresa;
            this.httpContextAccessor = httpContextAccessor;
        }

        [HttpGet, Route("obtenertodos")]
        public ActionResult<IEnumerable<ModeloEmpresa>> ObtenerTodos()
        {
            try
            {
                return Ok(this.servicioEmpresa.ObtenerTodos());
            }
            catch (Exception ex)
            {
                return StatusCode(500, ex.Message);
            }
        }

        [HttpPost, Route("guardar")]
        public ActionResult Guardar(ModeloEmpresa modelo)
        {
            try
            {
                this.servicioEmpresa.Guardar(modelo);
                return Ok();
            }
            catch (Exception ex)
            {
                return StatusCode(500, ex.Message);
            }
        }
    }
}
