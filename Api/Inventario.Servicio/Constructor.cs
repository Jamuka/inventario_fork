﻿using Inventario.Servicio.Models;
using Microsoft.Extensions.Configuration;

namespace Inventario.Servicio
{
    internal class Constructor
    {
        private static volatile Constructor intancia;
        private static readonly object ObjetoSincronizador = new object();
        private IConfiguration configuracion;

        private Constructor()
        {

        }

        internal static Constructor Instancia
        {
            get
            {
                if (intancia == null)
                {
                    lock (ObjetoSincronizador)
                    {
                        if (intancia == null)
                        {
                            intancia = new Constructor();
                        }
                    }
                }

                return intancia;
            }
        }

        internal void InicializaConfiguracion(IConfiguration configuration)
        {
            this.configuracion = configuration;
        }

        internal string ObtenerCadenaConexion(string connectionStringName)
        {
            return this.configuracion.GetConnectionString(connectionStringName);
        }

        internal Configuracion ObtenerConfiguracion()
        {
            return this.configuracion.GetSection("Configuracion").Get<Configuracion>();
        }
    }

}
