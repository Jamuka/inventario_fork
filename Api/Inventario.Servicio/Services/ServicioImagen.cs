﻿using System;
using System.IO;
using Newtonsoft.Json;

namespace Inventario.Servicio.Services
{
    internal static class ServicioImagen
    {
        const string jpeg = "image/jpeg";
        const string png = "image/png";

        internal static void Guardar(string viejoNombre, string nuevoNombre)
        {
            if (File.Exists(viejoNombre))
            {
                File.Move(viejoNombre, nuevoNombre);
            }
        }

        internal static void Guardar(object archivo, string directorio, string nombre)
        {
            if (!Directory.Exists(directorio))
            {
                Directory.CreateDirectory(directorio);
            }

            var imagen = JsonConvert.DeserializeObject<Models.ModeloArchivo>(archivo.ToString());
            var extContains = string.Empty;
            if (imagen.result.Contains(jpeg))
            {
                extContains = jpeg;
            }
            else if (imagen.result.Contains(png))
            {
                extContains = png;
            }

            var imagewithoutExt = imagen.result.Replace("data:" + extContains + ";base64,", string.Empty);
            var filebytes = Convert.FromBase64String(imagewithoutExt);

            if (filebytes.Length > 0)
            {
                using (var stream = new FileStream(directorio + nombre, FileMode.Create))
                {
                    stream.Write(filebytes);
                }
            }
        }
    }
}
