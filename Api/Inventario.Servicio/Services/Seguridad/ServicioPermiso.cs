﻿using System.Collections.Generic;
using Inventario.Comun.Seguridad;
using Inventario.Servicio.Contracts;
using Inventario.Servicio.Contracts.Seguridad;

namespace Inventario.Servicio.Services.Seguridad
{
    public class ServicioPermiso : Servicio, IServicioPermiso
    {
        private readonly IAccesoDatos accesoDatos;

        public ServicioPermiso(IAccesoDatos accesoDatos)
        {
            this.accesoDatos = accesoDatos;
        }

        public void Guardar(ModeloPermiso modelo)
        {
            this.accesoDatos.PermisoGuardar(modelo);
        }

        public IEnumerable<ModeloPermiso> ObtenerTodos()
        {
            var valorRetorno = this.accesoDatos.PermisoObtenerTodos();

            return valorRetorno;
        }
    }
}