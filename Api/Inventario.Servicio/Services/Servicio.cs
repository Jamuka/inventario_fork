﻿using System;
using System.Diagnostics;
using Inventario.Servicio.Contracts;

namespace Inventario.Servicio.Services
{
    public class Servicio : IServicio
    {
        private Guid guid;

        public Servicio()
            : this(Guid.NewGuid())
        {

        }

        public Servicio(Guid guid)
        {
            this.IdOperacion = guid;
        }

        public Guid IdOperacion
        {
            get
            {
                Trace.WriteLine(string.Format("{0} Obtener - {1}", this.GetType().Name, this.guid));
                return this.guid;
            }
            private set
            {
                this.guid = value;
                Trace.WriteLine(string.Format("{0} Guardar - {1}", this.GetType().Name, this.guid));
            }
        }
    }
}