﻿using System.Collections.Generic;
using Inventario.Comun.Inventario;
using Inventario.Servicio.Contracts;
using Inventario.Servicio.Contracts.Inventario;

namespace Inventario.Servicio.Services.Inventario
{
    public class ServicioEquipo : Servicio, IServicioEquipo
    {
        private readonly IAccesoDatos accesoDatos;

        public ServicioEquipo(IAccesoDatos accesoDatos)
        {
            this.accesoDatos = accesoDatos;
        }

        public void Guardar(ModeloEquipo modelo, int idUsuarioCaptura)
        {
            this.accesoDatos.EquipoGuardar(modelo, idUsuarioCaptura);
        }

        public ModeloEquipo Obtener(int idEquipo)
        {
            var valorRetorno = this.accesoDatos.EquipoObtener(idEquipo);

            return valorRetorno;
        }

        public IEnumerable<ModeloEquipo> ObtenerTodos(int? idSucursal, int? idUsuario)
        {
            var valorRetorno = this.accesoDatos.EquipoObtenerTodos(idSucursal, idUsuario);

            return valorRetorno;
        }

        public IEnumerable<ModeloEquipo> RegistroObtenerTodos(int idEquipo)
        {
            var valorRetorno = this.accesoDatos.EquipoRegistroObtenerTodos(idEquipo);

            return valorRetorno;
        }
    }
}
