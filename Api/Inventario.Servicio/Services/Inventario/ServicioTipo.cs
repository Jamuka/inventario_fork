﻿using System.Collections.Generic;
using Inventario.Comun.Inventario;
using Inventario.Servicio.Contracts;
using Inventario.Servicio.Contracts.Inventario;

namespace Inventario.Servicio.Services.Inventario
{
    public class ServicioTipo : Servicio, IServicioTipo
    {
        private readonly IAccesoDatos accesoDatos;

        public ServicioTipo(IAccesoDatos accesoDatos)
        {
            this.accesoDatos = accesoDatos;
        }

        public void Guardar(ModeloTipo modelo)
        {
            this.accesoDatos.TipoGuardar(modelo);
        }

        public IEnumerable<ModeloTipo> ObtenerTodos()
        {
            var valorRetorno = this.accesoDatos.TipoObtenerTodos();

            return valorRetorno;
        }

        public void CaracteristicaGuardar(ModeloTipo modelo)
        {
            this.accesoDatos.TipoCaracteristicaGuardar(modelo);
        }

        public ModeloTipo CaracteristicaObtenerTodos(int idTipo)
        {
            var valorRetorno = this.accesoDatos.TipoCaracteristicaObtenerTodos(idTipo);

            return valorRetorno;
        }
    }
}