﻿using System.Collections.Generic;
using Inventario.Comun.Inventario;
using Inventario.Servicio.Contracts;
using Inventario.Servicio.Contracts.Inventario;

namespace Inventario.Servicio.Services.Inventario
{
    public class ServicioCaracteristica : Servicio, IServicioCaracteristica
    {
        private readonly IAccesoDatos accesoDatos;

        public ServicioCaracteristica(IAccesoDatos accesoDatos)
        {
            this.accesoDatos = accesoDatos;
        }

        public void Guardar(ModeloCaracteristica modelo)
        {
            this.accesoDatos.CaracteristicaGuardar(modelo);
        }

        public void Eliminar(int idCaracteristica)
        {
            this.accesoDatos.CaracteristicaEliminar(idCaracteristica);
        }

        public IEnumerable<ModeloCaracteristica> ObtenerTodos()
        {
            var valorRetorno = this.accesoDatos.CaracteristicaObtenerTodos();

            return valorRetorno;
        }
    }
}