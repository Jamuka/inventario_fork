using System;
using System.IO;
using System.Linq;
using System.Text;
using Inventario.Servicio.Contracts;
using Inventario.Servicio.Contracts.Inventario;
using Inventario.Servicio.Contracts.Seguridad;
using Inventario.Servicio.Contracts.Ubicacion;
using Inventario.Servicio.Models;
using Inventario.Servicio.Services;
using Inventario.Servicio.Services.Inventario;
using Inventario.Servicio.Services.Seguridad;
using Inventario.Servicio.Services.Ubicacion;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Cors.Infrastructure;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.FileProviders;
using Microsoft.Extensions.Hosting;
using Microsoft.IdentityModel.Tokens;
using Newtonsoft.Json;
using NSwag;
using NSwag.Generation.Processors.Security;

namespace Inventario.Servicio
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
            Constructor.Instancia.InicializaConfiguracion(configuration);
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            //services.AddCors(); // Se agrego

            services.AddScoped<IAccesoDatos, AccesoDatos>();

            services.AddSingleton<IHttpContextAccessor, HttpContextAccessor>();
            // -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- Tablero
            services.AddScoped<IServicioTablero, ServicioTablero>();
            // -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- Inventario
            services.AddScoped<IServicioCaracteristica, ServicioCaracteristica>();
            services.AddScoped<IServicioEquipo, ServicioEquipo>();
            services.AddScoped<IServicioEstado, ServicioEstado>();
            services.AddScoped<IServicioHistoria, ServicioHistoria>();            
            services.AddScoped<IServicioTipo, ServicioTipo>();

            // -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- Seguridad
            services.AddScoped<IServicioPermiso, ServicioPermiso>();
            services.AddScoped<IServicioRol, ServicioRol>();
            services.AddScoped<IServicioUsuario, ServicioUsuario>();

            // -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- Ubicacion
            services.AddScoped<IServicioEmpresa, ServicioEmpresa>();
            services.AddScoped<IServicioSucursal, ServicioSucursal>();

            /*services.AddCors(options =>
            {
                options.AddPolicy("CorsPolicy",
                    builder => builder.WithOrigins("*")
                    .AllowAnyMethod()
                    .AllowAnyHeader());
            });*/
            services.AddCors(options =>
            {
                options.AddPolicy("CorsPolicy",
                    policyBuilder => policyBuilder.WithOrigins("*")//("http://localhost:4200", "https://inventariosite.azurewebsites.net")
                        .SetIsOriginAllowedToAllowWildcardSubdomains()
                        .AllowAnyMethod()
                        .AllowAnyHeader());
                        //.AllowCredentials()); ;
            });

            services.AddControllers();

            services.AddMvc().AddNewtonsoftJson(options => { options.SerializerSettings.ReferenceLoopHandling = ReferenceLoopHandling.Ignore; });

            // JWT
            var key = Encoding.UTF8.GetBytes(Constructor.Instancia.ObtenerConfiguracion().ClaveSecreta);
            services.AddAuthentication(x =>
            {
                x.DefaultAuthenticateScheme = JwtBearerDefaults.AuthenticationScheme;
                x.DefaultChallengeScheme = JwtBearerDefaults.AuthenticationScheme;
                x.DefaultScheme = JwtBearerDefaults.AuthenticationScheme;
            }).AddJwtBearer(x => {
                x.RequireHttpsMetadata = false;
                x.SaveToken = false;
                x.TokenValidationParameters = new TokenValidationParameters
                {
                    ValidateIssuerSigningKey = true,
                    IssuerSigningKey = new SymmetricSecurityKey(key),
                    ValidateIssuer = false,
                    ValidateAudience = false,
                    ClockSkew = TimeSpan.Zero
                };
            });

            // REGISTRAMOS SWAGGER COMO SERVICIO
            services.AddOpenApiDocument(document =>
            {
                document.Title = "Inventario API";
                document.Description = "Web API para el servicio de inventario.";

                // CONFIGURAMOS LA SEGURIDAD JWT PARA SWAGGER,
                // PERMITE AÑADIR EL TOKEN JWT A LA CABECERA.
                document.AddSecurity("JWT", Enumerable.Empty<string>(),
                    new OpenApiSecurityScheme
                    {
                        Type = OpenApiSecuritySchemeType.ApiKey,
                        Name = "Authorization",
                        In = OpenApiSecurityApiKeyLocation.Header,
                        Description = "Busca el metodo Entrar y logueate con el usuario/password Admin/123 elimina el campo sucursal, ejecutalo y copia y pega el Token en el campo 'Value:' así: Bearer {Token JWT}."
                    }
                );

                document.OperationProcessors.Add(new AspNetCoreOperationSecurityScopeProcessor("JWT"));
            });
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env, ICorsService corsService, ICorsPolicyProvider corsPolicyProvider)
        {

            if(!Directory.Exists(Path.Combine(Directory.GetCurrentDirectory(), "Recursos")))
            {
                Directory.CreateDirectory(Path.Combine(Directory.GetCurrentDirectory(), "Recursos"));
            }

            app.UseStaticFiles(new StaticFileOptions
            {
                FileProvider = new PhysicalFileProvider(Path.Combine(Directory.GetCurrentDirectory(), "Recursos")),                
                RequestPath = "/Recursos",
                ServeUnknownFileTypes = true,
                OnPrepareResponse = (ctx) =>
                {
                    var policy = corsPolicyProvider.GetPolicyAsync(ctx.Context, "CorsPolicy")
                        .ConfigureAwait(false)
                        .GetAwaiter().GetResult();

                    var corsResult = corsService.EvaluatePolicy(ctx.Context, policy);

                    corsService.ApplyResult(corsResult, ctx.Context.Response);
                }
            });

            if (env.IsDevelopment())
             {
                 app.UseDeveloperExceptionPage();
             }

            app.UseHttpsRedirection();
            app.UseRouting();

            app.UseCors("CorsPolicy");

            app.UseAuthentication();
            app.UseAuthorization();

            app.UseEndpoints(x => x.MapControllers());

            // AÑADIMOS EL MIDDLEWARE DE SWAGGER (NSwag)
            app.UseOpenApi();
            app.UseSwaggerUi3();
        }
    }
}
