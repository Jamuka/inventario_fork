﻿using System;
using System.Collections.Generic;
using System.Data;
using Inventario.Comun.Seguridad;

namespace Inventario.Servicio.Models.TipoTabla
{
    public class TUsuarioRol : DataTable
    {
        public TUsuarioRol()
        {
            this.Columns.Add(AccesoDatos.ProcedimientosAlmacenados.Campos.IdUsuario, type: System.Type.GetType("System.Int32"));
            this.Columns.Add(AccesoDatos.ProcedimientosAlmacenados.Campos.IdRol, type: System.Type.GetType("System.Int32"));
        }

        public void Add(int idUsuario, IEnumerable<ModeloRol> registros)
        {
            this.Clear();
            foreach (var registro in registros)
            {
                if (registro.Asignado.HasValue && registro.Asignado.Value)
                {
                    var registroNuevo = this.NewRow();
                    registroNuevo[AccesoDatos.ProcedimientosAlmacenados.Campos.IdUsuario] = idUsuario;
                    registroNuevo[AccesoDatos.ProcedimientosAlmacenados.Campos.IdRol] = registro.IdRol;
                    this.Rows.Add(registroNuevo);
                }
            }
        }
    }
}
