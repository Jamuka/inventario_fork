﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Runtime.Serialization;
using Inventario.Comun.Inventario;

namespace Inventario.Servicio.Models.TipoTabla
{
    [Serializable]
    public class TEquipoCaracteristica : DataTable
    {
        protected TEquipoCaracteristica(SerializationInfo info, StreamingContext context)
        {

        }

        public TEquipoCaracteristica()
        {
            this.Columns.Add(AccesoDatos.ProcedimientosAlmacenados.Campos.IdCaracteristica, type: System.Type.GetType("System.Int32"));
            this.Columns.Add(AccesoDatos.ProcedimientosAlmacenados.Campos.Valor, type: System.Type.GetType("System.String"));
        }

        public void Add(int idTipo, IEnumerable<ModeloCaracteristica> registros)
        {
            this.Clear();
            if (registros != null)
            {
                foreach (var registro in registros)
                {
                    if (!string.IsNullOrEmpty(registro.Valor))
                    {
                        var registroNuevo = this.NewRow();
                        registroNuevo[AccesoDatos.ProcedimientosAlmacenados.Campos.IdCaracteristica] = registro.IdCaracteristica;
                        registroNuevo[AccesoDatos.ProcedimientosAlmacenados.Campos.Valor] = registro.Valor;
                        this.Rows.Add(registroNuevo);
                    }
                }
            }
        }
    }
}
