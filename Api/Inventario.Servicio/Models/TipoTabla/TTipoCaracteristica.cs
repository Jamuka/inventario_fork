﻿using System.Collections.Generic;
using System.Data;
using Inventario.Comun.Inventario;

namespace Inventario.Servicio.Models.TipoTabla
{
    public class TTipoCaracteristica : DataTable
    {
        public TTipoCaracteristica()
        {
            this.Columns.Add(AccesoDatos.ProcedimientosAlmacenados.Campos.IdTipo, type: System.Type.GetType("System.Int32"));
            this.Columns.Add(AccesoDatos.ProcedimientosAlmacenados.Campos.IdCaracteristica, type: System.Type.GetType("System.Int32"));            
        }

        public void Add(int idTipo, IEnumerable<ModeloCaracteristica> registros)
        {
            this.Clear();
            foreach (var registro in registros)
            {
                if (registro.Asignado.HasValue && registro.Asignado.Value)
                {
                    var registroNuevo = this.NewRow();
                    registroNuevo[AccesoDatos.ProcedimientosAlmacenados.Campos.IdTipo] = idTipo;
                    registroNuevo[AccesoDatos.ProcedimientosAlmacenados.Campos.IdCaracteristica] = registro.IdCaracteristica;
                    this.Rows.Add(registroNuevo);
                }
            }
        }
    }
}
