﻿using System.Collections.Generic;
using System.Data;
using Inventario.Comun.Seguridad;

namespace Inventario.Servicio.Models.TipoTabla
{
    public class TRolPermiso : DataTable
    {
        public TRolPermiso()
        {
            this.Columns.Add(AccesoDatos.ProcedimientosAlmacenados.Campos.IdRol, type: System.Type.GetType("System.Int32"));
            this.Columns.Add(AccesoDatos.ProcedimientosAlmacenados.Campos.IdPermiso, type: System.Type.GetType("System.Int32"));            
        }

        public void Add(int idRol, IEnumerable<ModeloPermiso> registros)
        {
            this.Clear();
            foreach (var registro in registros)
            {
                if (registro.Concedido.HasValue && registro.Concedido.Value)
                {
                    var registroNuevo = this.NewRow();
                    registroNuevo[AccesoDatos.ProcedimientosAlmacenados.Campos.IdRol] = idRol;
                    registroNuevo[AccesoDatos.ProcedimientosAlmacenados.Campos.IdPermiso] = registro.IdPermiso;
                    this.Rows.Add(registroNuevo);
                }
            }
        }
    }
}
