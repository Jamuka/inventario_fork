﻿using System;
using System.Collections.Generic;
using System.Data;
using Inventario.Comun.Seguridad;
using Inventario.Libreria;

namespace Inventario.Servicio.Models
{
    public partial class AccesoDatos
    {
        internal partial class ProcedimientosAlmacenados
        {
            internal const string PermisoObtenerTodos = "[Seguridad].[PermisoObtenerTodos]";
            internal const string PermisoGuardar = "[Seguridad].[PermisoGuardar]";

            internal partial class Campos
            {
                internal const string IdPermiso = "IdPermiso";
                internal const string IdPermisoPadre = "IdPermisoPadre";
                internal const string Texto = "Texto";
                internal const string Icono = "Icono";
                internal const string Orden = "Orden";
                internal const string Concedido = "Concedido";
            }
        }

        public IEnumerable<ModeloPermiso> PermisoObtenerTodos()
        {
            List<ModeloPermiso> valorRetorno = null;

            using (var comando = base.ObtenerComando(ProcedimientosAlmacenados.PermisoObtenerTodos))
            {
                var tabla = comando.ObtenerTabla();

                if (tabla.Rows.Count > 0)
                {
                    valorRetorno = new List<ModeloPermiso>();
                    for (var i = 0; i < tabla.Rows.Count; i++)
                    {
                        var registro = tabla.Rows[i];

                        valorRetorno.Add(this.RegistroAModeloPermiso(registro));
                    }
                }
            }
            return valorRetorno;
        }

        public void PermisoGuardar(ModeloPermiso modelo)
        {
            var parametros = this.ModeloCatalogoADiccionario(modelo);
            parametros.Add(ProcedimientosAlmacenados.Campos.IdPermiso, modelo.IdPermiso);
            parametros.Add(ProcedimientosAlmacenados.Campos.IdPermisoPadre, modelo.IdPermisoPadre);
            parametros.Add(ProcedimientosAlmacenados.Campos.Texto, modelo.Texto);
            parametros.Add(ProcedimientosAlmacenados.Campos.Icono, modelo.Icono);
            parametros.Add(ProcedimientosAlmacenados.Campos.Orden, modelo.Orden);

            using (var comando = base.ObtenerComando(ProcedimientosAlmacenados.PermisoGuardar))
            {
                comando.EjecutarSinConsultar(parametros.ADiccionarioSoloLectura());
            }
        }

        private ModeloPermiso RegistroAModeloPermiso(DataRow registro)
        {
            var modeloCatalogo = this.RegistroAModeloCatalogo(registro);
            var valorRetorno = new ModeloPermiso
            {
                Nombre = modeloCatalogo.Nombre,
                Activo = modeloCatalogo.Activo,
                FechaInsercion = modeloCatalogo.FechaInsercion,
                FechaActualizacion = modeloCatalogo.FechaActualizacion
            };

            valorRetorno.IdPermiso = this.ObtenerValorCampo<int>(registro, ProcedimientosAlmacenados.Campos.IdPermiso);
            valorRetorno.IdPermisoPadre = registro[ProcedimientosAlmacenados.Campos.IdPermisoPadre] != DBNull.Value ?
                                          this.ObtenerValorCampo<int?>(registro, ProcedimientosAlmacenados.Campos.IdPermisoPadre) : null;
            valorRetorno.Texto = this.ObtenerValorCampo<string>(registro, ProcedimientosAlmacenados.Campos.Texto);
            valorRetorno.Icono = this.ObtenerValorCampo<string>(registro, ProcedimientosAlmacenados.Campos.Icono);
            valorRetorno.Orden = this.ObtenerValorCampo<int>(registro, ProcedimientosAlmacenados.Campos.Orden);
            valorRetorno.Concedido = registro.Table.Columns.Contains(ProcedimientosAlmacenados.Campos.Concedido) ?
                                        this.ObtenerValorCampo<bool?>(registro, ProcedimientosAlmacenados.Campos.Concedido) : null;
            return valorRetorno;
        }
    }
}