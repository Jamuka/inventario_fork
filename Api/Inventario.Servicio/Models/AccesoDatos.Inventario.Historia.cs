﻿using System;
using System.Collections.Generic;
using System.Data;
using Inventario.Comun.Inventario;
using Inventario.Libreria;

namespace Inventario.Servicio.Models
{
    public partial class AccesoDatos
    {
        internal partial class ProcedimientosAlmacenados
        {
            internal const string HistoriaObtenerTodos = "[Inventario].[HistoriaObtenerTodos]";
            internal const string HistoriaGuardar = "[Inventario].[HistoriaGuardar]";

            internal partial class Campos
            {
                internal const string IdHistoria = "IdHistoria";
                internal const string Comentarios = "Comentarios";
            }
        }

        public IEnumerable<ModeloHistoria> HistoriaObtenerTodos(int? idSucursal)
        {
            List<ModeloHistoria> valorRetorno = null;
            var parametros = new Dictionary<string, object>
            {
                { ProcedimientosAlmacenados.Campos.IdSucursal, idSucursal }
            };

            using (var comando = base.ObtenerComando(ProcedimientosAlmacenados.HistoriaObtenerTodos))
            {
                var tabla = comando.ObtenerTabla(parametros.ADiccionarioSoloLectura());

                if (tabla.Rows.Count > 0)
                {
                    valorRetorno = new List<ModeloHistoria>();
                    for (var i = 0; i < tabla.Rows.Count; i++)
                    {
                        var registro = tabla.Rows[i];

                        valorRetorno.Add(this.RegistroAModeloHistoria(registro));
                    }
                }
            }
            return valorRetorno;
        }

        public void HistoriaGuardar(ModeloHistoria modelo)
        {
            var parametros = new Dictionary<string, object>
            {
                { ProcedimientosAlmacenados.Campos.IdHistoria, modelo.IdHistoria },
                { ProcedimientosAlmacenados.Campos.IdUsuario, modelo.IdUsuario },
                { ProcedimientosAlmacenados.Campos.IdEquipo, modelo.IdEquipo },
                { ProcedimientosAlmacenados.Campos.IdSucursal, modelo.IdSucursal },
                { ProcedimientosAlmacenados.Campos.Comentarios, modelo.Comentarios }
            };

            using (var comando = base.ObtenerComando(ProcedimientosAlmacenados.HistoriaGuardar))
            {
                comando.EjecutarSinConsultar(parametros.ADiccionarioSoloLectura());
            }
        }

        private ModeloHistoria RegistroAModeloHistoria(DataRow registro)
        {           
            var valorRetorno = new ModeloHistoria
            {
                IdHistoria = this.ObtenerValorCampo<int>(registro, ProcedimientosAlmacenados.Campos.IdHistoria),
                IdUsuario = this.ObtenerValorCampo<int>(registro, ProcedimientosAlmacenados.Campos.IdUsuario),
                IdEquipo =  this.ObtenerValorCampo<int>(registro, ProcedimientosAlmacenados.Campos.IdEquipo),
                IdSucursal = this.ObtenerValorCampo<int>(registro, ProcedimientosAlmacenados.Campos.IdSucursal),
                Comentarios = this.ObtenerValorCampo<string>(registro, ProcedimientosAlmacenados.Campos.Comentarios),
                FechaInsercion = this.ObtenerValorCampo<DateTime>(registro, ProcedimientosAlmacenados.Campos.FechaInsercion)
            };

            return valorRetorno;
        }

    }
}
