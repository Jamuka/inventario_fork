﻿namespace Inventario.Servicio.Models
{
    public class Configuracion
    {
        public string ClaveSecreta
        {
            get;
            set;
        }

        public string CadenaConexion
        {
            get;
            set;
        }
    }
}
