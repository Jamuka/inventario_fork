﻿using System.Collections.Generic;
using System.Data;
using Inventario.Comun.Seguridad;
using Inventario.Libreria;
using Inventario.Servicio.Models.TipoTabla;

namespace Inventario.Servicio.Models
{
    public partial class AccesoDatos
    {
        internal partial class ProcedimientosAlmacenados
        {
            internal const string RolObtenerTodos = "[Seguridad].[RolObtenerTodos]";
            internal const string RolGuardar = "[Seguridad].[RolGuardar]";
            internal const string RolPermisoObtenerTodos = "[Seguridad].[RolPermisoObtenerTodos]";
            internal const string RolPermisoGuardar = "[Seguridad].[RolPermisoGuardar]";

            internal partial class Campos
            {
                internal const string IdRol = "IdRol";
                internal const string Permisos = "Permisos";
                internal const string Asignado = "Asignado";
            }
        }

        public IEnumerable<ModeloRol> RolObtenerTodos()
        {
            List<ModeloRol> valorRetorno = null;

            using (var comando = base.ObtenerComando(ProcedimientosAlmacenados.RolObtenerTodos))
            {
                var tabla = comando.ObtenerTabla();

                if (tabla.Rows.Count > 0)
                {
                    valorRetorno = new List<ModeloRol>();
                    for (var i = 0; i < tabla.Rows.Count; i++)
                    {
                        var registro = tabla.Rows[i];

                        valorRetorno.Add(this.RegistroAModeloRol(registro));
                    }
                }
            }
            return valorRetorno;
        }

        public void RolGuardar(ModeloRol modelo)
        {
            var parametros = this.ModeloCatalogoADiccionario(modelo);
            parametros.Add(ProcedimientosAlmacenados.Campos.IdRol, modelo.IdRol);

            using (var comando = base.ObtenerComando(ProcedimientosAlmacenados.RolGuardar))
            {
                comando.EjecutarSinConsultar(parametros.ADiccionarioSoloLectura());
            }
        }

        public ModeloRol RolPermisoObtenerTodos(int idRol)
        {
            ModeloRol valorRetorno = null;
            var parametros = new Dictionary<string, object>
            {
                { ProcedimientosAlmacenados.Campos.IdRol, idRol }
            };

            using (var comando = this.ObtenerComando(ProcedimientosAlmacenados.RolPermisoObtenerTodos))
            {
                var tablas = comando.ObtenerTablas(parametros.ADiccionarioSoloLectura());

                if (tablas.Count == 2)
                {
                    var tablaRol = tablas[1];
                    var tablaPermiso = tablas[2];

                    if (tablaRol.Rows.Count > 0)
                    {
                        valorRetorno = this.RegistroAModeloRol(tablaRol.Rows[0]);
                    }

                    if (tablaPermiso.Rows.Count > 0)
                    {
                        valorRetorno.Permisos = new List<ModeloPermiso>();
                        for (var i = 0; i < tablaPermiso.Rows.Count; i++)
                        {
                            var registro = tablaPermiso.Rows[i];

                            valorRetorno.Permisos.Add(this.RegistroAModeloPermiso(registro));
                        }
                    }
                }
            }
            return valorRetorno;
        }

        public void RolPermisoGuardar(ModeloRol modelo)
        {
            using (var permisos = new TRolPermiso())
            {
                permisos.Add(modelo.IdRol, modelo.Permisos);
                var parametros = new Dictionary<string, object>
                {
                    { ProcedimientosAlmacenados.Campos.Permisos, permisos }
                };

                using (var comando = base.ObtenerComando(ProcedimientosAlmacenados.RolPermisoGuardar))
                {
                    comando.EjecutarSinConsultar(parametros.ADiccionarioSoloLectura());
                }
            }
        }

        private ModeloRol RegistroAModeloRol(DataRow registro)
        {
            var modeloCatalogo = this.RegistroAModeloCatalogo(registro);
            var returnValue = new ModeloRol
            {
                Nombre = modeloCatalogo.Nombre,
                Activo = modeloCatalogo.Activo,
                FechaInsercion = modeloCatalogo.FechaInsercion,
                FechaActualizacion = modeloCatalogo.FechaActualizacion
            };

            returnValue.IdRol = this.ObtenerValorCampo<int>(registro, ProcedimientosAlmacenados.Campos.IdRol);
            returnValue.Asignado = registro.Table.Columns.Contains(ProcedimientosAlmacenados.Campos.Asignado) ?
                                        this.ObtenerValorCampo<bool?>(registro, ProcedimientosAlmacenados.Campos.Asignado) : null;
            return returnValue;
        }
    }
}
