﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Diagnostics;
using Inventario.Comun;
using Inventario.Servicio.Contracts;

namespace Inventario.Servicio.Models
{
    public partial class AccesoDatos : Libreria.AccesoDatos, IAccesoDatos
    {
        private Guid guid;

        internal partial class ProcedimientosAlmacenados
        {
            internal partial class Campos
            {
                internal const string Nombre = "Nombre";
                internal const string Activo = "Activo";
                internal const string FechaInsercion = "FechaInsercion";
                internal const string FechaActualizacion = "FechaActualizacion";
            }
        }

        public AccesoDatos()
            : this(Guid.NewGuid())
        {
        }

        internal AccesoDatos(Guid guid)
            : this(new List<string>())
        {
            this.IdOperacion = guid;
        }

        internal AccesoDatos(string procedimientoAlmacenado)
            : this(new List<string>() { procedimientoAlmacenado })
        {

        }

        internal AccesoDatos(List<string> procedimientosAlmacenados)
        : base(Constructor.Instancia.ObtenerConfiguracion().CadenaConexion,
              Constructor.Instancia.ObtenerCadenaConexion(Constructor.Instancia.ObtenerConfiguracion().CadenaConexion),
              procedimientosAlmacenados)
        {
        }

        public Guid IdOperacion
        {
            get
            {
                Trace.WriteLine(string.Format("{0} Obtener - {1}", this.GetType().Name, this.guid));
                return this.guid;
            }
            private set
            {
                this.guid = value;
                Trace.WriteLine(string.Format("{0} Guardar - {1}", this.GetType().Name, this.guid));
            }
        }

        private ModeloCatalogo RegistroAModeloCatalogo(DataRow registro)
        {
            var nombre = this.ObtenerValorCampo<string>(registro, ProcedimientosAlmacenados.Campos.Nombre);
            var activo = this.ObtenerValorCampo<bool>(registro, ProcedimientosAlmacenados.Campos.Activo);
            var fechaInsercion = this.ObtenerValorCampo<DateTime>(registro, ProcedimientosAlmacenados.Campos.FechaInsercion);
            var fechaActualizacion = this.ObtenerValorCampo<DateTime>(registro, ProcedimientosAlmacenados.Campos.FechaActualizacion);

            var valorRetorno = new ModeloCatalogo
            {
                Nombre = nombre,
                Activo = activo,
                FechaInsercion = fechaInsercion,
                FechaActualizacion = fechaActualizacion
            };

            return valorRetorno;
        }

        private Dictionary<string, object> ModeloCatalogoADiccionario(ModeloCatalogo modelo)
        {
            var valorRetorno = new Dictionary<string, object>
            {
                { ProcedimientosAlmacenados.Campos.Nombre, modelo.Nombre },
                { ProcedimientosAlmacenados.Campos.Activo, modelo.Activo }
            };

            return valorRetorno;
        }
    }
}