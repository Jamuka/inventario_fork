﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using Inventario.Comun.Inventario;
using Inventario.Comun.Seguridad;
using Inventario.Libreria;
using Inventario.Servicio.Models.TipoTabla;

namespace Inventario.Servicio.Models
{
    public partial class AccesoDatos
    {
        internal partial class ProcedimientosAlmacenados
        {
            internal const string UsuarioObtenerTodos = "[Seguridad].[UsuarioObtenerTodos]";
            internal const string UsuarioGuardar = "[Seguridad].[UsuarioGuardar]";
            internal const string UsuarioEliminar = "[Seguridad].[UsuarioEliminar]";
            internal const string UsuarioObtener = "[Seguridad].[UsuarioObtener]";
            internal const string UsuarioEntrar = "[Seguridad].[UsuarioEntrar]";
            internal const string UsuarioSalir = "[Seguridad].[UsuarioSalir]";
            internal const string UsuarioRolObtenerTodos = "[Seguridad].[UsuarioRolObtenerTodos]";
            internal const string UsuarioRolGuardar = "[Seguridad].[UsuarioRolGuardar]";

            internal partial class Campos
            {
                internal const string IdUsuario = "IdUsuario";
                internal const string Identificacion = "Identificacion";
                internal const string Correo = "Correo";
                internal const string Rut = "Rut";
                internal const string Contrasena = "Contrasena";
                internal const string IdSesion = "IdSesion";
                internal const string DireccionIp = "DireccionIp";
                internal const string Roles = "Roles";
            }
        }

        public IEnumerable<ModeloUsuario> UsuarioObtenerTodos(int? idSucursal)
        {
            List<ModeloUsuario> valorRetorno = null;
            var parametros = new Dictionary<string, object>
            {
                { ProcedimientosAlmacenados.Campos.IdSucursal, idSucursal }
            };

            using (var comando = base.ObtenerComando(ProcedimientosAlmacenados.UsuarioObtenerTodos))
            {
                var tablas = comando.ObtenerTablas(parametros.ADiccionarioSoloLectura());

                if(tablas.Count == 4)
                {
                    var tablaUsuario = tablas[1];
                    var tablaRol = tablas[2];
                    var tablaUsuarioRol = tablas[3];
                    var tablaEquipo = tablas[4];

                    if (tablaUsuario.Rows.Count > 0)
                    {
                        valorRetorno = new List<ModeloUsuario>();
                        for (var i = 0; i < tablaUsuario.Rows.Count; i++)
                        {
                            valorRetorno.Add(this.RegistroAModeloUsuario(tablaUsuario.Rows[i]));
                        }

                        var roles = new List<ModeloRol>();
                        if (tablaRol.Rows.Count > 0)
                        {
                            for (var i = 0; i < tablaRol.Rows.Count; i++)
                            {
                                roles.Add(this.RegistroAModeloRol(tablaRol.Rows[i]));
                            }
                        }

                        if (tablaUsuarioRol.Rows.Count > 0)
                        {
                            for(var i = 0; i < tablaUsuarioRol.Rows.Count; i++)
                            {
                                var idUsuario = this.ObtenerValorCampo<int>(tablaUsuarioRol.Rows[i], ProcedimientosAlmacenados.Campos.IdUsuario);
                                var idRol = this.ObtenerValorCampo<int>(tablaUsuarioRol.Rows[i], ProcedimientosAlmacenados.Campos.IdRol);

                                var usuario = valorRetorno.FirstOrDefault(u => u.IdUsuario == idUsuario);
                                if(usuario != null)
                                {
                                    var rol = roles.FirstOrDefault(r=>r.IdRol==idRol);
                                    if(rol != null)
                                    {
                                        usuario.Roles.Add(rol);
                                    }
                                }
                            }                            
                        }

                        var equipos = new List<ModeloEquipo>();
                        if (tablaEquipo.Rows.Count > 0)
                        {
                            for (var i = 0; i < tablaEquipo.Rows.Count; i++)
                            {
                                equipos.Add(this.RegistroAModeloEquipo(tablaEquipo.Rows[i]));
                            }

                            foreach(var usuario in valorRetorno)
                            {
                                usuario.Equipos = equipos.Where(e => e.IdUsuario == usuario.IdUsuario).ToList();
                            }
                        }
                    }
                }                
            }
            return valorRetorno;
        }

        public void UsuarioGuardar(ModeloUsuario modelo)
        {
            var parametros = this.ModeloCatalogoADiccionario(modelo);
            parametros.Add(ProcedimientosAlmacenados.Campos.IdUsuario, modelo.IdUsuario);
            parametros.Add(ProcedimientosAlmacenados.Campos.IdSucursal, modelo.IdSucursal);
            parametros.Add(ProcedimientosAlmacenados.Campos.Identificacion, modelo.Identificacion);
            parametros.Add(ProcedimientosAlmacenados.Campos.Correo, modelo.Correo);
            parametros.Add(ProcedimientosAlmacenados.Campos.Rut, modelo.Rut);
            parametros.Add(ProcedimientosAlmacenados.Campos.Contrasena, modelo.Contrasena);

            using (var comando = base.ObtenerComando(ProcedimientosAlmacenados.UsuarioGuardar))
            {
                comando.EjecutarSinConsultar(parametros.ADiccionarioSoloLectura());
            }
        }

        public void UsuarioEliminar(int idUsuario)
        {
            var parametros = new Dictionary<string, object>()
            {
                { ProcedimientosAlmacenados.Campos.IdUsuario, idUsuario }
            };

            using (var comando = base.ObtenerComando(ProcedimientosAlmacenados.UsuarioEliminar))
            {
                comando.EjecutarSinConsultar(parametros.ADiccionarioSoloLectura());
            }
        }

        public ModeloUsuario UsuarioObtener(string identificacion)
        {
            ModeloUsuario valorRetorno = null;

            var parametros = new Dictionary<string, object>
            {
                { ProcedimientosAlmacenados.Campos.Identificacion, identificacion }
            };

            using (var comando = base.ObtenerComando(ProcedimientosAlmacenados.UsuarioObtener))
            {
                var tablas = comando.ObtenerTablas(parametros.ADiccionarioSoloLectura());

                if (tablas.Count == 3)
                {
                    var tablaUsuario = tablas[1];
                    var tablaPermiso = tablas[2];
                    var tablaRol = tablas[3];

                    if (tablaUsuario.Rows.Count > 0)
                    {
                        valorRetorno = this.RegistroAModeloUsuario(tablaUsuario.Rows[0]);
                    }

                    if (tablaPermiso.Rows.Count > 0)
                    {
                        valorRetorno.Permisos = new List<ModeloPermiso>();
                        for (var i = 0; i < tablaPermiso.Rows.Count; i++)
                        {
                            var registro = tablaPermiso.Rows[i];

                            valorRetorno.Permisos.Add(this.RegistroAModeloPermiso(registro));
                        }

                        for (var i = 0; i < valorRetorno.Permisos.Count; i++)
                        {
                            var hijos = valorRetorno.Permisos.Where(p => p.IdPermisoPadre == valorRetorno.Permisos[i].IdPermiso).ToList();

                            if (hijos != null && hijos.Any())
                            {
                                valorRetorno.Permisos[i].Hijos = hijos;
                            }
                        }
                    }

                    if (tablaRol.Rows.Count > 0)
                    {
                        valorRetorno.Roles = new List<ModeloRol>();
                        for (var i = 0; i < tablaRol.Rows.Count; i++)
                        {
                            valorRetorno.Roles.Add(this.RegistroAModeloRol(tablaRol.Rows[i]));
                        }
                    }
                }
            }
            return valorRetorno;
        }

        public void UsuarioEntrar(int idUsuario, string idSesion, string direccionIp)
        {
            var parametros = new Dictionary<string, object>
            {
                { ProcedimientosAlmacenados.Campos.IdUsuario, idUsuario },
                { ProcedimientosAlmacenados.Campos.IdSesion, idSesion },
                { ProcedimientosAlmacenados.Campos.DireccionIp, direccionIp }
            };

            using (var comando = base.ObtenerComando(ProcedimientosAlmacenados.UsuarioEntrar))
            {
                comando.EjecutarSinConsultar(parametros.ADiccionarioSoloLectura());
            }
        }

        public void UsuarioSalir(int idUsuario, string idSesion)
        {
            var parameters = new Dictionary<string, object>
            {
                { ProcedimientosAlmacenados.Campos.IdUsuario, idUsuario },
                { ProcedimientosAlmacenados.Campos.IdSesion, idSesion }
            };

            using (var comando = base.ObtenerComando(ProcedimientosAlmacenados.UsuarioSalir))
            {
                comando.EjecutarSinConsultar(parameters.ADiccionarioSoloLectura());
            }
        }

        public ModeloUsuario UsuarioRolObtenerTodos(int idUsuario)
        {
            ModeloUsuario valorRetorno = null;
            var parametros = new Dictionary<string, object>
            {
                { ProcedimientosAlmacenados.Campos.IdUsuario, idUsuario }
            };

            using (var comando = this.ObtenerComando(ProcedimientosAlmacenados.UsuarioRolObtenerTodos))
            {
                var tablas = comando.ObtenerTablas(parametros.ADiccionarioSoloLectura());

                if (tablas.Count == 2)
                {
                    var tablaUsuario = tablas[1];
                    var tablaRol = tablas[2];

                    if (tablaUsuario.Rows.Count > 0)
                    {
                        valorRetorno = this.RegistroAModeloUsuario(tablaUsuario.Rows[0]);
                    }

                    if (tablaRol.Rows.Count > 0)
                    {
                        valorRetorno.Roles = new List<ModeloRol>();
                        for (var i = 0; i < tablaRol.Rows.Count; i++)
                        {
                            var registro = tablaRol.Rows[i];

                            valorRetorno.Roles.Add(this.RegistroAModeloRol(registro));
                        }
                    }
                }
            }
            return valorRetorno;
        }

        public void UsuarioRolGuardar(ModeloUsuario modelo)
        {
            using (var roles = new TUsuarioRol())
            {
                roles.Add(modelo.IdUsuario, modelo.Roles);
                var parametros = new Dictionary<string, object>
                {
                    { ProcedimientosAlmacenados.Campos.Roles, roles }
                };

                using (var comando = base.ObtenerComando(ProcedimientosAlmacenados.UsuarioRolGuardar))
                {
                    comando.EjecutarSinConsultar(parametros.ADiccionarioSoloLectura());
                }
            }
        }

        private ModeloUsuario RegistroAModeloUsuario(DataRow registro)
        {
            var modeloCatalogo = this.RegistroAModeloCatalogo(registro);
            var valorRetorno = new ModeloUsuario
            {
                Nombre = modeloCatalogo.Nombre,
                Activo = modeloCatalogo.Activo,
                FechaInsercion = modeloCatalogo.FechaInsercion,
                FechaActualizacion = modeloCatalogo.FechaActualizacion
            };

            valorRetorno.IdUsuario = this.ObtenerValorCampo<int>(registro, ProcedimientosAlmacenados.Campos.IdUsuario);
            valorRetorno.IdSucursal = registro[ProcedimientosAlmacenados.Campos.IdSucursal] != DBNull.Value ?
                                          this.ObtenerValorCampo<int?>(registro, ProcedimientosAlmacenados.Campos.IdSucursal) : null;
            valorRetorno.Identificacion = this.ObtenerValorCampo<string>(registro, ProcedimientosAlmacenados.Campos.Identificacion);
            valorRetorno.Correo = this.ObtenerValorCampo<string>(registro, ProcedimientosAlmacenados.Campos.Correo);
            valorRetorno.Rut = this.ObtenerValorCampo<string>(registro, ProcedimientosAlmacenados.Campos.Rut);
            valorRetorno.Contrasena = this.ObtenerValorCampo<string>(registro, ProcedimientosAlmacenados.Campos.Contrasena);
            return valorRetorno;
        }
    }
}
