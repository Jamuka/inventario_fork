﻿using System.Collections.Generic;
using System.Data;
using Inventario.Comun.Inventario;
using Inventario.Libreria;

namespace Inventario.Servicio.Models
{
    public partial class AccesoDatos
    {
        internal partial class ProcedimientosAlmacenados
        {
            internal const string EstadoObtenerTodos = "[Inventario].[EstadoObtenerTodos]";
            internal const string EstadoGuardar = "[Inventario].[EstadoGuardar]";

            internal partial class Campos
            {
                internal const string IdEstado = "IdEstado";
            }
        }

        public IEnumerable<ModeloEstado> EstadoObtenerTodos()
        {
            List<ModeloEstado> valorRetorno = null;

            using (var comando = base.ObtenerComando(ProcedimientosAlmacenados.EstadoObtenerTodos))
            {
                var tabla = comando.ObtenerTabla();

                if (tabla.Rows.Count > 0)
                {
                    valorRetorno = new List<ModeloEstado>();
                    for (var i = 0; i < tabla.Rows.Count; i++)
                    {
                        var registro = tabla.Rows[i];

                        valorRetorno.Add(this.RegistroAModeloEstado(registro));
                    }
                }
            }
            return valorRetorno;
        }

        public void EstadoGuardar(ModeloEstado modelo)
        {
            var parametros = this.ModeloCatalogoADiccionario(modelo);
            parametros.Add(ProcedimientosAlmacenados.Campos.IdEstado, modelo.IdEstado);

            using (var comando = base.ObtenerComando(ProcedimientosAlmacenados.EstadoGuardar))
            {
                comando.EjecutarSinConsultar(parametros.ADiccionarioSoloLectura());
            }
        }

        private ModeloEstado RegistroAModeloEstado(DataRow registro)
        {
            var modeloCatalogo = this.RegistroAModeloCatalogo(registro);
            var valorRetorno = new ModeloEstado
            {
                Nombre = modeloCatalogo.Nombre,
                Activo = modeloCatalogo.Activo,
                FechaInsercion = modeloCatalogo.FechaInsercion,
                FechaActualizacion = modeloCatalogo.FechaActualizacion
            };

            valorRetorno.IdEstado = this.ObtenerValorCampo<int>(registro, ProcedimientosAlmacenados.Campos.IdEstado);
            return valorRetorno;
        }
    }
}