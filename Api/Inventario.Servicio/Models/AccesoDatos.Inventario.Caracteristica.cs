﻿using System.Collections.Generic;
using System.Data;
using Inventario.Comun.Inventario;
using Inventario.Libreria;

namespace Inventario.Servicio.Models
{
    public partial class AccesoDatos
    {
        internal partial class ProcedimientosAlmacenados
        {
            internal const string CaracteristicaObtenerTodos = "[Inventario].[CaracteristicaObtenerTodos]";
            internal const string CaracteristicaGuardar = "[Inventario].[CaracteristicaGuardar]";
            internal const string CaracteristicaEliminar = "[Inventario].[CaracteristicaEliminar]";

            internal partial class Campos
            {
                internal const string IdCaracteristica = "IdCaracteristica";
                internal const string Valor = "Valor";
            }
        }

        public IEnumerable<ModeloCaracteristica> CaracteristicaObtenerTodos()
        {
            List<ModeloCaracteristica> valorRetorno = null;

            using (var comando = base.ObtenerComando(ProcedimientosAlmacenados.CaracteristicaObtenerTodos))
            {
                var tabla = comando.ObtenerTabla();

                if (tabla.Rows.Count > 0)
                {
                    valorRetorno = new List<ModeloCaracteristica>();
                    for (var i = 0; i < tabla.Rows.Count; i++)
                    {
                        var registro = tabla.Rows[i];

                        valorRetorno.Add(this.RegistroAModeloCaracteristica(registro));
                    }
                }
            }
            return valorRetorno;
        }

        public void CaracteristicaGuardar(ModeloCaracteristica modelo)
        {
            var parametros = this.ModeloCatalogoADiccionario(modelo);
            parametros.Add(ProcedimientosAlmacenados.Campos.IdCaracteristica, modelo.IdCaracteristica);

            using (var comando = base.ObtenerComando(ProcedimientosAlmacenados.CaracteristicaGuardar))
            {
                comando.EjecutarSinConsultar(parametros.ADiccionarioSoloLectura());
            }
        }

        public void CaracteristicaEliminar(int idCaracteristica)
        {
            var parametros = new Dictionary<string, object>()
            {
                { ProcedimientosAlmacenados.Campos.IdCaracteristica, idCaracteristica }
            };

            using (var comando = base.ObtenerComando(ProcedimientosAlmacenados.CaracteristicaEliminar))
            {
                comando.EjecutarSinConsultar(parametros.ADiccionarioSoloLectura());
            }
        }

        private ModeloCaracteristica RegistroAModeloCaracteristica(DataRow registro)
        {
            var modeloCatalogo = this.RegistroAModeloCatalogo(registro);
            var valorRetorno = new ModeloCaracteristica
            {
                Nombre = modeloCatalogo.Nombre,
                Activo = modeloCatalogo.Activo,
                FechaInsercion = modeloCatalogo.FechaInsercion,
                FechaActualizacion = modeloCatalogo.FechaActualizacion
            };

            valorRetorno.IdCaracteristica = this.ObtenerValorCampo<int>(registro, ProcedimientosAlmacenados.Campos.IdCaracteristica);
            valorRetorno.Asignado = registro.Table.Columns.Contains(ProcedimientosAlmacenados.Campos.Asignado) ?
                                        this.ObtenerValorCampo<bool?>(registro, ProcedimientosAlmacenados.Campos.Asignado) : null;
            valorRetorno.Valor = registro.Table.Columns.Contains(ProcedimientosAlmacenados.Campos.Valor) ?
                                        this.ObtenerValorCampo<string>(registro, ProcedimientosAlmacenados.Campos.Valor) : null;
            return valorRetorno;
        }
    }
}