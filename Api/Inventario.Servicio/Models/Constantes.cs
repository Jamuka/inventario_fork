﻿namespace Inventario.Servicio.Models
{
    public class Constantes
    {
        public const string IdUsuario = "IdUsuario";
        public const string IdSesion = "IdSesion";
        public const string IdSucursal = "IdSucursal";
        public const string DirectorioImagen = "/Recursos/Imagenes/";
    }
}
