﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using Inventario.Comun.Inventario;
using Inventario.Libreria;
using Inventario.Servicio.Models.TipoTabla;

namespace Inventario.Servicio.Models
{
    public partial class AccesoDatos
    {
        internal partial class ProcedimientosAlmacenados
        {
            internal const string EquipoObtener = "[Inventario].[EquipoObtener]";
            internal const string EquipoObtenerTodos = "[Inventario].[EquipoObtenerTodos]";
            internal const string EquipoGuardar = "[Inventario].[EquipoGuardar]";
            internal const string EquipoRegistroObtenerTodos = "[Inventario].[EquipoRegistroObtenerTodos]";

            internal partial class Campos
            {
                internal const string IdEquipo = "IdEquipo";
                internal const string ActivoFijo = "ActivoFijo";
                internal const string Marca = "Marca";
                internal const string Modelo = "Modelo";
                internal const string Serie = "Serie";
                internal const string Numero = "Numero";
                internal const string FechaCompra = "FechaCompra";
                internal const string IdUsuarioCaptura = "IdUsuarioCaptura";
            }
        }

        public ModeloEquipo EquipoObtener(int idEquipo)
        {
            ModeloEquipo valorRetorno = null;
            var parametros = new Dictionary<string, object>
            {
                { ProcedimientosAlmacenados.Campos.IdEquipo, idEquipo }
            };

            using (var comando = base.ObtenerComando(ProcedimientosAlmacenados.EquipoObtener))
            {
                var tablas = comando.ObtenerTablas(parametros.ADiccionarioSoloLectura());
                if (tablas.Count == 2)
                {
                    var tablaEquipo = tablas[1];
                    var tablaCaracteristicas = tablas[2];

                    if (tablaEquipo.Rows.Count > 0)
                    {
                        valorRetorno = this.RegistroAModeloEquipo(tablaEquipo.Rows[0]);

                        if (tablaCaracteristicas.Rows.Count > 0)
                        {
                            valorRetorno.Caracteristicas = new List<ModeloCaracteristica>();
                            for (var i = 0; i < tablaCaracteristicas.Rows.Count; i++)
                            {
                                valorRetorno.Caracteristicas.Add(this.RegistroAModeloCaracteristica(tablaCaracteristicas.Rows[i]));
                            }
                        }
                    }
                }
            }
            return valorRetorno;
        }


        public IEnumerable<ModeloEquipo> EquipoObtenerTodos(int? idSucursal, int? idUsuario)
        {
            List<ModeloEquipo> valorRetorno = null;
            var parametros = new Dictionary<string, object>
            {
                { ProcedimientosAlmacenados.Campos.IdSucursal, idSucursal },
                { ProcedimientosAlmacenados.Campos.IdUsuario, idUsuario }
            };

            using (var comando = base.ObtenerComando(ProcedimientosAlmacenados.EquipoObtenerTodos))
            {
                var tablas = comando.ObtenerTablas(parametros.ADiccionarioSoloLectura());
                if (tablas.Count == 2)
                {
                    var tablaEquipos = tablas[1];
                    var tablaCaracteristicas = tablas[2];

                    if (tablaEquipos.Rows.Count > 0)
                    {
                        valorRetorno = new List<ModeloEquipo>();
                        for (var i = 0; i < tablaEquipos.Rows.Count; i++)
                        {
                            valorRetorno.Add(this.RegistroAModeloEquipo(tablaEquipos.Rows[i]));
                        }

                        if (tablaCaracteristicas.Rows.Count > 0)
                        {
                            for (var i = 0; i < tablaCaracteristicas.Rows.Count; i++)
                            {
                                var idEquipo = this.ObtenerValorCampo<int>(tablaCaracteristicas.Rows[i], ProcedimientosAlmacenados.Campos.IdEquipo);
                                var equipo = valorRetorno.FirstOrDefault(t => t.IdEquipo == idEquipo);
                                if (equipo != null)
                                {
                                    if (equipo.Caracteristicas == null)
                                    {
                                        equipo.Caracteristicas = new List<ModeloCaracteristica>();
                                    }
                                    equipo.Caracteristicas.Add(this.RegistroAModeloCaracteristica(tablaCaracteristicas.Rows[i]));
                                }
                            }
                        }
                    }
                }
            }
            return valorRetorno;
        }

        public void EquipoGuardar(ModeloEquipo modelo, int idUsuarioCaptura)
        {
            using (var caracteristicas = new TEquipoCaracteristica())
            {
                caracteristicas.Add(modelo.IdTipo, modelo.Caracteristicas);
                var parametros = this.ModeloCatalogoADiccionario(modelo);
                parametros.Add(ProcedimientosAlmacenados.Campos.IdEquipo, modelo.IdEquipo);
                parametros.Add(ProcedimientosAlmacenados.Campos.IdTipo, modelo.IdTipo);
                parametros.Add(ProcedimientosAlmacenados.Campos.IdEstado, modelo.IdEstado);
                parametros.Add(ProcedimientosAlmacenados.Campos.IdSucursal, modelo.IdSucursal);
                parametros.Add(ProcedimientosAlmacenados.Campos.IdUsuario, modelo.IdUsuario);
                parametros.Add(ProcedimientosAlmacenados.Campos.ActivoFijo, modelo.ActivoFijo);
                parametros.Add(ProcedimientosAlmacenados.Campos.Marca, modelo.Marca);
                parametros.Add(ProcedimientosAlmacenados.Campos.Modelo, modelo.Modelo);
                parametros.Add(ProcedimientosAlmacenados.Campos.Serie, modelo.Serie);
                parametros.Add(ProcedimientosAlmacenados.Campos.Numero, modelo.Numero);
                parametros.Add(ProcedimientosAlmacenados.Campos.FechaCompra, modelo.FechaCompra);
                parametros.Add(ProcedimientosAlmacenados.Campos.Caracteristicas, caracteristicas);
                parametros.Add(ProcedimientosAlmacenados.Campos.IdUsuarioCaptura, idUsuarioCaptura);

                using (var comando = base.ObtenerComando(ProcedimientosAlmacenados.EquipoGuardar))
                {
                    comando.EjecutarSinConsultar(parametros.ADiccionarioSoloLectura());
                }
            }            
        }

        public IEnumerable<ModeloEquipo> EquipoRegistroObtenerTodos(int idEquipo)
        {
            List<ModeloEquipo> valorRetorno = null;
            var parametros = new Dictionary<string, object>
            {
                { ProcedimientosAlmacenados.Campos.IdEquipo, idEquipo }
            };

            using (var comando = base.ObtenerComando(ProcedimientosAlmacenados.EquipoRegistroObtenerTodos))
            {
                var tabla = comando.ObtenerTabla(parametros.ADiccionarioSoloLectura());
                if (tabla.Rows.Count > 0)
                {
                    valorRetorno = new List<ModeloEquipo>();
                    for (var i = 0; i < tabla.Rows.Count; i++)
                    {
                        valorRetorno.Add(this.RegistroAModeloEquipo(tabla.Rows[i]));
                    }
                }
            }

            return valorRetorno;
        }

        private ModeloEquipo RegistroAModeloEquipo(DataRow registro)
        {
            var modeloCatalogo = this.RegistroAModeloCatalogo(registro);
            var valorRetorno = new ModeloEquipo
            {
                Nombre = modeloCatalogo.Nombre,
                Activo = modeloCatalogo.Activo,
                FechaInsercion = modeloCatalogo.FechaInsercion,
                FechaActualizacion = modeloCatalogo.FechaActualizacion
            };

            valorRetorno.IdEquipo = this.ObtenerValorCampo<int>(registro, ProcedimientosAlmacenados.Campos.IdEquipo);
            valorRetorno.IdTipo = this.ObtenerValorCampo<int>(registro, ProcedimientosAlmacenados.Campos.IdTipo);
            valorRetorno.IdEstado = this.ObtenerValorCampo<int>(registro, ProcedimientosAlmacenados.Campos.IdEstado);
            valorRetorno.IdSucursal = this.ObtenerValorCampo<int>(registro, ProcedimientosAlmacenados.Campos.IdSucursal);
            valorRetorno.IdUsuario = registro[ProcedimientosAlmacenados.Campos.IdUsuario] != DBNull.Value ?
                                this.ObtenerValorCampo<int>(registro, ProcedimientosAlmacenados.Campos.IdUsuario) : null;
            valorRetorno.ActivoFijo = this.ObtenerValorCampo<string>(registro, ProcedimientosAlmacenados.Campos.ActivoFijo);
            valorRetorno.Marca = this.ObtenerValorCampo<string>(registro, ProcedimientosAlmacenados.Campos.Marca);
            valorRetorno.Modelo = this.ObtenerValorCampo<string>(registro, ProcedimientosAlmacenados.Campos.Modelo);
            valorRetorno.Serie = this.ObtenerValorCampo<string>(registro, ProcedimientosAlmacenados.Campos.Serie);
            valorRetorno.Numero = registro[ProcedimientosAlmacenados.Campos.Numero] != DBNull.Value ?
                                    this.ObtenerValorCampo<short?>(registro, ProcedimientosAlmacenados.Campos.Numero) : null;
            valorRetorno.FechaCompra = this.ObtenerValorCampo<DateTime>(registro, ProcedimientosAlmacenados.Campos.FechaCompra);
            valorRetorno.IdUsuarioCaptura = this.ObtenerValorCampo<int>(registro, ProcedimientosAlmacenados.Campos.IdUsuarioCaptura);

            return valorRetorno;
        }
    }
}
