﻿using inventario.Administradores;
using inventario.Servicios;
using inventario.Servicios.Seguridad;
using Inventario.Comun.Seguridad;
using Inventario.Comun.Ubicacion;
using Xamarin.Forms;

namespace inventario
{
    public partial class App : Application
    {    
        public App()
        {
            InitializeComponent();

            AdministradorInventario = new AdministradorInventario(new ServicioInventario());
            AdministradorSeguridad = new AdministradorSeguridad(new ServicioSeguridad());
            AdministradorUbicacion = new AdministradorUbicacion(new ServicioUbicacion());

            MainPage = new NavigationPage(new Vista.VistaEntrar());
        }

        public static AdministradorInventario AdministradorInventario
        {
            get;
            private set;
        }

        public static AdministradorSeguridad AdministradorSeguridad
        {
            get;
            private set;
        }

        public static AdministradorUbicacion AdministradorUbicacion
        {
            get;
            private set;
        }

        public static string Llave
        {
            get;
            set;
        }

        public static ModeloUsuario DatosUsuario
        {
            get;
            set;
        }

        public static ModeloSucursal DatosSucursal
        {
            get;
            set;
        }
    }
}
