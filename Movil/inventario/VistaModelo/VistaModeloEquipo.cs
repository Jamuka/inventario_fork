﻿using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using Inventario.Comun.Inventario;
using Inventario.Comun.Seguridad;
using Inventario.Comun.Ubicacion;
using Newtonsoft.Json;
using Xamarin.Forms;

namespace inventario.VistaModelo
{
    public class VistaModeloEquipo : INotifyPropertyChanged
    {
        private ModeloEquipo modeloEquipo;
        private ModeloTipo tipoSeleccionado;
        private ModeloEstado estadoSeleccionado;
        private ModeloUsuario usuarioSeleccionado;
        private ModeloSucursal sucursalSeleccionada;
        private bool estaGuardando;
        private ObservableCollection<ModeloCaracteristica> caracteristicas;
        private BackgroundWorker tareaGuardar;

        public VistaModeloEquipo(ModeloEquipo modeloEquipo, int? idUsuario = null)
        {
            System.Diagnostics.Trace.WriteLine("VistaModeloEquipo " + System.DateTime.Now.ToLongTimeString());
            this.Caracteristicas = new ObservableCollection<ModeloCaracteristica>();

            this.modeloEquipo = modeloEquipo;
            this.ComandoGuardar = new Command(() => this.Guardar(), () => this.TipoSeleccionado != null && this.EstadoSeleccionado != null && this.UsuarioSeleccionado != null);

            this.TipoSeleccionado = App.AdministradorInventario.Tipos.FirstOrDefault(t=>t.IdTipo == this.modeloEquipo.IdTipo);
            this.EstadoSeleccionado = App.AdministradorInventario.Estados.FirstOrDefault(e=> e.IdEstado == this.modeloEquipo.IdEstado);
            if(!idUsuario.HasValue)
                this.UsuarioSeleccionado = App.AdministradorSeguridad.Usuarios.FirstOrDefault(u=>u.IdUsuario == this.modeloEquipo.IdUsuario);
            else
                this.UsuarioSeleccionado = App.AdministradorSeguridad.Usuarios.FirstOrDefault(u => u.IdUsuario == idUsuario);

            this.SucursalSeleccionada = App.AdministradorUbicacion.Sucursales.FirstOrDefault(s=>s.IdSucursal == this.modeloEquipo.IdSucursal);

            this.Tipos = App.AdministradorInventario.Tipos;
            this.Estados = App.AdministradorInventario.Estados;

            if (!idUsuario.HasValue)
                this.Usuarios = App.AdministradorSeguridad.Usuarios;
            else
                this.Usuarios = App.AdministradorSeguridad.Usuarios.Where(u => u.IdUsuario == idUsuario).ToList();
            this.Sucursales = App.AdministradorUbicacion.Sucursales;

            this.tareaGuardar = new BackgroundWorker();
            this.tareaGuardar.DoWork += this.TareaGuardarEjecucion;
            this.tareaGuardar.RunWorkerCompleted += this.TareaGuardarTerminado;
        }

        public event PropertyChangedEventHandler PropertyChanged;

        public ModeloEquipo ModeloEquipo
        {
            get
            {
                return this.modeloEquipo;
            }
            set
            {
                this.modeloEquipo = value;
                this.PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(nameof(ModeloEquipo)));

                this.Nombre = this.modeloEquipo.Nombre;
            }
        }

        public ModeloTipo TipoSeleccionado
        {
            get
            {
                return this.tipoSeleccionado;
            }
            set
            {
                this.tipoSeleccionado = value;
                this.PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(nameof(TipoSeleccionado)));

                this.ComandoGuardar.ChangeCanExecute();
                this.Caracteristicas.Clear();
                if (this.TipoSeleccionado != null && this.tipoSeleccionado.Caracteristicas != null )
                {
                    foreach (var caracteristica in this.TipoSeleccionado.Caracteristicas)
                    {
                        var caracteristicaNueva = JsonConvert.DeserializeObject<ModeloCaracteristica>(JsonConvert.SerializeObject(caracteristica));
                        this.Caracteristicas.Add(caracteristicaNueva);

                        if (this.modeloEquipo.Caracteristicas != null)
                        {
                            var caracteristicaEquipo = this.modeloEquipo.Caracteristicas.FirstOrDefault(c => c.IdCaracteristica == caracteristicaNueva.IdCaracteristica);
                            if (caracteristicaEquipo != null)
                            {
                                caracteristicaNueva.Valor = caracteristicaEquipo.Valor;
                            }
                        }
                    }
                }
            }
        }

        public ModeloEstado EstadoSeleccionado
        {
            get
            {
                return this.estadoSeleccionado;
            }
            set
            {
                this.estadoSeleccionado = value;
                this.PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(nameof(EstadoSeleccionado)));
                this.ComandoGuardar.ChangeCanExecute();
            }
        }

        public ModeloUsuario UsuarioSeleccionado
        {
            get
            {
                return this.usuarioSeleccionado;
            }
            set
            {
                this.usuarioSeleccionado = value;
                this.PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(nameof(UsuarioSeleccionado)));
                this.ComandoGuardar.ChangeCanExecute();
            }
        }

        public ModeloSucursal SucursalSeleccionada
        {
            get
            {
                return this.sucursalSeleccionada;
            }
            set
            {
                this.sucursalSeleccionada = value;
                this.PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(nameof(SucursalSeleccionada)));
                this.ComandoGuardar.ChangeCanExecute();
            }
        }

        public string Nombre
        {
            get
            {
                return this.modeloEquipo.Nombre;
            }
            set
            {
                this.modeloEquipo.Nombre = value;
                this.PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(nameof(Nombre)));
                this.ComandoGuardar.ChangeCanExecute();
            }
        }

        public List<ModeloTipo> Tipos
        {
            get;
            set;
        }

        public List<ModeloEstado> Estados
        {
            get;
            set;
        }

        public List<ModeloUsuario> Usuarios
        {
            get;
            set;
        }

        public List<ModeloSucursal> Sucursales
        {
            get;
            set;
        }

        public bool EstaGuardando
        {
            get
            {
                return this.estaGuardando;
            }
            set
            {
                this.estaGuardando = value;
                this.PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(nameof(EstaGuardando)));
            }
        }

        public ObservableCollection<ModeloCaracteristica> Caracteristicas
        {
            get
            {
                return this.caracteristicas;
            }
            set
            {
                this.caracteristicas = value;
                this.PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(nameof(Caracteristicas)));
            }
        }

        public Command ComandoGuardar
        {
            get;
            private set;
        }

        private void Guardar()
        {
            this.tareaGuardar.RunWorkerAsync();
        }

        private void TareaGuardarEjecucion(object sender, DoWorkEventArgs e)
        {
            this.EstaGuardando = true;
            this.ModeloEquipo.IdEstado = this.EstadoSeleccionado.IdEstado;
            this.ModeloEquipo.IdSucursal = this.SucursalSeleccionada.IdSucursal;
            this.ModeloEquipo.IdTipo = this.TipoSeleccionado.IdTipo;
            this.ModeloEquipo.IdUsuario = this.UsuarioSeleccionado.IdUsuario;
            this.ModeloEquipo.Caracteristicas = this.Caracteristicas.ToList();
            App.AdministradorInventario.EquipoGuardar(this.ModeloEquipo).GetAwaiter();
        }

        private void TareaGuardarTerminado(object sender, RunWorkerCompletedEventArgs e)
        {
            Application.Current.MainPage.Navigation.PopAsync(true);
            this.EstaGuardando = false;
        }
    }
}
