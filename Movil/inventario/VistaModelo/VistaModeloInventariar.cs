﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using Inventario.Comun.Inventario;
using Inventario.Comun.Seguridad;
using Inventario.Comun.Ubicacion;
using Xamarin.Forms;

namespace inventario.VistaModelo
{
    public class VistaModeloInventariar : INotifyPropertyChanged
    {
        private ModeloSucursal sucursalSeleccionada;
        private ModeloUsuario usuarioSeleccionado;
        private ModeloEquipo equipoSeleccionado;
        private string comentarios;
        private bool estaGuardando;
        private bool seleccionarSucursal;
        private BackgroundWorker tareaGuardar;

        public VistaModeloInventariar(ModeloEquipo modeloEquipo)
        {
            this.ComandoGuardar = new Command(() => this.Guardar(), () => this.UsuarioSeleccionado != null && this.EquipoSeleccionado != null && this.SucursalSeleccionada != null);

            this.EquipoSeleccionado = modeloEquipo;

            this.Usuarios = new List<ModeloUsuario>(){ App.DatosUsuario };
            this.UsuarioSeleccionado = App.DatosUsuario;

            this.Equipos = new List<ModeloEquipo>() { this.EquipoSeleccionado };

            this.Sucursales = App.AdministradorUbicacion.Sucursales;
            if(App.DatosSucursal != null)
                this.SucursalSeleccionada = this.Sucursales.FirstOrDefault(s=> s.IdSucursal == App.DatosSucursal.IdSucursal);

            this.SeleccionarSucursal = this.SucursalSeleccionada == null;

            this.tareaGuardar = new BackgroundWorker();
            this.tareaGuardar.DoWork += this.TareaGuardarEjecucion;
            this.tareaGuardar.RunWorkerCompleted += this.TareaGuardarTerminado;
        }

        public event PropertyChangedEventHandler PropertyChanged;

        public ModeloUsuario UsuarioSeleccionado
        {
            get
            {
                return this.usuarioSeleccionado;
            }
            set
            {
                this.usuarioSeleccionado = value;
                this.PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(nameof(UsuarioSeleccionado)));
                this.ComandoGuardar.ChangeCanExecute();
            }
        }

        public ModeloEquipo EquipoSeleccionado
        {
            get
            {
                return this.equipoSeleccionado;
            }
            set
            {
                this.equipoSeleccionado = value;
                this.PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(nameof(EquipoSeleccionado)));
                this.ComandoGuardar.ChangeCanExecute();
            }
        }

        public ModeloSucursal SucursalSeleccionada
        {
            get
            {
                return this.sucursalSeleccionada;
            }
            set
            {
                this.sucursalSeleccionada = value;
                this.PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(nameof(SucursalSeleccionada)));
                this.ComandoGuardar.ChangeCanExecute();
            }
        }

        public string Comentarios
        {
            get
            {
                return this.comentarios;
            }
            set
            {
                this.comentarios = value;
                this.PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(nameof(Comentarios)));
            }
        }

        public List<ModeloUsuario> Usuarios
        {
            get;
            set;
        }

        public List<ModeloEquipo> Equipos
        {
            get;
            set;
        }

        public List<ModeloSucursal> Sucursales
        {
            get;
            set;
        }

        public bool EstaGuardando
        {
            get
            {
                return this.estaGuardando;
            }
            set
            {
                this.estaGuardando = value;
                this.PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(nameof(EstaGuardando)));
            }
        }

        public bool SeleccionarSucursal
        {
            get
            {
                return this.seleccionarSucursal;
            }
            set
            {
                this.seleccionarSucursal = value;
                this.PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(nameof(SeleccionarSucursal)));
            }
        }

        public Command ComandoGuardar
        {
            get;
            private set;
        }

        private void Guardar()
        {
            this.tareaGuardar.RunWorkerAsync();
        }

        private void TareaGuardarEjecucion(object sender, DoWorkEventArgs e)
        {
            this.EstaGuardando = true;
            var historia = new ModeloHistoria
            {
                IdEquipo = this.EquipoSeleccionado.IdEquipo,
                IdSucursal = this.SucursalSeleccionada.IdSucursal,
                IdUsuario = this.UsuarioSeleccionado.IdUsuario,
                Comentarios = this.Comentarios
            };

            App.AdministradorInventario.HistoriaGuardar(historia).GetAwaiter();
        }

        private void TareaGuardarTerminado(object sender, RunWorkerCompletedEventArgs e)
        {
            Application.Current.MainPage.Navigation.PopAsync(true);
            this.EstaGuardando = false;
        }
    }
}
