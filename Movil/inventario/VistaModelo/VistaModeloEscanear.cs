﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Threading;
using inventario.Vista;
using Inventario.Comun.Inventario;
using Inventario.Comun.Seguridad;
using Inventario.Comun.Ubicacion;
using Xamarin.Forms;

namespace inventario.VistaModelo
{
    public class VistaModeloEscanear : INotifyPropertyChanged
    {
        private ModeloEquipo modeloEquipo;
        private bool estaConsultando;
        private ZXing.Result resultado;
        private readonly Queue cola = new Queue();
        private readonly Thread hiloDesencolar;
        private readonly EventWaitHandle disparador = new AutoResetEvent(false);
        private static readonly object Sincronizador = new object();
        private string error;
        private DateTime ultimaLecturaValida = DateTime.Now;
        private string equipoLeido;
        private bool mostrarError;
        private bool mostrarEquipo;
        private ModeloSucursal sucursalSeleccionada;
        private ModeloUsuario usuarioSeleccionado;

        public VistaModeloEscanear()
        {
            this.ComandoEdicion = new Command(this.Edicion, () => this.ModeloEquipo != null);
            this.ComandoInventariar = new Command(this.Inventariar, () => this.ModeloEquipo != null);

            this.hiloDesencolar = new Thread(this.ProcesoDesencolar) { IsBackground = true };
            this.hiloDesencolar.Start();
        }

        public event PropertyChangedEventHandler PropertyChanged;

        public ModeloEquipo ModeloEquipo
        {
            get
            {
                return this.modeloEquipo;
            }
            set
            {
                this.modeloEquipo = value;
                this.PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(nameof(ModeloEquipo)));

                if(this.modeloEquipo != null)
                {
                    this.UsuarioSeleccionado = App.AdministradorSeguridad.Usuarios.FirstOrDefault(u=> u.IdUsuario == this.modeloEquipo.IdUsuario);
                    this.SucursalSeleccionada = App.AdministradorUbicacion.Sucursales.FirstOrDefault(s => s.IdSucursal == this.modeloEquipo.IdSucursal);
                }

                this.ComandoEdicion.ChangeCanExecute();
                this.ComandoInventariar.ChangeCanExecute();
            }
        }

        public bool EstaConsultando
        {
            get
            {
                return this.estaConsultando;
            }
            set
            {
                this.estaConsultando = value;
                this.PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(nameof(EstaConsultando)));
            }
        }

        public ZXing.Result Resultado
        {
            get
            {
                return this.resultado;
            }
            set
            {
                this.resultado = value;
                this.PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(nameof(Resultado)));
                this.Encolar(this.resultado);
            }
        }

        public string Error
        {
            get
            {
                return this.error;
            }
            set
            {
                this.error = value;
                this.PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(nameof(Error)));

                if(string.IsNullOrEmpty(this.error))
                {
                    this.MostrarError = false;
                }
                else
                {
                    this.MostrarError = true;
                }

                this.MostrarEquipo = !this.MostrarError;
                
            }
        }

        public bool MostrarError
        {
            get
            {
                return this.mostrarError;
            }
            set
            {
                this.mostrarError = value;
                this.PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(nameof(MostrarError)));
            }
        }

        public bool MostrarEquipo
        {
            get
            {
                return this.mostrarEquipo;
            }
            set
            {
                this.mostrarEquipo = value;
                this.PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(nameof(MostrarEquipo)));
            }
        }

        public ModeloSucursal SucursalSeleccionada
        {
            get
            {
                return this.sucursalSeleccionada;
            }
            set
            {
                this.sucursalSeleccionada = value;
                this.PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(nameof(SucursalSeleccionada)));
            }
        }

        public ModeloUsuario UsuarioSeleccionado
        {
            get
            {
                return this.usuarioSeleccionado;
            }
            set
            {
                this.usuarioSeleccionado = value;
                this.PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(nameof(UsuarioSeleccionado)));
            }
        }

        public Command ComandoEdicion
        {
            get;
            private set;
        }

        public Command ComandoInventariar
        {
            get;
            private set;
        }

        private async void Edicion()
        {
            var vistaModeloEquipo = new VistaModeloEquipo(this.ModeloEquipo);
            var vistaEquipo = new VistaEquipo
            {
                BindingContext = vistaModeloEquipo
            };

            await Application.Current.MainPage.Navigation.PushAsync(vistaEquipo);
        }

        private async void Inventariar()
        {
            var vistaModeloInventariar = new VistaModeloInventariar(this.ModeloEquipo);
            var vistaInventariar = new VistaInventariar
            {
                BindingContext = vistaModeloInventariar
            };

            await Application.Current.MainPage.Navigation.PushAsync(vistaInventariar);
        }

        private void Encolar(ZXing.Result codigoLeido)
        {
            lock(Sincronizador)
            {
                var lectura = new DateTime(codigoLeido.Timestamp);
                //System.Diagnostics.Trace.WriteLine(string.Format("Encolar(1) {0} - {1}", lectura.ToLongTimeString(), codigoLeido.Text));
                if (this.ultimaLecturaValida.AddSeconds(10) < lectura)
                {
                    if (equipoLeido != codigoLeido.Text)
                    {
                        //System.Diagnostics.Trace.WriteLine(string.Format("Encolar(2) -  {0} - {1}", lectura.ToLongTimeString(), codigoLeido.Text));
                        this.ultimaLecturaValida = lectura;
                        this.cola.Enqueue(codigoLeido.Text);
                        this.disparador.Set();
                    }
                }
            }
        }

        private void ProcesoDesencolar()
        {
            while(true)
            {
                string equipoLeido = null;
                lock(Sincronizador)
                {
                    if(this.cola.Count > 0)
                    {
                        equipoLeido = (string)this.cola.Dequeue();
                    }
                }

                if(equipoLeido == null)
                {
                    this.disparador.WaitOne();
                    continue;
                }

                try
                {
                    //System.Diagnostics.Trace.WriteLine(string.Format("ProcesoDesencolar(1) -  {0}", equipoLeido));
                    var idEquipoCadena = equipoLeido.Split(new char[] { ',' })[0];
                    var idEquipo = System.Convert.ToInt32(idEquipoCadena);
                    var equipo = App.AdministradorInventario.EquipoObtener(idEquipo).GetAwaiter().GetResult();
                    App.Current.Dispatcher.BeginInvokeOnMainThread(()=>
                    {
                        this.ModeloEquipo = equipo;
                        this.Error = string.Empty;
                    });
                    
                    lock (Sincronizador)
                    {
                        this.equipoLeido = equipoLeido;
                    }
                }
                catch(Exception ex)
                {
                    System.Diagnostics.Trace.WriteLine(string.Format("ProcesoDesencolar(2) -  {0} {1}", equipoLeido, ex.Message));
                    this.Error = "No fue posible recuperar la informacion del equipo";
                }
            }
        }
    }
}
