﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Windows.Input;
using inventario.Vista;
using Inventario.Comun.Seguridad;
using Inventario.Comun.Ubicacion;
using Xamarin.Essentials;
using Xamarin.Forms;

namespace inventario.VistaModelo
{
    public class VistaModeloEntrar : INotifyPropertyChanged
    {
        private bool mostrarSucursales;
        private string identificacion;
        private string contrasena;
        private ModeloSucursal sucursalSeleccionada;
        private BackgroundWorker tareaCargarUsuariosYSucursales;
        private BackgroundWorker tareaConsultar;
        private ModeloUsuario inventariador;
        private bool estaConsultando;

        public VistaModeloEntrar()
        {
            this.Sucursales = new ObservableCollection<ModeloSucursal>();
            this.ComandoEntrar = new Command(this.OnEntrar);

            this.tareaCargarUsuariosYSucursales = new BackgroundWorker();
            this.tareaCargarUsuariosYSucursales.DoWork += this.TareaCargarUsuariosYSucursalesEjecucion;
            this.tareaCargarUsuariosYSucursales.RunWorkerAsync();

            this.tareaConsultar = new BackgroundWorker();
            this.tareaConsultar.DoWork += this.TareaConsultarEjecucion;
            this.tareaConsultar.RunWorkerCompleted += this.TareaConsultarTerminado;
        }

        public event PropertyChangedEventHandler PropertyChanged;

        public ObservableCollection<ModeloSucursal> Sucursales
        {
            get;
            set;
        }

        public List<ModeloUsuario> Inventariadores
        {
            get;
            set;
        }

        public bool MostrarSucursales
        {
            get
            {
                return this.mostrarSucursales;
            }
            set
            {
                this.mostrarSucursales = value;
                this.PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(nameof(MostrarSucursales)));
            }
        }

        public string Identificacion
        {
            get
            {
                return this.identificacion;
            }
            set
            {
                this.identificacion = value;
                this.PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(nameof(Identificacion)));
                if (this.Inventariadores != null)
                {
                    this.inventariador = this.Inventariadores.FirstOrDefault(i => i.Identificacion == this.identificacion);
                    if (this.inventariador != null)
                    {
                        this.MostrarSucursales = true;
                    }
                    else
                    {
                        this.MostrarSucursales = false;
                    }
                }
            }
        }

        public string Contrasena
        {
            get
            {
                return this.contrasena;
            }
            set
            {
                this.contrasena = value;
                this.PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(nameof(Contrasena)));
            }
        }

        public ModeloSucursal SucursalSeleccionada
        {
            get
            {
                return this.sucursalSeleccionada;
            }
            set
            {
                this.sucursalSeleccionada = value;
                this.PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(nameof(SucursalSeleccionada)));
            }
        }

        public bool EstaConsultando
        {
            get
            {
                return this.estaConsultando;
            }
            set
            {
                this.estaConsultando = value;
                this.PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(nameof(EstaConsultando)));
            }
        }

        public ICommand ComandoEntrar
        {
            get;
            private set;
        }

        private void OnEntrar()
        {
            this.tareaConsultar.RunWorkerAsync();
        }

        private void TareaConsultarEjecucion(object sender, DoWorkEventArgs e)
        {
            try
            {
                this.EstaConsultando = true;
                var solicitud = new ModeloSolicitudEntrar
                {
                    Identificacion = this.identificacion,
                    Contrasena = this.contrasena
                };
                if (this.SucursalSeleccionada != null)
                {
                    solicitud.IdSucursal = this.SucursalSeleccionada.IdSucursal;
                }
                else
                {
                    solicitud.IdSucursal = null;
                }

                var respuesta = App.AdministradorSeguridad.UsuarioEntrar(solicitud).GetAwaiter().GetResult();
                App.Llave = respuesta.Llave;
                App.DatosUsuario = respuesta.Usuario;
                App.DatosSucursal = respuesta.Sucursal;

                var vistaModeloMenu = new VistaModeloMenu();
                var vistaMenu = new VistaMenu
                {
                    BindingContext = vistaModeloMenu
                };

                
                App.Current.Dispatcher.BeginInvokeOnMainThread(async () => await Application.Current.MainPage.Navigation.PushAsync(vistaMenu));
                
            }
            catch (Exception ex)
            {
                App.Current.Dispatcher.BeginInvokeOnMainThread(async () => await Application.Current.MainPage.DisplayAlert("Error", ex.Message, "OK"));
            }
        }

        private void TareaConsultarTerminado(object sender, RunWorkerCompletedEventArgs e)
        {
            this.EstaConsultando = false;
        }

        private void TareaCargarUsuariosYSucursalesEjecucion(object sender, DoWorkEventArgs e)
        {
            this.Inventariadores = App.AdministradorSeguridad.UsuarioObtenerTodosPorRol("Inventariador").Result;
            var sucursales = App.AdministradorUbicacion.SucursalObtenerTodos().Result;
            Application.Current.Dispatcher.BeginInvokeOnMainThread(() =>
            {
                foreach (var sucursal in sucursales)
                {
                    this.Sucursales.Add(sucursal);
                }
            });
        }
    }
}

