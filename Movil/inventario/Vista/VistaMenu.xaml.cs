﻿using Xamarin.Forms.PlatformConfiguration.AndroidSpecific;
using Xamarin.Forms.Xaml;

namespace inventario.Vista
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class VistaMenu : Xamarin.Forms.TabbedPage
    {
        public VistaMenu()
        {
            InitializeComponent();
            On<Xamarin.Forms.PlatformConfiguration.Android>().SetToolbarPlacement(ToolbarPlacement.Bottom);
        }
    }
}