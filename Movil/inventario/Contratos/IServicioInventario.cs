﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Inventario.Comun.Inventario;

namespace inventario.Contratos
{
    public interface IServicioInventario
    {
        Task<ModeloEquipo> EquipoObtener(int idEquipo);

        Task<List<ModeloEquipo>> EquipoObtenerTodos(int? idUsuario);

        Task EquipoGuardar(ModeloEquipo modelo);

        Task<List<ModeloTipo>> TipoObtenerTodos();

        Task<List<ModeloEstado>> EstadoObtenerTodos();

        Task HistoriaGuardar(ModeloHistoria modelo);
    }
}
