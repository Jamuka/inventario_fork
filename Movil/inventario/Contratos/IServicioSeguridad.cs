﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Inventario.Comun.Seguridad;

namespace inventario.Contratos
{
    public interface IServicioSeguridad
    {
        Task<List<ModeloUsuario>> UsuarioObtenerTodosPorRol(string rol);

        Task<List<ModeloUsuario>> UsuarioObtenerTodos();

        Task<ModeloRespuestaEntrar> UsuarioEntrar(ModeloSolicitudEntrar modelo);
    }
}
