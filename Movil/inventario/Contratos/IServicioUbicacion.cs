﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Inventario.Comun.Ubicacion;

namespace inventario.Contratos
{
    public interface IServicioUbicacion
    {
        Task<List<ModeloSucursal>> SucursalObtenerTodos();
    }
}
