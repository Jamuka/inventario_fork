﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using inventario.Contratos;
using inventario.Modelo;
using Inventario.Comun.Seguridad;
using Newtonsoft.Json;

namespace inventario.Servicios.Seguridad
{
    public class ServicioSeguridad : IServicioSeguridad
    {
        public async Task<List<ModeloUsuario>> UsuarioObtenerTodosPorRol(string rol)
        {
            var uri = new Uri(Constantes.ApiInventarioUsuario + "obtenerTodosPorRol/?rol=" + rol);
            var elementos = new List<ModeloUsuario>();

            using (var client = new CustomHttpClient())
            {
                var response = await client.GetAsync(uri);
                if (response.IsSuccessStatusCode)
                {
                    string content = await response.Content.ReadAsStringAsync();
                    elementos = JsonConvert.DeserializeObject<List<ModeloUsuario>>(content);
                }
            }

            return elementos;
        }

        public async Task<List<ModeloUsuario>> UsuarioObtenerTodos()
        {
            var uri = new Uri(Constantes.ApiInventarioUsuario + "obtenerTodos");
            var elementos = new List<ModeloUsuario>();

            using (var client = new CustomHttpClient())
            {
                var response = await client.GetAsync(uri);
                if (response.IsSuccessStatusCode)
                {
                    string content = await response.Content.ReadAsStringAsync();
                    elementos = JsonConvert.DeserializeObject<List<ModeloUsuario>>(content);
                }
            }

            return elementos;
        }

        public async Task<ModeloRespuestaEntrar> UsuarioEntrar(ModeloSolicitudEntrar modelo)
        {
            var uri = new Uri(Constantes.ApiInventarioUsuario + "entrar");
            ModeloRespuestaEntrar valorRetorno = null;

            using (var client = new CustomHttpClient())
            {
                var response = await client.PostAsync(uri, JsonConvert.SerializeObject(modelo));
                if (response.IsSuccessStatusCode)
                {
                    string content = await response.Content.ReadAsStringAsync();
                    valorRetorno = JsonConvert.DeserializeObject<ModeloRespuestaEntrar>(content);
                }
                else
                {
                    throw new Exception("Credenciales invalidas, vuelva a intentar");
                }
            }

            return valorRetorno;
        }
    }
}