﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using inventario.Contratos;
using inventario.Modelo;
using Inventario.Comun.Inventario;
using Newtonsoft.Json;

namespace inventario.Servicios
{
    public class ServicioInventario : IServicioInventario
    {
        public async Task<ModeloEquipo> EquipoObtener(int idEquipo)
        {
            var uri = new Uri(Constantes.ApiInventarioEquipo + "obtener/?idEquipo=" + idEquipo);
            ModeloEquipo valorRetorno = null;

            using (var client = new CustomHttpClient())
            {
                var response = await client.GetAsync(uri);
                if (response.IsSuccessStatusCode)
                {
                    string content = await response.Content.ReadAsStringAsync();
                    valorRetorno = JsonConvert.DeserializeObject<ModeloEquipo>(content);
                }
            }

            return valorRetorno;
        }

        public async Task<List<ModeloEquipo>> EquipoObtenerTodos(int? idUsuario)
        {
            var uri = new Uri(Constantes.ApiInventarioEquipo + "obtenerTodos" + (idUsuario.HasValue ? "/?idUsuario=" +idUsuario.Value : "") );
            var elementos = new List<ModeloEquipo>();
            System.Diagnostics.Trace.WriteLine("Url : " + uri.ToString());
            using (var client = new CustomHttpClient())
            {
                var response = await client.GetAsync(uri);
                if (response.IsSuccessStatusCode)
                {
                    string content = await response.Content.ReadAsStringAsync();
                    elementos = JsonConvert.DeserializeObject<List<ModeloEquipo>>(content);
                }
            }

            return elementos;
        }

        public async Task EquipoGuardar(ModeloEquipo modelo)
        {
            var uri = new Uri(Constantes.ApiInventarioEquipo + "guardar");

            using (var client = new CustomHttpClient())
            {
                var response = await client.PostAsync(uri, JsonConvert.SerializeObject(modelo));
                if (!response.IsSuccessStatusCode)
                {
                    throw new Exception("Error al guardar el equipo");
                }
            }
        }

        public async Task<List<ModeloTipo>> TipoObtenerTodos()
        {
            var uri = new Uri(Constantes.ApiInventarioTipo + "obtenerTodos");
            var elementos = new List<ModeloTipo>();

            using (var client = new CustomHttpClient())
            {
                var response = await client.GetAsync(uri);
                if (response.IsSuccessStatusCode)
                {
                    string content = await response.Content.ReadAsStringAsync();
                    elementos = JsonConvert.DeserializeObject<List<ModeloTipo>>(content);
                }
            }

            return elementos;
        }

        public async Task<List<ModeloEstado>> EstadoObtenerTodos()
        {
            var uri = new Uri(Constantes.ApiInventarioEstado + "obtenerTodos");
            var elementos = new List<ModeloEstado>();

            using (var client = new CustomHttpClient())
            {
                var response = await client.GetAsync(uri);
                if (response.IsSuccessStatusCode)
                {
                    string content = await response.Content.ReadAsStringAsync();
                    elementos = JsonConvert.DeserializeObject<List<ModeloEstado>>(content);
                }
            }

            return elementos;
        }

        public async Task HistoriaGuardar(ModeloHistoria modelo)
        {
            var uri = new Uri(Constantes.ApiInventarioHistoria + "guardar");

            using (var client = new CustomHttpClient())
            {
                var response = await client.PostAsync(uri, JsonConvert.SerializeObject(modelo));
                if (!response.IsSuccessStatusCode)
                {
                    throw new Exception("Error al registrar el ivnentario");
                }
            }
        }

    }
}
