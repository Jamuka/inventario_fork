﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using inventario.Contratos;
using inventario.Modelo;
using Inventario.Comun.Ubicacion;
using Newtonsoft.Json;

namespace inventario.Servicios
{
    public class ServicioUbicacion : IServicioUbicacion
    {
        public async Task<List<ModeloSucursal>> SucursalObtenerTodos()
        {
            var uri = new Uri(Constantes.ApiInventarioSucursal + "obtenerTodos");
            var elementos = new List<ModeloSucursal>();

            using (var client = new CustomHttpClient())
            {
                var response = await client.GetAsync(uri);
                if (response.IsSuccessStatusCode)
                {
                    string content = await response.Content.ReadAsStringAsync();
                    elementos = JsonConvert.DeserializeObject<List<ModeloSucursal>>(content);
                }
            }

            return elementos;
        }
    }
}
