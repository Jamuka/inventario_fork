﻿using System;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace inventario.Servicios
{
    public sealed class CustomHttpClient : IDisposable
    {
        private HttpClient client;

        public CustomHttpClient()
        {
            this.client = new HttpClient(new HttpClientHandler());
            if (!string.IsNullOrEmpty(App.Llave))
            {
                var authHeader = new AuthenticationHeaderValue("bearer", App.Llave);
                this.client.DefaultRequestHeaders.Authorization = authHeader;
            }
        }

        public async Task<HttpResponseMessage> PostAsync(Uri uri, string parameters)
        {
            var valorRetorno = await this.client.PostAsync(uri, new StringContent(parameters, Encoding.UTF8, "application/json"));

            if (valorRetorno.StatusCode == System.Net.HttpStatusCode.Unauthorized)
            {
                this.CloseSession();
            }

            return valorRetorno;
        }

        public async Task<HttpResponseMessage> GetAsync(Uri uri)
        {
            var valorRetorno = await this.client.GetAsync(uri);

            if(valorRetorno.StatusCode == System.Net.HttpStatusCode.Unauthorized)
            {
                this.CloseSession();
            }

            return valorRetorno;
        }

        public void Dispose()
        {
            this.client.Dispose();
            this.client = null;
        }

        private void CloseSession()
        {
            App.Llave = string.Empty;
            App.DatosSucursal = null;
            App.DatosUsuario = null;
            Application.Current.MainPage = new NavigationPage(new Vista.VistaEntrar());
        }
    }
}
