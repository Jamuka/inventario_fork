﻿using System.Collections.Generic;
using System.Threading.Tasks;
using inventario.Contratos;
using Inventario.Comun.Inventario;

namespace inventario.Administradores
{
    public class AdministradorInventario
    {
        private IServicioInventario servicioInventario;

        public AdministradorInventario(IServicioInventario servicioInventario)
        {
            this.servicioInventario = servicioInventario;
        }

        public List<ModeloTipo> Tipos
        {
            get;
            set;
        }

        public List<ModeloEstado> Estados
        {
            get;
            set;
        }

        public Task<ModeloEquipo> EquipoObtener(int idEquipo)
        {
            return this.servicioInventario.EquipoObtener(idEquipo);
        }

        public Task<List<ModeloEquipo>> EquipoObtenerTodos(int? idUsuario=null)
        {
            return this.servicioInventario.EquipoObtenerTodos(idUsuario);
        }

        public Task EquipoGuardar(ModeloEquipo modelo)
        {
            return this.servicioInventario.EquipoGuardar(modelo);
        }

        public Task<List<ModeloTipo>> TipoObtenerTodos()
        {
            return this.servicioInventario.TipoObtenerTodos();
        }

        public Task<List<ModeloEstado>> EstadoObtenerTodos()
        {
            return this.servicioInventario.EstadoObtenerTodos();
        }

        public Task HistoriaGuardar(ModeloHistoria modelo)
        {
            return this.servicioInventario.HistoriaGuardar(modelo);
        }
    }
}
