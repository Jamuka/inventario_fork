﻿using System.Collections.Generic;
using System.Threading.Tasks;
using inventario.Contratos;
using Inventario.Comun.Seguridad;

namespace inventario.Administradores
{
    public class AdministradorSeguridad
    {
        private IServicioSeguridad servicioSeguridad;

        public AdministradorSeguridad(IServicioSeguridad servicioSeguridad)
        {
            this.servicioSeguridad = servicioSeguridad;
        }

        public List<ModeloUsuario> Usuarios
        {
            get;
            set;
        }

        public Task<List<ModeloUsuario>> UsuarioObtenerTodosPorRol(string rol)
        {
            return this.servicioSeguridad.UsuarioObtenerTodosPorRol(rol);
        }

        public Task<List<ModeloUsuario>> UsuarioObtenerTodos()
        {
            return this.servicioSeguridad.UsuarioObtenerTodos();
        }

        public Task<ModeloRespuestaEntrar> UsuarioEntrar(ModeloSolicitudEntrar modelo)
        {
            return this.servicioSeguridad.UsuarioEntrar(modelo);
        }
    }
}
