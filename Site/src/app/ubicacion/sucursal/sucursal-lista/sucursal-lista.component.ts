import { HttpErrorResponse } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { IModeloEmpresa } from 'src/app/modelos/ubicacion/empresa.model';
import { IModeloSucursal, ModeloSucursal } from 'src/app/modelos/ubicacion/sucursal.model';
import { Notification } from 'src/app/shared/Notification';
import { ServicioEmpresa } from '../../empresa/empresa.service';
import { SucursalFormularioComponent } from '../sucursal-formulario/sucursal-formulario.component';
import { ServicioSucursal } from '../sucursal.service';

@Component({
  selector: 'app-sucursal-lista',
  templateUrl: './sucursal-lista.component.html',
  styleUrls: ['./sucursal-lista.component.css']
})

export class SucursalListaComponent implements OnInit {
  // -- Variables
  private cargando : boolean;
  empresas: IModeloEmpresa[];
  sucursales: IModeloSucursal[];
  pagina : number;

  // -- Constructor
  constructor(private servicioModal : NgbModal, 
    private servicioEmpresa: ServicioEmpresa, 
    private servicioSucursal: ServicioSucursal) {     
  }

  // -- Propiedades
  get Cargando() : boolean{
    return this.cargando;
  }

  // -- Funciones
  ngOnInit(): void {
    this.obtenerTodos();
  }

  obtenerTodos(){
    this.cargando = true;
    this.servicioSucursal.obtenerTodos()
      .then(registros =>
        {
          this.sucursales = registros;
          this.pagina = 1;
          this.cargando = false;
          this.obtenerTodosEmpresa();
        })
      .catch((error : HttpErrorResponse) =>
        {
          Notification.Show('bottom', 'right', 'danger', error.message);
        })
  }

  obtenerTodosEmpresa(){
    this.servicioEmpresa.obtenerTodos()
      .then(registros =>
        {
          this.empresas = registros;
        })
      .catch((error : HttpErrorResponse) =>
        {
          Notification.Show('bottom', 'right', 'danger', error.message);
        })
  }

  public clickAgregar(){
    const modal = this.servicioModal.open(SucursalFormularioComponent);
    modal.result.then(this.cerrandoFormulario.bind(this), this.cerrandoFormulario.bind(this));
    modal.componentInstance.empresas = this.empresas;
  }

  public clickEditar(modeloSucursal :ModeloSucursal){
    const modal = this.servicioModal.open(SucursalFormularioComponent);
    modal.result.then(this.cerrandoFormulario.bind(this), this.cerrandoFormulario.bind(this));
    modal.componentInstance.modoAgregar = false;
    modal.componentInstance.modeloSucursal = modeloSucursal;
    modal.componentInstance.empresas = this.empresas;
  }

  public clickActivarDesactivar(modeloSucursal:ModeloSucursal){
    modeloSucursal.activo = !modeloSucursal.activo;
    this.servicioSucursal.guardar(modeloSucursal)
      .then(() => 
        {
          this.obtenerTodos();
        })
      .catch((error : HttpErrorResponse) =>
        {
          Notification.Show('bottom', 'right', 'danger', error.message);
        })
  }

  private cerrandoFormulario(respuesta){
    if(respuesta == Object(respuesta)){
      this.obtenerTodos();
    }
  }

  public obtenerNombreEmpresa(idEmpresa){
    if(this.empresas != null){
      let empresa = this.empresas.filter(e => e.idEmpresa == idEmpresa);
      if(empresa == null)
          return null;

          return empresa[0].nombre;
    }

    return null;
  }
}