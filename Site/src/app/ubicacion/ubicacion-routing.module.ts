import { Routes, RouterModule } from '@angular/router';
import { AutorizacionGuardia } from '../seguridad/autorizacion-guardia';
import { EmpresaListaComponent } from './empresa/empresa-lista/empresa-lista.component';
import { SucursalListaComponent } from './sucursal/sucursal-lista/sucursal-lista.component';

export const UbicacionRoutes: Routes = [
  {
    path: '',
    children:[
      { path: 'empresa', component: EmpresaListaComponent, canActivate: [AutorizacionGuardia] },
      { path: 'sucursal', component: SucursalListaComponent, canActivate: [AutorizacionGuardia] }
    ]
  }
];