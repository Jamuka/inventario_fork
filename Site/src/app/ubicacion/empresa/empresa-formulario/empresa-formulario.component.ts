import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { ModeloEmpresa } from 'src/app/modelos/ubicacion/empresa.model';
import { ServicioEmpresa } from '../empresa.service';
import { Notification } from '../../../shared/Notification';
import { HttpErrorResponse } from '@angular/common/http';

@Component({
  selector: 'app-empresa-formulario',
  templateUrl: './empresa-formulario.component.html',
  styleUrls: ['./empresa-formulario.component.css']
})

export class EmpresaFormularioComponent implements OnInit {
  // -- Variables
  public formularioEmpresa : FormGroup;
  public modoAgregar : boolean = true;
  public modeloEmpresa : ModeloEmpresa;
  public archivoASubir: File = null;
  public VACIA = "./assets/img/image_placeholder.jpg";

  // -- Constructor
  constructor(private constructorFormulario: FormBuilder,
    public modalActivo: NgbActiveModal,
    private servicioEmpresa:ServicioEmpresa) {     
  }

  // -- Funciones
  ngOnInit(): void {
    this.formularioEmpresa = this.constructorFormulario.group({
      nombre: ['', Validators.required]
    });

    if(!this.modoAgregar){
      this.formularioEmpresa.patchValue(this.modeloEmpresa);
    }
  }

  public guardar(){
    if(this.formularioEmpresa.invalid){
      return;
    }

    let modeloEmpresaAGuardar : ModeloEmpresa = this.formularioEmpresa.value;
    if(!this.modoAgregar){
      modeloEmpresaAGuardar.idEmpresa = this.modeloEmpresa.idEmpresa;
      modeloEmpresaAGuardar.activo = this.modeloEmpresa.activo;
    }
    else{
      modeloEmpresaAGuardar.activo = true
    }

    modeloEmpresaAGuardar.imagen = this.archivoASubir;

    this.servicioEmpresa.guardar(modeloEmpresaAGuardar)
      .then(respuesta => 
        {
          this.modalActivo.dismiss({ empresa : this.modeloEmpresa });
          Notification.Show('bottom', 'right', 'success', "Empresa guardada satisfactoriamente" )
        })
      .catch((error : HttpErrorResponse) => 
        {
          Notification.Show('bottom', 'right', 'danger', error.error);
        });
  }

  cambioEscuchador(archivos: FileList) {
    this.archivoASubir = archivos.item(0);
}
}
