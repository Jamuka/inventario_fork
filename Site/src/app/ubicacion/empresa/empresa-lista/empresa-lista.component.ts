import { HttpErrorResponse } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { IModeloEmpresa, ModeloEmpresa } from 'src/app/modelos/ubicacion/empresa.model';
import { Notification } from 'src/app/shared/Notification';
import { EmpresaFormularioComponent } from '../empresa-formulario/empresa-formulario.component';
import { ServicioEmpresa } from '../empresa.service';

@Component({
  selector: 'app-empresa-lista',
  templateUrl: './empresa-lista.component.html',
  styleUrls: ['./empresa-lista.component.css']
})

export class EmpresaListaComponent implements OnInit {
  // -- Variables
  private cargando : boolean;
  empresas: IModeloEmpresa[];
  pagina: number;

  // -- Constructor
  constructor(private servicioModal: NgbModal, 
    private servicioEmpresa: ServicioEmpresa ) {
  }

  // -- Propiedades
  get Cargando() : boolean{
    return this.cargando;
  }

  // -- Funciones
  ngOnInit(): void {
    this.obtenerTodos();
  }

  obtenerTodos(){
    this.cargando = true;
    this.servicioEmpresa.obtenerTodos()
      .then(registros => 
        { 
          this.empresas = registros ;
          this.pagina = 1;
          this.cargando = false;
        })
      .catch((error :HttpErrorResponse) =>
        {
          Notification.Show('bottom', 'right', 'danger', error.message);
        })
  }

  public clickAgregar(){
    const modal = this.servicioModal.open(EmpresaFormularioComponent);
    modal.result.then(this.cerrandoFormulario.bind(this), this.cerrandoFormulario.bind(this));
  }

  public clickEditar(modeloEmpresa : ModeloEmpresa){
    const modal = this.servicioModal.open(EmpresaFormularioComponent);
    modal.result.then(this.cerrandoFormulario.bind(this), this.cerrandoFormulario.bind(this));
    modal.componentInstance.modoAgregar = false;
    modal.componentInstance.modeloEmpresa = modeloEmpresa;
  }

  public clickActivarDesactivar(modeloEmpresa : ModeloEmpresa){
    modeloEmpresa.activo = !modeloEmpresa.activo;
    this.servicioEmpresa.guardar(modeloEmpresa)
      .then(() => 
        {
          this.obtenerTodos();
        })
      .catch((error : HttpErrorResponse) => 
        {
          Notification.Show('bottom', 'right', 'danger', error.message);
        })
  }

  private cerrandoFormulario(respuesta){
    if(respuesta == Object(respuesta)){
      this.obtenerTodos();
    }
  }
}