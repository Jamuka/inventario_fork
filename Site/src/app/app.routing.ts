import { Routes } from '@angular/router';

import { AdminLayoutComponent } from './layouts/admin/admin-layout.component';
import { AuthLayoutComponent } from './layouts/auth/auth-layout.component';
import { AutorizacionGuardia } from './seguridad/autorizacion-guardia';

export const AppRoutes: Routes = [
    {
      path: '', redirectTo: 'dashboard', pathMatch: 'full', canActivate: [AutorizacionGuardia] 
    }, 
    {
      path: '', component: AdminLayoutComponent, children: [
          { path: '', loadChildren: './dashboard/dashboard.module#DashboardModule', canActivate: [AutorizacionGuardia] }, 
          { path: 'inventario', loadChildren: './inventario/inventario.module#InventarioModule', canActivate: [AutorizacionGuardia] }, 
          { path: 'ubicacion', loadChildren: './ubicacion/ubicacion.module#UbicacionModule', canActivate: [AutorizacionGuardia] }, 
          { path: 'seguridad', loadChildren: './seguridad/seguridad.module#SeguridadModule', canActivate: [AutorizacionGuardia] }
        ]
    }, 
    {
      path: '', component: AuthLayoutComponent, children: [
          { path: 'pages',loadChildren: './pages/pages.module#PagesModule' }
        ]
    }
];
