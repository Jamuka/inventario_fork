import { HttpErrorResponse } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { TreeviewConfig, TreeviewItem } from 'ngx-treeview';
import { ModeloUsuario } from 'src/app/modelos/seguridad/usuario.model';
import { ServicioUsuario } from '../usuario.service';
import { Notification } from 'src/app/shared/Notification';

@Component({
  selector: 'app-usuario-rol-formulario',
  templateUrl: './usuario-rol-formulario.component.html',
  styleUrls: ['./usuario-rol-formulario.component.css']
})

export class UsuarioRolFormularioComponent implements OnInit {
  // -- Variables
  private cargando : boolean;
  public modeloUsuario : ModeloUsuario;
  public elementos: TreeviewItem[];
  configuracion = TreeviewConfig.create({
    hasAllCheckBox: false,
    hasFilter: false,
    hasCollapseExpand: false,
    decoupleChildFromParent: false,
    maxHeight: 300
  });

  // -- Constructor
  constructor(public modalActivo: NgbActiveModal,
    private servicioUsuario:ServicioUsuario) {
  }

  // -- Propiedades
  get Cargando() : boolean{
    return this.cargando;
  }

  // -- Funciones
  ngOnInit(): void {
    this.obtenerTodos();
  }

  obtenerTodos(){
    this.cargando = true;
    this.servicioUsuario.rolObtenerTodos(this.modeloUsuario.idUsuario)
      .then(registros => 
        {
          this.prepararArbol(registros)
          this.cargando = false;
        })
      .catch((error :HttpErrorResponse) =>
        {
          Notification.Show('bottom', 'right', 'danger', error.message);
        })
  }

  public guardar(){
    for (let i = 0; i < this.modeloUsuario.roles.length; i++) {
      let rol = this.modeloUsuario.roles[i];
      let elementoEnArbol = this.buscarItemArbol(this.elementos, rol.idRol);

      if (elementoEnArbol != null) {
          rol.asignado = elementoEnArbol.checked;
      }
    }

    this.servicioUsuario.rolGuardar(this.modeloUsuario)
    .then(respuesta => 
      {
        this.modalActivo.dismiss({ usuario : this.modeloUsuario });
        Notification.Show('bottom', 'right', 'success', "Usuario guardado satisfactoriamente" )
      })
    .catch((error : HttpErrorResponse) => 
      {
        Notification.Show('bottom', 'right', 'danger', error.error);
      });
  }

  private buscarItemArbol(elementosArbol :TreeviewItem[], idRol:number): TreeviewItem {
    let encontrado = elementosArbol.find(i => i.value == idRol);
    if (encontrado != null) {
        return encontrado;
    }
    else {
        for (let i = 0; i < elementosArbol.length; i++) {
            if (elementosArbol[i].children != null) {
                let encontrado = this.buscarItemArbol(elementosArbol[i].children, idRol);
                if (encontrado != null) {
                    return encontrado;
                }
            }
        }
    }
    return null;
  }

  private prepararArbol(respuesta:ModeloUsuario) {
    this.modeloUsuario = respuesta;
    this.elementos = new Array<TreeviewItem>();

    for (let i = 0; i < this.modeloUsuario.roles.length; i++) {
      let rol = this.modeloUsuario.roles[i];

      let newParentItem = new TreeviewItem({ text: rol.nombre, value: rol.idRol, children: null, checked: rol.asignado });
      this.elementos.push(newParentItem);
    }                   
  }
}
