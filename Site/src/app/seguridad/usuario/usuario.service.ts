import { HttpClient, HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { ConstantesAplicacion } from 'src/app/app-constants';
import { IModeloEntrarRespuesta } from 'src/app/modelos/seguridad/entrar-respuesta.model';
import { IModeloEntrarSolicitud } from 'src/app/modelos/seguridad/entrar-solicitud.model';
import { IModeloUsuario, ModeloUsuario } from 'src/app/modelos/seguridad/usuario.model';

@Injectable({
  providedIn: 'root'
})

export class ServicioUsuario {
  constructor(private clienteHttp:HttpClient) { 
  }

  entrar(model: IModeloEntrarSolicitud){
    return this.clienteHttp.post<IModeloEntrarRespuesta>(ConstantesAplicacion.ApiSeguridadUsuario + 'entrar', model).toPromise();
  }

  salir(){
    return this.clienteHttp.get(ConstantesAplicacion.ApiSeguridadUsuario + 'salir').toPromise();
  }

  obtenerDireccionIP(): any{  
    return this.clienteHttp.get("http://api.ipify.org/?format=json").toPromise();  
  }  

  obtenerTodos(porSucursal:boolean= true){
    let params = new HttpParams().set('porSucursal', porSucursal.toString());
    return this.clienteHttp.get<IModeloUsuario[]>(ConstantesAplicacion.ApiSeguridadUsuario + 'obtenerTodos', { params: params }).toPromise();
  }

  obtenerTodosPorRol(rol:string){
    let params = new HttpParams().set('rol', rol);
    return this.clienteHttp.get<IModeloUsuario[]>(ConstantesAplicacion.ApiSeguridadUsuario + 'obtenerTodosPorRol', { params: params } ).toPromise();
  }

  guardar(modelo: IModeloUsuario){
    return this.clienteHttp.post(ConstantesAplicacion.ApiSeguridadUsuario + 'guardar', modelo).toPromise();
  }

  eliminar(idUsuario:number){
    let params = new HttpParams().set('idUsuario', idUsuario.toString());
    return this.clienteHttp.delete(ConstantesAplicacion.ApiSeguridadUsuario + 'eliminar', { params: params}).toPromise();
  }

  rolObtenerTodos(idUsuario: number){
    let params = new HttpParams().set('idUsuario', idUsuario.toString());
    return this.clienteHttp.get<IModeloUsuario>(ConstantesAplicacion.ApiSeguridadUsuario + 'rolobtenertodos', { params : params }).toPromise();
  }

  rolGuardar(modelo: ModeloUsuario){
    return this.clienteHttp.post(ConstantesAplicacion.ApiSeguridadUsuario + 'rolGuardar', modelo).toPromise();
  }
}
