import { Component, OnInit } from '@angular/core';
import { ModeloUsuario } from 'src/app/modelos/seguridad/usuario.model';
import { ServicioUsuario } from '../usuario.service';
import { Notification } from 'src/app/shared/Notification';
import { HttpErrorResponse } from '@angular/common/http';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'app-usuario-eliminar',
  templateUrl: './usuario-eliminar.component.html',
  styleUrls: ['./usuario-eliminar.component.css']
})
export class UsuarioEliminarComponent implements OnInit {
  // -- Variables
  public modeloUsuario : ModeloUsuario;

  // -- Constructor
  constructor(public modalActivo: NgbActiveModal,
    private servicioUsuario:ServicioUsuario) {     
  }

  // -- Funciones
  ngOnInit(): void {
  }

  public eliminar(){
    this.servicioUsuario.eliminar(this.modeloUsuario.idUsuario)
      .then(respuesta => 
        {
          this.modalActivo.dismiss({ usuario : this.modeloUsuario });
          Notification.Show('bottom', 'right', 'success', "Usuario eliminado satisfactoriamente" )
        })
      .catch((error : HttpErrorResponse) => 
        {
          Notification.Show('bottom', 'right', 'danger', error.error);
        });
  }
}
