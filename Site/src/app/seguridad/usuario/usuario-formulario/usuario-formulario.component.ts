import { HttpErrorResponse } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { NgbActiveModal, NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { ModeloUsuario } from 'src/app/modelos/seguridad/usuario.model';
import { ServicioUsuario } from '../usuario.service';
import { Notification } from 'src/app/shared/Notification';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { IModeloSucursal } from 'src/app/modelos/ubicacion/sucursal.model';

@Component({
  selector: 'app-usuario-formulario',
  templateUrl: './usuario-formulario.component.html',
  styleUrls: ['./usuario-formulario.component.css']
})
export class UsuarioFormularioComponent implements OnInit {
  // -- Variables
  public formularioUsuario : FormGroup;
  public modoAgregar : boolean = true;
  public modeloUsuario : ModeloUsuario;
  sucursales: IModeloSucursal[];

  // -- Constructor
  constructor(private constructorFormulario: FormBuilder,
    public modalActivo: NgbActiveModal,
    private servicioUsuario:ServicioUsuario) {
  }

  // -- Funciones
  ngOnInit(): void {
    this.formularioUsuario = this.constructorFormulario.group({
      idSucursal: [],
      identificacion: ['', Validators.required],
      correo: ['', Validators.required],
      rut: ['', Validators.required],
      nombre: ['', Validators.required],
      contrasena: ['']
    });

    if(!this.modoAgregar){
      this.formularioUsuario.patchValue(this.modeloUsuario);
    }
  }

  public guardar(){
    if(this.formularioUsuario.invalid){
      return;
    }

    let modeloUsuarioAGuardar : ModeloUsuario = this.formularioUsuario.value;
    if(!this.modoAgregar){
      modeloUsuarioAGuardar.idUsuario = this.modeloUsuario.idUsuario;
      modeloUsuarioAGuardar.activo = this.modeloUsuario.activo;
    }
    else{
      modeloUsuarioAGuardar.activo = true
    }

    this.servicioUsuario.guardar(modeloUsuarioAGuardar)
      .then(respuesta => 
        {
          this.modalActivo.dismiss({ usuario : this.modeloUsuario });
          Notification.Show('bottom', 'right', 'success', "Usuario guardado satisfactoriamente" )
        })
      .catch((error : HttpErrorResponse) => 
        {
          Notification.Show('bottom', 'right', 'danger', error.error);
        });
  }
}
