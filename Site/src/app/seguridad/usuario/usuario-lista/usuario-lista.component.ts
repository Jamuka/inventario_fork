import { HttpErrorResponse } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { IModeloUsuario, ModeloUsuario } from 'src/app/modelos/seguridad/usuario.model';
import { ServicioUsuario } from '../usuario.service';
import { Notification } from 'src/app/shared/Notification';
import { UsuarioFormularioComponent } from '../usuario-formulario/usuario-formulario.component';
import { UsuarioRolFormularioComponent } from '../usuario-rol-formulario/usuario-rol-formulario.component';
import { IModeloSucursal } from 'src/app/modelos/ubicacion/sucursal.model';
import { ServicioSucursal } from 'src/app/ubicacion/sucursal/sucursal.service';
import { ConstantesAplicacion } from 'src/app/app-constants';
import { UsuarioEliminarComponent } from '../usuario-eliminar/usuario-eliminar.component';

@Component({
  selector: 'app-usuario-lista',
  templateUrl: './usuario-lista.component.html',
  styleUrls: ['./usuario-lista.component.css']
})
export class UsuarioListaComponent implements OnInit {
// -- Variables
private cargando : boolean;
usuarios: IModeloUsuario[];
sucursales: IModeloSucursal[];
mostrarEliminar: boolean = false;
pagina: number;

// -- Constructor
constructor(private servicioModal: NgbModal, 
  private servicioUsuario: ServicioUsuario,
  private servicioSucursal: ServicioSucursal) {
}

// -- Propiedades
get Cargando() : boolean{
  return this.cargando;
}

get MostrarEliminar() : boolean{
  return this.mostrarEliminar;
}

// -- Funciones
ngOnInit(): void {
  this.obtenerTodos();

  let usuario:IModeloUsuario = JSON.parse(localStorage.getItem(ConstantesAplicacion.DatosUsuario));
  let roles = usuario.roles.filter(r=> r.nombre == "Administrador");
  if(roles != null && roles.length > 0){
    this.mostrarEliminar = true;
  }
}

obtenerTodos(){
  this.cargando = true;
  this.servicioUsuario.obtenerTodos()
    .then(registros => 
      { 
        this.usuarios = registros ;
        this.pagina = 1;
        this.cargando = false;
        this.obtenerTodosSucursal();
      })
    .catch((error :HttpErrorResponse) =>
      {
        Notification.Show('bottom', 'right', 'danger', error.message);
      })
}

obtenerTodosSucursal(){
  this.servicioSucursal.obtenerTodos()
    .then(registros =>
      {
        this.sucursales = registros;
      })
    .catch((error : HttpErrorResponse) =>
      {
        Notification.Show('bottom', 'right', 'danger', error.message);
      })
}

public clickAgregar(){
  const modal = this.servicioModal.open(UsuarioFormularioComponent);
  modal.result.then(this.cerrandoFormulario.bind(this), this.cerrandoFormulario.bind(this));
  modal.componentInstance.sucursales = this.sucursales;
}

public clickEditar(modeloUsuario : ModeloUsuario){
  const modal = this.servicioModal.open(UsuarioFormularioComponent);
  modal.result.then(this.cerrandoFormulario.bind(this), this.cerrandoFormulario.bind(this));
  modal.componentInstance.modoAgregar = false;
  modal.componentInstance.modeloUsuario = modeloUsuario;
  modal.componentInstance.sucursales = this.sucursales;
}

public clickEliminar(modeloUsuario : ModeloUsuario){
  const modal = this.servicioModal.open(UsuarioEliminarComponent);
  modal.result.then(this.cerrandoFormulario.bind(this), this.cerrandoFormulario.bind(this));
  modal.componentInstance.modeloUsuario = modeloUsuario;
}

public clickActivarDesactivar(modeloUsuario : ModeloUsuario){
  modeloUsuario.activo = !modeloUsuario.activo;
  this.servicioUsuario.guardar(modeloUsuario)
    .then(() => 
      {
        this.obtenerTodos();
      })
    .catch((error : HttpErrorResponse) => 
      {
        Notification.Show('bottom', 'right', 'danger', error.message);
      })
}

public clickRoles(modeloUsuario: ModeloUsuario) {
  const modal = this.servicioModal.open(UsuarioRolFormularioComponent);
  modal.result.then(this.cerrandoFormulario.bind(this), this.cerrandoFormulario.bind(this));
  modal.componentInstance.modeloUsuario = modeloUsuario;
}

private cerrandoFormulario(respuesta){
  if(respuesta == Object(respuesta)){
    this.obtenerTodos();
  }
}
}
