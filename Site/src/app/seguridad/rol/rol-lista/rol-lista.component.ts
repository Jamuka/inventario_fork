import { HttpErrorResponse } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { IModeloRol, ModeloRol } from 'src/app/modelos/seguridad/rol.model';
import { ServicioRol } from '../rol.service';
import { Notification } from 'src/app/shared/Notification';
import { RolFormularioComponent } from '../rol-formulario/rol-formulario.component';
import { RolPermisoFormularioComponent } from '../rol-permiso-formulario/rol-permiso-formulario.component';

@Component({
  selector: 'app-rol-lista',
  templateUrl: './rol-lista.component.html',
  styleUrls: ['./rol-lista.component.css']
})
export class RolListaComponent implements OnInit {
// -- Variables
  private cargando : boolean;
  roles: IModeloRol[];
  pagina: number;

  // -- Constructor
  constructor(private servicioModal: NgbModal, 
    private servicioRol: ServicioRol ) {
  }

  // -- Propiedades
  get Cargando() : boolean{
    return this.cargando;
  }

  // -- Funciones
  ngOnInit(): void {
    this.obtenerTodos();
  }

  obtenerTodos(){
    this.cargando = true;
    this.servicioRol.obtenerTodos()
      .then(registros => 
        { 
          this.roles = registros ;
          this.pagina = 1;
          this.cargando = false;
        })
      .catch((error :HttpErrorResponse) =>
        {
          Notification.Show('bottom', 'right', 'danger', error.message);
        })
  }

  public clickAgregar(){
    const modal = this.servicioModal.open(RolFormularioComponent);
    modal.result.then(this.cerrandoFormulario.bind(this), this.cerrandoFormulario.bind(this));
  }

  public clickEditar(modeloRol : ModeloRol){
    const modal = this.servicioModal.open(RolFormularioComponent);
    modal.result.then(this.cerrandoFormulario.bind(this), this.cerrandoFormulario.bind(this));
    modal.componentInstance.modoAgregar = false;
    modal.componentInstance.modeloRol = modeloRol;
  }

  public clickActivarDesactivar(modeloRol : ModeloRol){
    modeloRol.activo = !modeloRol.activo;
    this.servicioRol.guardar(modeloRol)
      .then(() => 
        {
          this.obtenerTodos();
        })
      .catch((error : HttpErrorResponse) => 
        {
          Notification.Show('bottom', 'right', 'danger', error.message);
        })
  }

  public clickPermisos(modeloRol: ModeloRol) {
    const modal = this.servicioModal.open(RolPermisoFormularioComponent);
    modal.result.then(this.cerrandoFormulario.bind(this), this.cerrandoFormulario.bind(this));
    modal.componentInstance.modeloRol = modeloRol;
  }

  private cerrandoFormulario(respuesta){
    if(respuesta == Object(respuesta)){
      this.obtenerTodos();
    }
  }
}