import { HttpErrorResponse } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { TreeviewConfig, TreeviewItem } from 'ngx-treeview';
import { ModeloRol } from 'src/app/modelos/seguridad/rol.model';
import { ServicioRol } from '../rol.service';
import { Notification } from 'src/app/shared/Notification';

@Component({
  selector: 'app-rol-permiso-formulario',
  templateUrl: './rol-permiso-formulario.component.html',
  styleUrls: ['./rol-permiso-formulario.component.css']
})

export class RolPermisoFormularioComponent implements OnInit {
  // -- Variables
  private cargando : boolean;
  public modeloRol : ModeloRol;
  public elementos: TreeviewItem[];
  configuracion = TreeviewConfig.create({
    hasAllCheckBox: false,
    hasFilter: false,
    hasCollapseExpand: false,
    decoupleChildFromParent: true,
    maxHeight: 300
  });

  // -- Constructor
  constructor(public modalActivo: NgbActiveModal,
    private servicioRol:ServicioRol) {
  }

    // -- Propiedades
    get Cargando() : boolean{
      return this.cargando;
    }

  // -- Funciones
  ngOnInit(): void {
    this.obtenerTodos();
  }

  obtenerTodos(){
    this.cargando = true;
    this.servicioRol.permisoObtenerTodos(this.modeloRol.idRol)
      .then(registros => 
        {
          this.prepararArbol(registros)
          this.cargando = false;
        })
      .catch((error :HttpErrorResponse) =>
        {
          Notification.Show('bottom', 'right', 'danger', error.message);
        })
  }

  public guardar(){
    for (let i = 0; i < this.modeloRol.permisos.length; i++) {
      let permiso = this.modeloRol.permisos[i];
      let elementoEnArbol = this.buscarItemArbol(this.elementos, permiso.idPermiso);

      if (elementoEnArbol != null) {
          permiso.concedido = elementoEnArbol.checked;
      }
    }

    this.servicioRol.permisoGuardar(this.modeloRol)
    .then(respuesta => 
      {
        this.modalActivo.dismiss({ rol : this.modeloRol });
        Notification.Show('bottom', 'right', 'success', "Rol guardado satisfactoriamente" )
      })
    .catch((error : HttpErrorResponse) => 
      {
        Notification.Show('bottom', 'right', 'danger', error.error);
      });
  }

  private buscarItemArbol(elementosArbol :TreeviewItem[], idPermiso:number): TreeviewItem {
    let encontrado = elementosArbol.find(i => i.value == idPermiso);
    if (encontrado != null) {
        return encontrado;
    }
    else {
        for (let i = 0; i < elementosArbol.length; i++) {
            if (elementosArbol[i].children != null) {
                let encontrado = this.buscarItemArbol(elementosArbol[i].children, idPermiso);
                if (encontrado != null) {
                    return encontrado;
                }
            }
        }
    }
    return null;
  }

  private prepararArbol(respuesta:ModeloRol) {
    this.modeloRol = respuesta;
    this.elementos = new Array<TreeviewItem>();

    let padres = this.modeloRol.permisos.filter(p => p.idPermisoPadre == null);
    for (let i = 0; i < padres.length; i++) {
        let padre = padres[i];
        let children = new Array<TreeviewItem>();

        let childs = this.modeloRol.permisos.filter(c => c.idPermisoPadre == padre.idPermiso);
        for (let j = 0; j < childs.length; j++) {
            let child = childs[j];
            children.push(new TreeviewItem({ text: child.nombre, value: child.idPermiso, checked:child.concedido, disabled:false }));
        }
        let newParentItem = new TreeviewItem({ text: padre.nombre, value: padre.idPermiso, children: children, checked: padre.concedido });
        this.elementos.push(newParentItem);
    }            
  }
}
