import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { ModeloRol } from 'src/app/modelos/seguridad/rol.model';
import { ServicioRol } from '../rol.service';
import { Notification } from '../../../shared/Notification';
import { HttpErrorResponse } from '@angular/common/http';

@Component({
  selector: 'app-rol-formulario',
  templateUrl: './rol-formulario.component.html',
  styleUrls: ['./rol-formulario.component.css']
})

export class RolFormularioComponent implements OnInit {
  // -- Variables
  public formularioRol : FormGroup;
  public modoAgregar : boolean = true;
  public modeloRol : ModeloRol;

  // -- Constructor
  constructor(private constructorFormulario: FormBuilder,
    public modalActivo: NgbActiveModal,
    private servicioRol:ServicioRol) {
  }

  // -- Funciones
  ngOnInit(): void {
    this.formularioRol = this.constructorFormulario.group({
      nombre: ['', Validators.required]
    });

    if(!this.modoAgregar){
      this.formularioRol.patchValue(this.modeloRol);
    }
  }

  public guardar(){
    if(this.formularioRol.invalid){
      return;
    }

    let modeloRolAGuardar : ModeloRol = this.formularioRol.value;
    if(!this.modoAgregar){
      modeloRolAGuardar.idRol = this.modeloRol.idRol;
      modeloRolAGuardar.activo = this.modeloRol.activo;
    }
    else{
      modeloRolAGuardar.activo = true
    }

    this.servicioRol.guardar(modeloRolAGuardar)
      .then(respuesta => 
        {
          this.modalActivo.dismiss({ rol : this.modeloRol });
          Notification.Show('bottom', 'right', 'success', "Rol guardado satisfactoriamente" )
        })
      .catch((error : HttpErrorResponse) => 
        {
          Notification.Show('bottom', 'right', 'danger', error.error);
        });
  }
}
