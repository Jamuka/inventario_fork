import { HttpErrorResponse } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { IModeloPermiso, ModeloPermiso } from 'src/app/modelos/seguridad/permiso.model';
import { ServicioPermiso } from '../permiso.service';
import { Notification } from 'src/app/shared/Notification';
import { PermisoFormularioComponent } from '../permiso-formulario/permiso-formulario.component';

@Component({
  selector: 'app-permiso-lista',
  templateUrl: './permiso-lista.component.html',
  styleUrls: ['./permiso-lista.component.css']
})

export class PermisoListaComponent implements OnInit {
  // -- Variables
  private cargando : boolean;
  permisos: IModeloPermiso[];
  pagina: number;

  // -- Constructor
  constructor(private servicioModal: NgbModal, 
    private servicioPermiso: ServicioPermiso ) {
  }

  // -- Propiedades
  get Cargando() : boolean{
    return this.cargando;
  }

  // -- Funciones
  ngOnInit(): void {
    this.obtenerTodos();
  }

  obtenerTodos(){
    this.cargando = true;
    this.servicioPermiso.obtenerTodos()
      .then(registros => 
        { 
          this.permisos = registros ;
          this.pagina = 1;
          this.cargando = false;
        })
      .catch((error :HttpErrorResponse) =>
        {
          Notification.Show('bottom', 'right', 'danger', error.message);
        })
  }

  public clickAgregar(){
    const modal = this.servicioModal.open(PermisoFormularioComponent);
    modal.result.then(this.cerrandoFormulario.bind(this), this.cerrandoFormulario.bind(this));
    modal.componentInstance.permisos = this.permisos;
  }

  public clickEditar(modeloPermiso : ModeloPermiso){
    const modal = this.servicioModal.open(PermisoFormularioComponent);
    modal.result.then(this.cerrandoFormulario.bind(this), this.cerrandoFormulario.bind(this));
    modal.componentInstance.modoAgregar = false;
    modal.componentInstance.modeloPermiso = modeloPermiso;
    modal.componentInstance.permisos = this.permisos;
  }

  public clickActivarDesactivar(modeloPermiso : ModeloPermiso){
    modeloPermiso.activo = !modeloPermiso.activo;
    this.servicioPermiso.guardar(modeloPermiso)
      .then(() => 
        {
          this.obtenerTodos();
        })
      .catch((error : HttpErrorResponse) => 
        {
          Notification.Show('bottom', 'right', 'danger', error.message);
        })
  }

  private cerrandoFormulario(respuesta){
    if(respuesta == Object(respuesta)){
      this.obtenerTodos();
    }
  }

  public obtenerPermisoPadre(idPermisoPadre){
    if(this.permisos != null && idPermisoPadre != null){
      let permiso = this.permisos.filter(e => e.idPermiso == idPermisoPadre);
      if(permiso == null)
          return null;

          return permiso[0].nombre;
    }

    return null;
  }
}
