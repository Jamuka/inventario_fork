import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { IModeloPermiso, ModeloPermiso } from 'src/app/modelos/seguridad/permiso.model';
import { ServicioPermiso } from '../permiso.service';
import { Notification } from '../../../shared/Notification';
import { HttpErrorResponse } from '@angular/common/http';

@Component({
  selector: 'app-permiso-formulario',
  templateUrl: './permiso-formulario.component.html',
  styleUrls: ['./permiso-formulario.component.css']
})

export class PermisoFormularioComponent implements OnInit {
  // -- Variables
  permisos : IModeloPermiso[];
  public formularioPermiso : FormGroup;
  public modoAgregar : boolean = true;
  public modeloPermiso : ModeloPermiso;

  // -- Constructor
  constructor(private constructorFormulario: FormBuilder,
    public modalActivo: NgbActiveModal,
    private servicioPermiso:ServicioPermiso) {     
  }

  // -- Propiedades
  get PermisosFiltrados(){
    if(this.modoAgregar){
      return this.permisos;
    }

    return this.permisos.filter(p=>p.idPermiso != this.modeloPermiso.idPermiso);
  }

  // -- Funciones
  ngOnInit(): void {
    this.formularioPermiso = this.constructorFormulario.group({
      idPermisoPadre: [],
      nombre: ['', Validators.required],
      texto: [''],
      icono : [''],
      orden : ['']
    });

    if(!this.modoAgregar){
      this.formularioPermiso.patchValue(this.modeloPermiso);
    }
    else{
      let max: number = 0;

      if(this.permisos != null){
        if(this.permisos.length > 0){
          max = this.permisos.reduce((a, b) => a = a > b.orden ? a : b.orden, 0);
        }
      }

      max = max +1;
      this.formularioPermiso.patchValue({
        'orden' : max
      });
    }
  }

  public guardar(){
    if(this.formularioPermiso.invalid){
      return;
    }

    let modeloPermisoAGuardar : ModeloPermiso = this.formularioPermiso.value;
    if(!this.modoAgregar){
      modeloPermisoAGuardar.idPermiso = this.modeloPermiso.idPermiso;
      modeloPermisoAGuardar.activo = this.modeloPermiso.activo;
    }
    else{
      modeloPermisoAGuardar.activo = true
    }

    this.servicioPermiso.guardar(modeloPermisoAGuardar)
      .then(respuesta => 
        {
          this.modalActivo.dismiss({ permiso : this.modeloPermiso });
          Notification.Show('bottom', 'right', 'success', "Permiso guardado satisfactoriamente" )
        })
      .catch((error : HttpErrorResponse) => 
        {
          Notification.Show('bottom', 'right', 'danger', error.error);
        });
  }
}