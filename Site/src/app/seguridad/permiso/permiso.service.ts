import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { ConstantesAplicacion } from 'src/app/app-constants';
import { IModeloPermiso } from 'src/app/modelos/seguridad/permiso.model';

@Injectable({
  providedIn: 'root'
})
export class ServicioPermiso {
  constructor(private clienteHttp:HttpClient) { 
  }

  obtenerTodos(){
    return this.clienteHttp.get<IModeloPermiso[]>(ConstantesAplicacion.ApiSeguridadPermiso + 'obtenerTodos' ).toPromise();
  }

  guardar(modelo: IModeloPermiso){
    return this.clienteHttp.post(ConstantesAplicacion.ApiSeguridadPermiso + 'guardar', modelo).toPromise();
  }
}