import { Injectable } from "@angular/core";
import { ActivatedRouteSnapshot, CanActivate, Router, RouterStateSnapshot, UrlTree } from "@angular/router";
import { Observable } from "rxjs";
import { ConstantesAplicacion } from "../app-constants";

@Injectable({
    providedIn: 'root'
})

export class AutorizacionGuardia implements CanActivate {
    constructor(private router: Router){        
    }

    canActivate(route: ActivatedRouteSnapshot, 
        state: RouterStateSnapshot): boolean | UrlTree | Observable<boolean | UrlTree> | Promise<boolean | UrlTree> {
        if(localStorage.getItem(ConstantesAplicacion.Llave)!=null){
            return true;
        }
        else{
            localStorage.setItem(ConstantesAplicacion.Redireccion, state.url);
            this.router.navigate(['/pages/login'], { skipLocationChange:true });
        }

        return false;
    }
}
