import { IModeloCatalogo, ModeloCatalogo } from "../catalogo.model";

export interface IModeloSucursal extends IModeloCatalogo{
    idEmpresa : number;
    idSucursal : number;    
}

export class ModeloSucursal extends ModeloCatalogo implements IModeloSucursal {
    idEmpresa: number;
    idSucursal: number;
}
