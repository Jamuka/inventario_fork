import { ConstantesAplicacion } from "src/app/app-constants";
import { IModeloCatalogo, ModeloCatalogo } from "../catalogo.model";

export interface IModeloEmpresa extends IModeloCatalogo{
    idEmpresa : number;
    urlLogo : string;
    imagen : any;
}

export class ModeloEmpresa extends ModeloCatalogo implements IModeloEmpresa {
    idEmpresa: number;
    urlLogo : string;
    imagen : any;
}
