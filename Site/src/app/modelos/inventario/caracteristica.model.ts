import { IModeloCatalogo, ModeloCatalogo } from "../catalogo.model";

export interface IModeloCaracteristica extends IModeloCatalogo{
    idCaracteristica: number;
    asignado?: boolean;
    valor:string;
}

export class ModeloCaracteristica extends ModeloCatalogo implements IModeloCaracteristica {
    idCaracteristica: number;
    asignado?: boolean;
    valor:string;
}
