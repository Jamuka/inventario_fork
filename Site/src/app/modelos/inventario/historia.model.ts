import { IModeloBase, ModeloBase } from "../base.model";

export interface IModeloHistoria extends IModeloBase{
    idHistoria : number;
    idUsuario : number;
    idEquipo : number;
    idSucursal : number;
    comentarios : string
}

export class ModeloHistoria extends ModeloBase implements IModeloHistoria {
    idHistoria: number;
    idUsuario: number;
    idEquipo: number;
    idSucursal: number;
    comentarios: string;
}