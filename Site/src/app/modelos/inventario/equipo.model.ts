import { IModeloCatalogo, ModeloCatalogo } from "../catalogo.model";
import { IModeloCaracteristica } from "./caracteristica.model";

export interface IModeloEquipo extends IModeloCatalogo{
    idEquipo : number;
    idTipo : number;
    idEstado : number;
    idSucursal : number;
    idUsuario?: number;
    activoFijo:string;
    marca: string;
    modelo : string;
    serie : string;
    numero? : number;
    fechaCompra : Date;
    caracteristicas : IModeloCaracteristica[];
}

export class ModeloEquipo extends ModeloCatalogo implements IModeloEquipo {
    idEquipo: number;
    idTipo: number;
    idEstado: number;
    idSucursal: number;
    activoFijo:string;
    idUsuario?: number;
    marca: string;
    modelo: string;
    serie: string;
    numero?: number;
    fechaCompra: Date;
    caracteristicas : IModeloCaracteristica[];
}