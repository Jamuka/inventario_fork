import { IModeloCatalogo, ModeloCatalogo } from "../catalogo.model";

export interface IModeloEstado extends IModeloCatalogo{
    idEstado : number;
}

export class ModeloEstado extends ModeloCatalogo implements IModeloEstado {
    idEstado: number;
}