import { IModeloCatalogo, ModeloCatalogo } from "../catalogo.model";
import { IModeloCaracteristica } from "./caracteristica.model";

export interface IModeloTipo extends IModeloCatalogo{
    idTipo: number;
    caracteristicas:IModeloCaracteristica[];
}

export class ModeloTipo extends ModeloCatalogo implements IModeloTipo {
    idTipo: number;
    caracteristicas:IModeloCaracteristica[];
}
