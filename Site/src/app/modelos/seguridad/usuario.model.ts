import { IModeloCatalogo, ModeloCatalogo } from "../catalogo.model";
import { IModeloEquipo } from "../inventario/equipo.model";
import { IModeloPermiso } from "./permiso.model";
import { IModeloRol } from "./rol.model";

export interface IModeloUsuario extends IModeloCatalogo{
    idUsuario : number;
    idSucursal?: number;
    identificacion : string;
    correo:string;
    rut:string;
    contrasena : string;
    roles : IModeloRol[];
    permisos : IModeloPermiso[];
    equipos: IModeloEquipo[];
}

export class ModeloUsuario extends ModeloCatalogo implements IModeloUsuario {
    idUsuario: number;
    idSucursal?: number;
    identificacion: string;
    correo: string;
    rut : string;
    contrasena: string;
    roles: IModeloRol[];
    permisos: IModeloPermiso[];
    equipos: IModeloEquipo[];
}