import { IModeloCatalogo, ModeloCatalogo } from "../catalogo.model";

export interface IModeloPermiso extends IModeloCatalogo{
    idPermiso : number;
    idPermisoPadre?: number;
    texto:string;
    icono:string;
    orden:number;
    concedido?: boolean;
    hijos: IModeloPermiso[];
}

export class ModeloPermiso extends ModeloCatalogo implements IModeloPermiso {
    idPermiso: number;
    idPermisoPadre?: number;
    texto: string;
    icono: string;
    orden: number;
    concedido?: boolean;
    hijos: IModeloPermiso[];
}
