import { IModeloCatalogo, ModeloCatalogo } from "../catalogo.model";
import { IModeloPermiso } from "./permiso.model";

export interface IModeloRol extends IModeloCatalogo{
    idRol:number;
    asignado?: boolean;
    permisos: IModeloPermiso[];
}

export class ModeloRol extends ModeloCatalogo implements IModeloRol {
    idRol: number;
    asignado?: boolean;
    permisos: IModeloPermiso[];
}