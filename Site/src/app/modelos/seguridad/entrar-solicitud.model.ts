export interface IModeloEntrarSolicitud{
    identificacion:string;
    contrasena:string;
    direccionIP: string;
    idSucursal?: number;
}
export class ModeloEntrarSolicitud implements IModeloEntrarSolicitud {
    identificacion: string;
    contrasena: string;
    direccionIP: string;
    idSucursal?: number;
}
