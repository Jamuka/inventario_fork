import { IModeloEmpresa } from "../ubicacion/empresa.model";
import { IModeloSucursal } from "../ubicacion/sucursal.model";
import { IModeloUsuario } from "./usuario.model";

export interface IModeloEntrarRespuesta {
    llave:string;
    usuario:IModeloUsuario;
    empresa:IModeloEmpresa;
    sucursal:IModeloSucursal;
}

export class ModeloEntrarRespuesta implements IModeloEntrarRespuesta{
    llave: string;
    usuario: IModeloUsuario;
    empresa:IModeloEmpresa;
    sucursal:IModeloSucursal;
}
