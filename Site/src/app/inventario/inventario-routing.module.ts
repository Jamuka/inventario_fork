import { Routes, RouterModule } from '@angular/router';
import { AutorizacionGuardia } from '../seguridad/autorizacion-guardia';
import { CaracteristicaListaComponent } from './caracteristica/caracteristica-lista/caracteristica-lista.component';
import { EquipoListaComponent } from './equipo/equipo-lista/equipo-lista.component';
import { EstadoListaComponent } from './estado/estado-lista/estado-lista.component';
import { HistoriaListaComponent } from './historia/historia-lista/historia-lista.component';
import { TipoListaComponent } from './tipo/tipo-lista/tipo-lista.component';

export const InventarioRoutes: Routes = [
  {
    path: '',
    children:[
      { path: 'caracteristica', component: CaracteristicaListaComponent, canActivate: [AutorizacionGuardia] },
      { path: 'tipo', component: TipoListaComponent, canActivate: [AutorizacionGuardia] },
      { path: 'estado', component: EstadoListaComponent, canActivate: [AutorizacionGuardia] },
      { path: 'equipo', component: EquipoListaComponent, canActivate: [AutorizacionGuardia] },
      { path: 'historia', component: HistoriaListaComponent, canActivate: [AutorizacionGuardia] }
    ]
  }
];