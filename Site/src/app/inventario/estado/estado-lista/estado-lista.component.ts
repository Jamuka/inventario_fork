import { HttpErrorResponse } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { IModeloEstado, ModeloEstado } from 'src/app/modelos/inventario/estado.model';
import { ServicioEstado } from '../estado.service';
import { Notification } from 'src/app/shared/Notification';
import { EstadoFormularioComponent } from '../estado-formulario/estado-formulario.component';

@Component({
  selector: 'app-estado-lista',
  templateUrl: './estado-lista.component.html',
  styleUrls: ['./estado-lista.component.css']
})

export class EstadoListaComponent implements OnInit {
  // -- Variables
  private cargando : boolean;
  estados: IModeloEstado[];
  pagina: number;

  // -- Constructor
  constructor(private servicioModal: NgbModal, 
    private servicioEstado: ServicioEstado ) {
  }

  // -- Propiedades
  get Cargando() : boolean{
    return this.cargando;
  }

  // -- Funciones
  ngOnInit(): void {
    this.obtenerTodos();
  }

  obtenerTodos(){
    this.cargando = true;
    this.servicioEstado.obtenerTodos()
      .then(registros => 
        { 
          this.estados = registros ;
          this.pagina = 1;
          this.cargando = false;
        })
      .catch((error :HttpErrorResponse) =>
        {
          Notification.Show('bottom', 'right', 'danger', error.message);
        })
  }

  public clickAgregar(){
    const modal = this.servicioModal.open(EstadoFormularioComponent);
    modal.result.then(this.cerrandoFormulario.bind(this), this.cerrandoFormulario.bind(this));
  }

  public clickEditar(modeloEstado : ModeloEstado){
    const modal = this.servicioModal.open(EstadoFormularioComponent);
    modal.result.then(this.cerrandoFormulario.bind(this), this.cerrandoFormulario.bind(this));
    modal.componentInstance.modoAgregar = false;
    modal.componentInstance.modeloEstado = modeloEstado;
  }

  public clickActivarDesactivar(modeloEstado : ModeloEstado){
    modeloEstado.activo = !modeloEstado.activo;
    this.servicioEstado.guardar(modeloEstado)
      .then(() => 
        {
          this.obtenerTodos();
        })
      .catch((error : HttpErrorResponse) => 
        {
          Notification.Show('bottom', 'right', 'danger', error.message);
        })
  }

  private cerrandoFormulario(respuesta){
    if(respuesta == Object(respuesta)){
      this.obtenerTodos();
    }
  }
}
