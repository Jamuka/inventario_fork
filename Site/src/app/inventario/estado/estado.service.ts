import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { ConstantesAplicacion } from 'src/app/app-constants';
import { IModeloEstado } from 'src/app/modelos/inventario/estado.model';

@Injectable({
  providedIn: 'root'
})

export class ServicioEstado {
  constructor(private clienteHttp: HttpClient) {     
  }

  obtenerTodos(){
    return this.clienteHttp.get<IModeloEstado[]>(ConstantesAplicacion.ApiInventarioEstado + 'obtenerTodos' ).toPromise();
  }

  guardar(modelo: IModeloEstado){
    return this.clienteHttp.post(ConstantesAplicacion.ApiInventarioEstado + 'guardar', modelo).toPromise();
  }
}
