import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { ModeloEstado } from 'src/app/modelos/inventario/estado.model';
import { ServicioEstado } from '../estado.service';
import { Notification } from '../../../shared/Notification';
import { HttpErrorResponse } from '@angular/common/http';

@Component({
  selector: 'app-estado-formulario',
  templateUrl: './estado-formulario.component.html',
  styleUrls: ['./estado-formulario.component.css']
})
export class EstadoFormularioComponent implements OnInit {
  // -- Variables
  public formularioEstado : FormGroup;
  public modoAgregar : boolean = true;
  public modeloEstado : ModeloEstado;

 // -- Constructor
 constructor(private constructorFormulario: FormBuilder,
   public modalActivo: NgbActiveModal,
   private servicioEstado:ServicioEstado) {     
 }

 // -- Funciones
 ngOnInit(): void {
   this.formularioEstado = this.constructorFormulario.group({
     nombre: ['', Validators.required]
   });

   if(!this.modoAgregar){
     this.formularioEstado.patchValue(this.modeloEstado);
   }
 }

 public guardar(){
   if(this.formularioEstado.invalid){
     return;
   }

   let modeloEstadoAGuardar : ModeloEstado = this.formularioEstado.value;
   if(!this.modoAgregar){
     modeloEstadoAGuardar.idEstado = this.modeloEstado.idEstado;
     modeloEstadoAGuardar.activo = this.modeloEstado.activo;
   }
   else{
     modeloEstadoAGuardar.activo = true
   }

   this.servicioEstado.guardar(modeloEstadoAGuardar)
     .then(respuesta => 
       {
         this.modalActivo.dismiss({ estado : this.modeloEstado });
         Notification.Show('bottom', 'right', 'success', "Estado guardada satisfactoriamente" )
       })
     .catch((error : HttpErrorResponse) => 
       {
         Notification.Show('bottom', 'right', 'danger', error.error);
       });
 }
}
