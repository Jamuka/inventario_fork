import { HttpErrorResponse } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { IModeloCaracteristica, ModeloCaracteristica } from 'src/app/modelos/inventario/caracteristica.model';
import { ServicioCaracteristica } from '../caracteristica.service';
import { Notification } from '../../../shared/Notification';
import { CaracteristicaFormularioComponent } from '../caracteristica-formulario/caracteristica-formulario.component';
import { ConstantesAplicacion } from 'src/app/app-constants';
import { IModeloUsuario } from 'src/app/modelos/seguridad/usuario.model';
import { CaracteristicaEliminarComponent } from '../caracteristica-eliminar/caracteristica-eliminar.component';

@Component({
  selector: 'app-caracteristica-lista',
  templateUrl: './caracteristica-lista.component.html',
  styleUrls: ['./caracteristica-lista.component.css']
})

export class CaracteristicaListaComponent implements OnInit {
 // -- Variables
 private cargando : boolean;
 caracteristicas: IModeloCaracteristica[];
 mostrarEliminar: boolean = false;
 pagina: number;

 // -- Constructor
 constructor(private servicioModal: NgbModal, 
   private servicioCaracteristica: ServicioCaracteristica ) {
 }

 // -- Propiedades
 get Cargando() : boolean{
   return this.cargando;
 }

 get MostrarEliminar() : boolean{
  return this.mostrarEliminar;
}

 // -- Funciones
 ngOnInit(): void {
   this.obtenerTodos();

   let usuario:IModeloUsuario = JSON.parse(localStorage.getItem(ConstantesAplicacion.DatosUsuario));
   let roles = usuario.roles.filter(r=> r.nombre == "Administrador");
   if(roles != null && roles.length > 0){
     this.mostrarEliminar = true;
   }
 }

 obtenerTodos(){
   this.cargando = true;
   this.servicioCaracteristica.obtenerTodos()
     .then(registros => 
       { 
         this.caracteristicas = registros ;
         this.pagina = 1;
         this.cargando = false;
       })
     .catch((error :HttpErrorResponse) =>
       {
         Notification.Show('bottom', 'right', 'danger', error.message);
       })
 }

 public clickAgregar(){
   const modal = this.servicioModal.open(CaracteristicaFormularioComponent);
   modal.result.then(this.cerrandoFormulario.bind(this), this.cerrandoFormulario.bind(this));
 }

 public clickEditar(modeloCaracteristica : ModeloCaracteristica){
   const modal = this.servicioModal.open(CaracteristicaFormularioComponent);
   modal.result.then(this.cerrandoFormulario.bind(this), this.cerrandoFormulario.bind(this));
   modal.componentInstance.modoAgregar = false;
   modal.componentInstance.modeloCaracteristica = modeloCaracteristica;
 }

 public clickEliminar(modeloCaracteristica : ModeloCaracteristica){
  const modal = this.servicioModal.open(CaracteristicaEliminarComponent);
  modal.result.then(this.cerrandoFormulario.bind(this), this.cerrandoFormulario.bind(this));
  modal.componentInstance.modeloCaracteristica = modeloCaracteristica;
}

 public clickActivarDesactivar(modeloCaracteristica : ModeloCaracteristica){
   modeloCaracteristica.activo = !modeloCaracteristica.activo;
   this.servicioCaracteristica.guardar(modeloCaracteristica)
     .then(() => 
       {
         this.obtenerTodos();
       })
     .catch((error : HttpErrorResponse) => 
       {
         Notification.Show('bottom', 'right', 'danger', error.message);
       })
 }

 private cerrandoFormulario(respuesta){
   if(respuesta == Object(respuesta)){
     this.obtenerTodos();
   }
 }
}
