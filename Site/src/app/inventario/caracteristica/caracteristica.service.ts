import { HttpClient, HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { ConstantesAplicacion } from 'src/app/app-constants';
import { IModeloCaracteristica } from 'src/app/modelos/inventario/caracteristica.model';

@Injectable({
  providedIn: 'root'
})

export class ServicioCaracteristica {
  constructor(private clienteHttp:HttpClient) { 
  }

  obtenerTodos(){
    return this.clienteHttp.get<IModeloCaracteristica[]>(ConstantesAplicacion.ApiInventarioCaracteristica + 'obtenerTodos' ).toPromise();
  }

  guardar(modelo: IModeloCaracteristica){
    return this.clienteHttp.post(ConstantesAplicacion.ApiInventarioCaracteristica + 'guardar', modelo).toPromise();
  }

  eliminar(idCaracteristica:number){
    let params = new HttpParams().set('idCaracteristica', idCaracteristica.toString());
    return this.clienteHttp.delete(ConstantesAplicacion.ApiInventarioCaracteristica + 'eliminar', { params: params}).toPromise();
  }
}