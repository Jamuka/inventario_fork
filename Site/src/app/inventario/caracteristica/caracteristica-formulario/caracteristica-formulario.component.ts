import { HttpErrorResponse } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { ModeloCaracteristica } from 'src/app/modelos/inventario/caracteristica.model';
import { Notification } from '../../../shared/Notification';
import { ServicioCaracteristica } from '../caracteristica.service';

@Component({
  selector: 'app-caracteristica-formulario',
  templateUrl: './caracteristica-formulario.component.html',
  styleUrls: ['./caracteristica-formulario.component.css']
})

export class CaracteristicaFormularioComponent implements OnInit {
  // -- Variables
  public formularioCaracteristica : FormGroup;
  public modoAgregar : boolean = true;
  public modeloCaracteristica : ModeloCaracteristica;

  // -- Constructor
  constructor(private constructorFormulario: FormBuilder,
    public modalActivo: NgbActiveModal,
    private servicioCaracteristica:ServicioCaracteristica) {
  }

  // -- Funciones
  ngOnInit(): void {
    this.formularioCaracteristica = this.constructorFormulario.group({
      nombre: ['', Validators.required]
    });

    if(!this.modoAgregar){
      this.formularioCaracteristica.patchValue(this.modeloCaracteristica);
    }
  }

  public guardar(){
    if(this.formularioCaracteristica.invalid){
      return;
    }

    let modeloCaracteristicaAGuardar : ModeloCaracteristica = this.formularioCaracteristica.value;
    if(!this.modoAgregar){
      modeloCaracteristicaAGuardar.idCaracteristica = this.modeloCaracteristica.idCaracteristica;
      modeloCaracteristicaAGuardar.activo = this.modeloCaracteristica.activo;
    }
    else{
      modeloCaracteristicaAGuardar.activo = true
    }

    this.servicioCaracteristica.guardar(modeloCaracteristicaAGuardar)
      .then(respuesta => 
        {
          this.modalActivo.dismiss({ caracteristica : this.modeloCaracteristica });
          Notification.Show('bottom', 'right', 'success', "Caracteristica guardada satisfactoriamente" )
        })
      .catch((error : HttpErrorResponse) => 
        {
          Notification.Show('bottom', 'right', 'danger', error.error);
        });
  }
}
