import { HttpErrorResponse } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { IModeloEquipo, ModeloEquipo } from 'src/app/modelos/inventario/equipo.model';
import { ServicioEquipo } from '../equipo.service';
import { Notification } from 'src/app/shared/Notification';
import { EquipoFormularioComponent } from '../equipo-formulario/equipo-formulario.component';
import { IModeloSucursal, ModeloSucursal } from 'src/app/modelos/ubicacion/sucursal.model';
import { IModeloTipo } from 'src/app/modelos/inventario/tipo.model';
import { IModeloEstado } from 'src/app/modelos/inventario/estado.model';
import { IModeloUsuario } from 'src/app/modelos/seguridad/usuario.model';
import { ServicioTipo } from '../../tipo/tipo.service';
import { ServicioEstado } from '../../estado/estado.service';
import { ServicioSucursal } from 'src/app/ubicacion/sucursal/sucursal.service';
import { ServicioUsuario } from 'src/app/seguridad/usuario/usuario.service';
import { EquipoEtiquetaFormularioComponent } from '../equipo-etiqueta-formulario/equipo-etiqueta-formulario.component';
import { EquipoHistoriaListaComponent } from '../equipo-historia-lista/equipo-historia-lista.component';
import { Sort } from '@angular/material/sort';
import { IModeloEmpresa } from 'src/app/modelos/ubicacion/empresa.model';
import { ServicioEmpresa } from 'src/app/ubicacion/empresa/empresa.service';
import { ConstantesAplicacion } from 'src/app/app-constants';

@Component({
  selector: 'app-equipo-lista',
  templateUrl: './equipo-lista.component.html',
  styleUrls: ['./equipo-lista.component.css']
})

export class EquipoListaComponent implements OnInit {
 // -- Variables
 private cargando : boolean;
 tipos: IModeloTipo[];
 estados: IModeloEstado[];
 sucursales: IModeloSucursal[];
 usuarios: IModeloUsuario[];
 usuariosSucursal : IModeloUsuario[];
 equipos: IModeloEquipo[];
 pagina : number;
 equiposOrdenados: IModeloEquipo[];
 empresas:IModeloEmpresa[];

 // -- Constructor
 constructor(private servicioModal : NgbModal, 
   private servicioTipo: ServicioTipo,
   private servicioEstado: ServicioEstado,
   private servicioSucursal: ServicioSucursal,
   private servicioUsuario: ServicioUsuario,
   private servicioEquipo: ServicioEquipo,
   private servicioEmpresa: ServicioEmpresa) {
 }

 // -- Propiedades
 get Cargando() : boolean{
   return this.cargando;
 }

 // -- Funciones
 ngOnInit(): void {
   this.obtenerTodos();
 }

 obtenerTodos(){
   this.cargando = true;
   this.servicioEquipo.obtenerTodos()
     .then(registros =>
       {
         this.equipos = registros;
         this.equiposOrdenados = this.equipos.slice();
         this.pagina = 1;
         this.cargando = false;
          this.obtenerTodosTipo();
          this.obtenerTodosEstado();
          this.obtenerTodosSucursal();
          this.obtenerTodosUsuario();
          this.obtenerTodosEmpresa();
       })
     .catch((error : HttpErrorResponse) =>
       {
         Notification.Show('bottom', 'right', 'danger', error.message);
       })
 }

 obtenerTodosTipo(){
   this.servicioTipo.obtenerTodos()
     .then(registros =>
       {
         this.tipos = registros;
       })
     .catch((error : HttpErrorResponse) =>
       {
         Notification.Show('bottom', 'right', 'danger', error.message);
       })
 }

 obtenerTodosEstado(){
  this.servicioEstado.obtenerTodos()
    .then(registros =>
      {
        this.estados = registros;
      })
    .catch((error : HttpErrorResponse) =>
      {
        Notification.Show('bottom', 'right', 'danger', error.message);
      })
}

obtenerTodosSucursal(){
  this.servicioSucursal.obtenerTodos()
    .then(registros =>
      {
        this.sucursales = registros;
      })
    .catch((error : HttpErrorResponse) =>
      {
        Notification.Show('bottom', 'right', 'danger', error.message);
      })
}

obtenerTodosUsuario(){
  this.servicioUsuario.obtenerTodos(false)
    .then(registros =>
      {
        this.usuarios = registros;

        let sucursal:ModeloSucursal = JSON.parse(localStorage.getItem(ConstantesAplicacion.DatosSucursal));
        if(sucursal !=null)
        {          
          this.usuariosSucursal = this.usuarios.filter(u=> u.idSucursal == sucursal.idSucursal);
        }
        else
        {
          this.usuariosSucursal = this.usuarios;
        }
      })
    .catch((error : HttpErrorResponse) =>
      {
        Notification.Show('bottom', 'right', 'danger', error.message);
      })
}

obtenerTodosEmpresa(){
  this.servicioEmpresa.obtenerTodos()
    .then(registros =>
      {
        this.empresas = registros;
      })
    .catch((error : HttpErrorResponse) =>
      {
        Notification.Show('bottom', 'right', 'danger', error.message);
      })
}

 public clickAgregar(){
   const modal = this.servicioModal.open(EquipoFormularioComponent, { size: 'lg' });
   modal.result.then(this.cerrandoFormulario.bind(this), this.cerrandoFormulario.bind(this));
   modal.componentInstance.tipos = this.tipos;
   modal.componentInstance.estados = this.estados;
   modal.componentInstance.usuarios = this.usuariosSucursal;

   let sucursal:ModeloSucursal = JSON.parse(localStorage.getItem(ConstantesAplicacion.DatosSucursal));
   if(sucursal !=null)
   {
      modal.componentInstance.sucursales = this.sucursales.filter(s => s.idSucursal == sucursal.idSucursal);
   }
   else
   {
    modal.componentInstance.sucursales = this.sucursales; 
   }
 }

 public clickEditar(modeloEquipo :ModeloEquipo){
   const modal = this.servicioModal.open(EquipoFormularioComponent, { size: 'lg' });
   modal.result.then(this.cerrandoFormulario.bind(this), this.cerrandoFormulario.bind(this));
   modal.componentInstance.modoAgregar = false;
   modal.componentInstance.modeloEquipo = modeloEquipo;
   modal.componentInstance.tipos = this.tipos;
   modal.componentInstance.estados = this.estados;
   modal.componentInstance.usuarios = this.usuariosSucursal;

   let sucursal:ModeloSucursal = JSON.parse(localStorage.getItem(ConstantesAplicacion.DatosSucursal));
   if(sucursal !=null)
   {
      modal.componentInstance.sucursales = this.sucursales.filter(s => s.idSucursal == sucursal.idSucursal);
   }
   else
   {
    modal.componentInstance.sucursales = this.sucursales; 
   }
 }

 public clickImprimirEtiqueta(modeloEquipo :ModeloEquipo){
  const modal = this.servicioModal.open(EquipoEtiquetaFormularioComponent);
  modal.componentInstance.modeloEquipo = modeloEquipo;
  modal.componentInstance.empresas = this.empresas;
  modal.componentInstance.sucursales = this.sucursales;
  modal.result.then(this.cerrandoFormulario.bind(this), this.cerrandoFormulario.bind(this, false));
}

public clickHistoria(modeloEquipo :ModeloEquipo){
  const modal = this.servicioModal.open(EquipoHistoriaListaComponent, { size: 'lg' });
  modal.result.then(this.cerrandoFormulario.bind(this), this.cerrandoFormulario.bind(this, false));
  modal.componentInstance.modeloEquipo = modeloEquipo;
  modal.componentInstance.tipos = this.tipos;
  modal.componentInstance.estados = this.estados;
  modal.componentInstance.sucursales = this.sucursales;
  modal.componentInstance.usuarios = this.usuarios;
}

 public clickActivarDesactivar(modeloEquipo:ModeloEquipo){
   modeloEquipo.activo = !modeloEquipo.activo;
   this.servicioEquipo.guardar(modeloEquipo)
     .then(() => 
       {
         this.obtenerTodos();
       })
     .catch((error : HttpErrorResponse) =>
       {
         Notification.Show('bottom', 'right', 'danger', error.message);
       })
 }

 private cerrandoFormulario(respuesta, flag=true){
   if(respuesta == Object(respuesta)){
     if(flag)
      this.obtenerTodos();
   }
 }

 public obtenerNombreTipo(idTipo){
   if(this.tipos != null){
     let tipo = this.tipos.filter(t => t.idTipo == idTipo);
     if(tipo == null)
         return null;

         return tipo[0].nombre;
   }

   return null;
 }

 public obtenerNombreEstado(idEstado){
  if(this.estados != null){
    let estado = this.estados.filter(e => e.idEstado == idEstado);
    if(estado == null)
      return null;

    return estado[0].nombre;
  }

  return null;
 }

 public obtenerNombreSucursal(idSucursal){
  if(this.sucursales != null){
    let sucursal = this.sucursales.filter(s => s.idSucursal == idSucursal);
    if(sucursal == null || sucursal.length == 0)
        return null;

        return sucursal[0].nombre;
  }

  return null;
 }

  public obtenerIdentificacionUsuario(idUsuario){
    if(this.usuarios != null){
      let usuario = this.usuarios.filter(u => u.idUsuario == idUsuario);
      if(usuario == null || usuario.length == 0)
        return null;

      return usuario[0].nombre;
    }

    return null;
  }

  sortData(sort: Sort) {
    const data = this.equipos.slice();
    if (!sort.active || sort.direction === '') {
      this.equiposOrdenados = data;
      return;
    }

    this.equiposOrdenados = data.sort((a, b) => {
      const isAsc = sort.direction === 'asc';
      switch (sort.active) {
        case 'idEquipo': return compare(a.idEquipo, b.idEquipo, isAsc);
        case 'sucursal': return compare(this.obtenerNombreSucursal(a.idSucursal), this.obtenerNombreSucursal(b.idSucursal), isAsc);
        case 'nombre': return compare(a.nombre, b.nombre, isAsc);
        case 'marca': return compare(a.marca, b.marca, isAsc);
        default: return 0;
      }
    });
  }
}

function compare(a: number | string, b: number | string, isAsc: boolean) {
  return (a < b ? -1 : 1) * (isAsc ? 1 : -1);
}
