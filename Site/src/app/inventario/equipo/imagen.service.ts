import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class ServicioImagen {

  constructor(private clienteHttp:HttpClient) {    
  }

  obtener(imageUrl: string): Observable<Blob> {
    return this.clienteHttp.get( environment.RECURSOS_URL + imageUrl, { responseType: 'blob' });
  }
}
