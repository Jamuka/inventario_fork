import { HttpClient, HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { ConstantesAplicacion } from 'src/app/app-constants';
import { IModeloEquipo } from 'src/app/modelos/inventario/equipo.model';

@Injectable({
  providedIn: 'root'
})

export class ServicioEquipo {
  constructor(private clienteHttp:HttpClient){    
  }

  obtenerTodos(porSucursal:boolean= true){
    let params = new HttpParams().set('porSucursal', porSucursal.toString());
    return this.clienteHttp.get<IModeloEquipo[]>(ConstantesAplicacion.ApiInventarioEquipo + "obtenerTodos", { params: params }).toPromise();
  }

  guardar(modelo: IModeloEquipo){
    return this.clienteHttp.post(ConstantesAplicacion.ApiInventarioEquipo + 'guardar', modelo).toPromise();
  }

  registroObtenerTodos(idEquipo: number){
    let params = new HttpParams().set('idEquipo', idEquipo.toString());
    return this.clienteHttp.get<IModeloEquipo[]>(ConstantesAplicacion.ApiInventarioEquipo + "registroobtenerTodos", { params : params }).toPromise();
  }
}
