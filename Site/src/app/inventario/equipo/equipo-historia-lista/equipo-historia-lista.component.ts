import { HttpErrorResponse } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { NgbActiveModal, NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { IModeloEquipo, ModeloEquipo } from 'src/app/modelos/inventario/equipo.model';
import { IModeloEstado } from 'src/app/modelos/inventario/estado.model';
import { IModeloTipo } from 'src/app/modelos/inventario/tipo.model';
import { IModeloUsuario } from 'src/app/modelos/seguridad/usuario.model';
import { IModeloSucursal } from 'src/app/modelos/ubicacion/sucursal.model';
import { ServicioEquipo } from '../equipo.service';
import { Notification } from 'src/app/shared/Notification';

@Component({
  selector: 'app-equipo-historia-lista',
  templateUrl: './equipo-historia-lista.component.html',
  styleUrls: ['./equipo-historia-lista.component.css']
})

export class EquipoHistoriaListaComponent implements OnInit {
  // -- Variables
  private cargando : boolean;
  tipos: IModeloTipo[];
  estados: IModeloEstado[];
  sucursales: IModeloSucursal[];
  usuarios: IModeloUsuario[];
  historias: IModeloEquipo[];
  modeloEquipo : ModeloEquipo;
  pagina : number;

  // -- Constructor
  constructor(public modalActivo: NgbActiveModal, 
    private servicioEquipo: ServicioEquipo) {    
  }

  // -- Propiedades
  get Cargando() : boolean{
    return this.cargando;
  }

  // -- Funciones
  ngOnInit(): void {
    this.obtenerTodos();
  }

  obtenerTodos(){
    this.cargando = true;
    this.servicioEquipo.registroObtenerTodos(this.modeloEquipo.idEquipo)
      .then(registros =>
        {
          this.historias = registros;
          this.pagina = 1;
          this.cargando = false;     
        })
      .catch((error : HttpErrorResponse) =>
        {
          Notification.Show('bottom', 'right', 'danger', error.message);
        })
  }

  public obtenerNombreTipo(idTipo){
    if(this.tipos != null){
      let tipo = this.tipos.filter(t => t.idTipo == idTipo);
      if(tipo == null)
          return null;
 
          return tipo[0].nombre;
    }
 
    return null;
  }
 
  public obtenerNombreEstado(idEstado){
   if(this.estados != null){
     let estado = this.estados.filter(e => e.idEstado == idEstado);
     if(estado == null)
       return null;
 
     return estado[0].nombre;
   }
 
   return null;
  }
 
  public obtenerNombreSucursal(idSucursal){
   if(this.sucursales != null){
     let sucursal = this.sucursales.filter(s => s.idSucursal == idSucursal);
     if(sucursal == null || sucursal.length == 0)
         return null;
 
         return sucursal[0].nombre;
   }
 
   return null;
  }
 
   public obtenerIdentificacionUsuario(idUsuario){
     if(this.usuarios != null){
       let usuario = this.usuarios.filter(u => u.idUsuario == idUsuario);
       if(usuario == null|| usuario.length == 0)
         return null;
 
       return usuario[0].nombre;
     }
 
     return null;
   }
}
