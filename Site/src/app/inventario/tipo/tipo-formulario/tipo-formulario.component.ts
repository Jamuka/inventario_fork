import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { ModeloTipo } from 'src/app/modelos/inventario/tipo.model';
import { ServicioTipo } from '../tipo.service';
import { Notification } from '../../../shared/Notification';
import { HttpErrorResponse } from '@angular/common/http';

@Component({
  selector: 'app-tipo-formulario',
  templateUrl: './tipo-formulario.component.html',
  styleUrls: ['./tipo-formulario.component.css']
})

export class TipoFormularioComponent implements OnInit {
  // -- Variables
  public formularioTipo : FormGroup;
  public modoAgregar : boolean = true;
  public modeloTipo : ModeloTipo;

  // -- Constructor
  constructor(private constructorFormulario: FormBuilder,
    public modalActivo: NgbActiveModal,
    private servicioTipo:ServicioTipo) {     
  }

  // -- Funciones
  ngOnInit(): void {
    this.formularioTipo = this.constructorFormulario.group({
      nombre: ['', Validators.required]
    });

    if(!this.modoAgregar){
      this.formularioTipo.patchValue(this.modeloTipo);
    }
  }

  public guardar(){
    if(this.formularioTipo.invalid){
      return;
    }

    let modeloTipoAGuardar : ModeloTipo = this.formularioTipo.value;
    if(!this.modoAgregar){
      modeloTipoAGuardar.idTipo = this.modeloTipo.idTipo;
      modeloTipoAGuardar.activo = this.modeloTipo.activo;
    }
    else{
      modeloTipoAGuardar.activo = true
    }

    this.servicioTipo.guardar(modeloTipoAGuardar)
      .then(respuesta => 
        {
          this.modalActivo.dismiss({ tipo : this.modeloTipo });
          Notification.Show('bottom', 'right', 'success', "Tipo guardada satisfactoriamente" )
        })
      .catch((error : HttpErrorResponse) => 
        {
          Notification.Show('bottom', 'right', 'danger', error.error);
        });
  }
}