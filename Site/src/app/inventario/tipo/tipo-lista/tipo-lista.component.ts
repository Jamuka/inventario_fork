import { HttpErrorResponse } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { IModeloTipo, ModeloTipo } from 'src/app/modelos/inventario/tipo.model';
import { ServicioTipo } from '../tipo.service';
import { Notification } from 'src/app/shared/Notification';
import { TipoFormularioComponent } from '../tipo-formulario/tipo-formulario.component';
import { TipoCaracteristicaFormularioComponent } from '../tipo-caracteristica-formulario/tipo-caracteristica-formulario.component';

@Component({
  selector: 'app-tipo-lista',
  templateUrl: './tipo-lista.component.html',
  styleUrls: ['./tipo-lista.component.css']
})
export class TipoListaComponent implements OnInit {
  // -- Variables
  private cargando : boolean;
  tipos: IModeloTipo[];
  pagina: number;

  // -- Constructor
  constructor(private servicioModal: NgbModal, 
    private servicioTipo: ServicioTipo ) {
  }

  // -- Propiedades
  get Cargando() : boolean{
    return this.cargando;
  }

  // -- Funciones
  ngOnInit(): void {
    this.obtenerTodos();
  }

  obtenerTodos(){
    this.cargando = true;
    this.servicioTipo.obtenerTodos()
      .then(registros => 
        { 
          this.tipos = registros ;
          this.pagina = 1;
          this.cargando = false;
        })
      .catch((error :HttpErrorResponse) =>
        {
          Notification.Show('bottom', 'right', 'danger', error.message);
        })
  }

  public clickAgregar(){
    const modal = this.servicioModal.open(TipoFormularioComponent);
    modal.result.then(this.cerrandoFormulario.bind(this), this.cerrandoFormulario.bind(this));
  }

  public clickEditar(modeloTipo : ModeloTipo){
    const modal = this.servicioModal.open(TipoFormularioComponent);
    modal.result.then(this.cerrandoFormulario.bind(this), this.cerrandoFormulario.bind(this));
    modal.componentInstance.modoAgregar = false;
    modal.componentInstance.modeloTipo = modeloTipo;
  }

  public clickActivarDesactivar(modeloTipo : ModeloTipo){
    modeloTipo.activo = !modeloTipo.activo;
    this.servicioTipo.guardar(modeloTipo)
      .then(() => 
        {
          this.obtenerTodos();
        })
      .catch((error : HttpErrorResponse) => 
        {
          Notification.Show('bottom', 'right', 'danger', error.message);
        })
  }

  public clickCaracteristicas(modeloTipo: ModeloTipo) {
    const modal = this.servicioModal.open(TipoCaracteristicaFormularioComponent);
    modal.result.then(this.cerrandoFormulario.bind(this), this.cerrandoFormulario.bind(this));
    modal.componentInstance.modeloTipo = modeloTipo;
  }

  private cerrandoFormulario(respuesta){
    if(respuesta == Object(respuesta)){
      this.obtenerTodos();
    }
  }
}
