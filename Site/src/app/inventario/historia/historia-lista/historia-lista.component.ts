import { HttpErrorResponse } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { IModeloEquipo } from 'src/app/modelos/inventario/equipo.model';
import { IModeloHistoria, ModeloHistoria } from 'src/app/modelos/inventario/historia.model';
import { IModeloUsuario } from 'src/app/modelos/seguridad/usuario.model';
import { IModeloSucursal, ModeloSucursal } from 'src/app/modelos/ubicacion/sucursal.model';
import { ServicioUsuario } from 'src/app/seguridad/usuario/usuario.service';
import { ServicioSucursal } from 'src/app/ubicacion/sucursal/sucursal.service';
import { ServicioEquipo } from '../../equipo/equipo.service';
import { HistoriaFormularioComponent } from '../historia-formulario/historia-formulario.component';
import { ServicioHistoria } from '../historia.service';
import { Notification } from 'src/app/shared/Notification';
import { ConstantesAplicacion } from 'src/app/app-constants';

@Component({
  selector: 'app-historia-lista',
  templateUrl: './historia-lista.component.html',
  styleUrls: ['./historia-lista.component.css']
})

export class HistoriaListaComponent implements OnInit {
 // -- Variables
 private cargando : boolean;
 usuarios: IModeloUsuario[];
 equipos: IModeloEquipo[];
 equiposSucursal: IModeloEquipo[];
 sucursales: IModeloSucursal[];
 historias: IModeloHistoria[];
 pagina : number;

 // -- Constructor
 constructor(private servicioModal : NgbModal,    
   private servicioUsuario: ServicioUsuario,
   private servicioEquipo: ServicioEquipo,
   private servicioSucursal: ServicioSucursal,
   private servicioHistoria: ServicioHistoria) {
 }

 // -- Propiedades
 get Cargando() : boolean{
   return this.cargando;
 }

 // -- Funciones
 ngOnInit(): void {
   this.obtenerTodos();
 }

 obtenerTodos(){
   this.cargando = true;
   this.servicioHistoria.obtenerTodos()
     .then(registros =>
       {
         this.historias = registros;
         this.pagina = 1;
         this.cargando = false;
         this.obtenerTodosUsuario();
         this.obtenerTodosEquipo();
          this.obtenerTodosSucursal();          
       })
     .catch((error : HttpErrorResponse) =>
       {
         Notification.Show('bottom', 'right', 'danger', error.message);
       })
 }

obtenerTodosUsuario(){
  this.servicioUsuario.obtenerTodos()
    .then(registros =>
      {
        this.usuarios = registros;
      })
    .catch((error : HttpErrorResponse) =>
      {
        Notification.Show('bottom', 'right', 'danger', error.message);
      })
}

obtenerTodosEquipo(){
  this.servicioEquipo.obtenerTodos(false)
    .then(registros =>
      {
        this.equipos = registros;
      })
    .catch((error : HttpErrorResponse) =>
      {
        Notification.Show('bottom', 'right', 'danger', error.message);
      })
}

obtenerTodosSucursal(){
  this.servicioSucursal.obtenerTodos()
    .then(registros =>
      {
        this.sucursales = registros;
      })
    .catch((error : HttpErrorResponse) =>
      {
        Notification.Show('bottom', 'right', 'danger', error.message);
      })
}

 public clickAgregar(){
   const modal = this.servicioModal.open(HistoriaFormularioComponent);
   modal.result.then(this.cerrandoFormulario.bind(this), this.cerrandoFormulario.bind(this));   
   modal.componentInstance.usuarios = this.usuarios;
   modal.componentInstance.equipos = this.equipos;

   let sucursal:ModeloSucursal = JSON.parse(localStorage.getItem(ConstantesAplicacion.DatosSucursal));
   if(sucursal !=null)
   {
      modal.componentInstance.sucursales = this.sucursales.filter(s => s.idSucursal == sucursal.idSucursal);
   }
   else
   {
    modal.componentInstance.sucursales = this.sucursales; 
   }
 }

 public clickEditar(modeloHistoria :ModeloHistoria){
   const modal = this.servicioModal.open(HistoriaFormularioComponent);
   modal.result.then(this.cerrandoFormulario.bind(this), this.cerrandoFormulario.bind(this));
   modal.componentInstance.modoAgregar = false;
   modal.componentInstance.modeloHistoria = modeloHistoria;
   modal.componentInstance.usuarios = this.usuarios;
   modal.componentInstance.equipos = this.equipos;
   
   let sucursal:ModeloSucursal = JSON.parse(localStorage.getItem(ConstantesAplicacion.DatosSucursal));
   if(sucursal !=null)
   {
      modal.componentInstance.sucursales = this.sucursales.filter(s => s.idSucursal == sucursal.idSucursal);
   }
   else
   {
    modal.componentInstance.sucursales = this.sucursales; 
   }
 }

 private cerrandoFormulario(respuesta){
   if(respuesta == Object(respuesta)){
     this.obtenerTodos();
   }
 }

 public obtenerIdentificacionUsuario(idUsuario){
  if(this.usuarios != null){
    let usuario = this.usuarios.filter(u => u.idUsuario == idUsuario);
    if(usuario == null || usuario.length == 0)
      return null;

    return usuario[0].nombre;
  }

  return null;
}

 public obtenerNombreEquipo(idEquipo){
  if(this.equipos != null){
    let equipo = this.equipos.filter(e => e.idEquipo == idEquipo);
    if(equipo == null || equipo.length == 0)
      return null;

    return equipo[0].nombre;
  }

  return null;
 }

 public obtenerNombreSucursal(idSucursal){
  if(this.sucursales != null){
    let sucursal = this.sucursales.filter(s => s.idSucursal == idSucursal);
    if(sucursal == null || sucursal.length == 0)
        return null;

        return sucursal[0].nombre;
  }

  return null;
 }


}
