import { HttpErrorResponse } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { IModeloEquipo } from 'src/app/modelos/inventario/equipo.model';
import { ModeloHistoria } from 'src/app/modelos/inventario/historia.model';
import { IModeloUsuario } from 'src/app/modelos/seguridad/usuario.model';
import { IModeloSucursal } from 'src/app/modelos/ubicacion/sucursal.model';
import { Notification } from 'src/app/shared/Notification';
import { ServicioHistoria } from '../historia.service';

@Component({
  selector: 'app-historia-formulario',
  templateUrl: './historia-formulario.component.html',
  styleUrls: ['./historia-formulario.component.css']
})

export class HistoriaFormularioComponent implements OnInit {
  // -- Variables
  public formularioHistoria : FormGroup;
  public modoAgregar : boolean = true;
  public modeloHistoria : ModeloHistoria;
  usuarios: IModeloUsuario[];
  equipos: IModeloEquipo[];
  sucursales: IModeloSucursal[]; 

  // -- Constructor
  constructor(private constructorFormulario : FormBuilder,
    public modalActivo: NgbActiveModal,
    private servicioHistoria:ServicioHistoria) {
  }

  // -- Funciones
  ngOnInit(): void {
    this.formularioHistoria = this.constructorFormulario.group({
      idUsuario: [Validators.required],
      idEquipo: [Validators.required],
      idSucursal: [Validators.required],    
      comentarios: ['']
    });

    if(!this.modoAgregar){
      this.formularioHistoria.patchValue(this.modeloHistoria);
    }
  }

  public guardar(){
    if(this.formularioHistoria.invalid){
      return;
    }

    let modeloHistoriaAGuardar : ModeloHistoria = this.formularioHistoria.value;
    if(!this.modoAgregar){
      modeloHistoriaAGuardar.idHistoria = this.modeloHistoria.idHistoria;
    }

    this.servicioHistoria.guardar(modeloHistoriaAGuardar)
      .then(respuesta => 
        {
          this.modalActivo.dismiss({ historia : this.modeloHistoria });
          Notification.Show('bottom', 'right', 'success', "Historia guardada satisfactoriamente" )
        })
      .catch((error : HttpErrorResponse) => 
        {
          Notification.Show('bottom', 'right', 'danger', error.error);
        });
  }
}
