import { environment } from "src/environments/environment";

export class ConstantesAplicacion {
    public static get Llave() : string {//TokenKey
        return "Llave";
    }

    public static get DatosUsuario() : string {//UserDataKey
        return "DatosUsuario";
    }

    public static get DatosEmpresa() : string {
        return "DatosEmpresa";
    }

    public static get DatosSucursal() : string {
        return "DatosSucursal";
    }

    public static get Bearer() : string {//BearerKey
        return "Bearer";
    }

    public static get Redireccion() : string {//RedirectionUrlKey
        return "Redireccion";
    }

    //----------------------------------------------Tablero
    public static get ApiInventarioTablero(): string {
        return environment.BASE_URL + "/tablero/"
    }

    //----------------------------------------------Inventario
    public static get ApiInventarioCaracteristica(): string {
        return environment.BASE_URL + "/inventario/caracteristica/"
    }

    public static get ApiInventarioEquipo(): string {
        return environment.BASE_URL + "/inventario/equipo/"
    }

    public static get ApiInventarioEstado(): string {
        return environment.BASE_URL + "/inventario/estado/"
    }

    public static get ApiInventarioHistoria(): string {
        return environment.BASE_URL + "/inventario/historia/"
    }

    public static get ApiInventarioTipo(): string {
        return environment.BASE_URL + "/inventario/tipo/"
    }

    //----------------------------------------------Seguridad   
    public static get ApiSeguridadPermiso(): string {
        return environment.BASE_URL + "/seguridad/permiso/"
    }

    public static get ApiSeguridadRol(): string {
        return environment.BASE_URL + "/seguridad/rol/"
    }

    public static get ApiSeguridadUsuario(): string {
        return environment.BASE_URL + "/seguridad/usuario/"
    }

    //----------------------------------------------Ubicacion   
    public static get ApiUbicacionEmpresa(): string {
        return environment.BASE_URL + "/ubicacion/empresa/"
    }

    public static get ApiUbicacionSucursal(): string {
        return environment.BASE_URL + "/ubicacion/sucursal/"
    }
}